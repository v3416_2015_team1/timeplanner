#ifndef ADDUSERDIALOG_H
#define ADDUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>
#include <QCheckBox>
#include <QAction>
#include <QClipboard>
#include <QApplication>

class AddUserDialog : public QDialog
{
    Q_OBJECT
public:
    AddUserDialog(QWidget *parent = 0);
    ~AddUserDialog();
    inline QString getLogin(){return _pLtxtLogin->text();}
    inline QString getPassword(){return _pLblTempPassword->text();}
    inline QString getFirstName(){return _pLtxtFirstName->text();}
    inline QString getSecondame(){return _pLtxtSecondName->text();}
    inline QString getLastName(){return _pLtxtLastName->text();}
    inline QString getPosition(){return _pLtxtPosition->text();}
    inline bool getCreateTaskRight(){return _pChkbxCreateTaskRight->isChecked();}
private:
    QLabel *_pLblLogin;
    QLabel *_pLblTempPasswordLbl;
    QLabel *_pLblFirstName;
    QLabel *_pLblSecondName;
    QLabel *_pLblLastName;
    QLabel *_pLblPosition;
    QLabel *_pLblCreateTaskRight;
    QLineEdit *_pLtxtLogin;
    QLabel *_pLblTempPassword;
    QLineEdit *_pLtxtFirstName;
    QLineEdit *_pLtxtSecondName;
    QLineEdit *_pLtxtLastName;
    QLineEdit *_pLtxtPosition;
    QCheckBox *_pChkbxCreateTaskRight;
    QPushButton *_pBtnSave;
    QPushButton *_pBtnCancel;
    QGridLayout *_pGltMain;
    QAction *_pActionCopyTempPassword;
private slots:
    void _generateTempPassword();
    void _onBtnSaveClicked();
    void _onBtnCancelClicked();
    void _copyTempPassword();
};

#endif // ADDUSERDIALOG_H
