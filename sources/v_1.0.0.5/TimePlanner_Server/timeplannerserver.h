#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QApplication>
#include <QCloseEvent>
#include <QMainWindow>
#include <QInputDialog>
#include "sqlconnector.h"
#include "adduserdialog.h"
#include "edituserdialog.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QDateTime>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QSystemTrayIcon>
#include <QDesktopWidget>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QLabel>
#include <QFileDialog>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include "signinpasswordtype.h"

#define INITIAL_PORT                3333
#define MSG_ERROR                   0
#define MSG_MESSAGE                 10
#define MSG_SIGN_IN                 1
#define MSG_SIGN_OUT                11
#define MSG_TEMP_PASSWORD           21
#define MSG_FORCE_SIGN_OUT          31
#define MSG_TASK_CREATE             2
#define MSG_TASK_USER_CREATOR       12
#define MSG_TASK_USER_ASSIGNEE      22
#define MSG_TASK_EDIT               32
#define MSG_TASK_DELETE             42
#define MSG_TASK_EDIT_STATUS        52
#define MSG_REGISTRATION            3
#define MSG_USERS                   13
#define MSG_NOTIFICATIONS           14
#define MSG_NOTIFY_NEW_TASK         24
#define ERROR_SIGN_IN_FAILURE       4001
#define ERROR_SESSION_FAILURE       4002
#define STATUS_LBL_MYSQL_PORT       "MySQL port: "
#define STATUS_LBL_MYSQL_USER       "MySQL user: "
#define STATUS_LBL_MYSQL_DATABASE   "MySQL database: "
#define STATUS_LBL_TCP_PORT         "TCP port: "

class TimePlannerServer : public QMainWindow
{
    Q_OBJECT
public:
    TimePlannerServer(QWidget *parent = 0);
    ~TimePlannerServer();
protected:
    virtual void closeEvent(QCloseEvent *event);
public slots:
    void newConncetion();
    void receiveClientQuery();
    void log(QString strMsg);
    void logNow(QString strMsg = "");
    void showHide();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason);
    void onAppQuit();
    void newNotification(QString strCaption,
                         QString strContent);
private:
    QTextEdit *_pTxtLog;
    SqlConnector *_pSqlConnector;
    QTcpServer *_pTcpServer;
    QWidget *_pCentralWidget;
    QVBoxLayout *_pVltCentralWidget;
    QSystemTrayIcon *_pSystemTrayIcon;
    QMenu *_pSystemTrayIconContextMenu;
    QAction *_pActionShowHide;
    QAction *_pActionQuit;
    QMenu *_pMenuLog;
    QMenu *_pMenuMySQL;
    QMenu *_pMenuTCP;
    QMenu *_pMenuUsers;
    QMenu *_pMenuAbout;
    QAction *_pActionSaveLog;
    QAction *_pActionPrintLog;
    QAction *_pActionClearLog;
    QAction *_pActionConnectMySQL;
    QAction *_pActionDisconnectMySQL;
    QAction *_pActionRunTCP;
    QAction *_pActionStopTCP;
    QAction *_pActionAddUser;
    QAction *_pActionEditUser;
    QAction *_pActionDeleteUser;
    QAction *_pActionShowLicense;
    QAction *_pActionShowAbout;
    QLabel *_pLblMySQLPort;
    QLabel *_pLblMySQLUser;
    QLabel *_pLblMySQLDataBase;
    QLabel *_pLblTCPPort;
    QMap<QByteArray, QTcpSocket*> _mapSockets;
    void _createActions();
    void _createMenus();
    void _sendToClient(QTcpSocket *pTcpSocket,
                      quint8 reason,
                      QVector<QVariant> vecData);
    void _saveLogStr(QString strLogFilename);
    void _clientSignIn(QTcpSocket *pClientSocket,
                       QString strLogin,
                       QString strPassword,
                       SignInPasswordType passwordType,
                       quint32 clientIP,
                       QString &strLog);
private slots:
    void _saveLog();
    void _printLog();
    void _clearLog();
    void _connectMySQL();
    void _disconnectMySQL();
    void _runTCP();
    void _stopTCP();
    void _addUser();
    void _editUser();
    void _deleteUser();
    void _showLicense();
    void _showAbout();
};

#endif // MAINWINDOW_H
