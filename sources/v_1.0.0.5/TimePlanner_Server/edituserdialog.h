#ifndef EDITUSERDIALOG_H
#define EDITUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>
#include <QCheckBox>
#include <QVariant>
#include <QAction>
#include <QClipboard>
#include <QApplication>

class EditUserDialog : public QDialog
{
public:
    EditUserDialog(QVector<QVariant> &initData);
    ~EditUserDialog();
    inline QString getLogin(){return _pLtxtLogin->text();}
    inline QString getResetedPassword(){return _pLblResetedPassword->text();}
    inline QString getFirstName(){return _pLtxtFirstName->text();}
    inline QString getSecondame(){return _pLtxtSecondName->text();}
    inline QString getLastName(){return _pLtxtLastName->text();}
    inline QString getPosition(){return _pLtxtPosition->text();}
    inline bool getCreateTaskRight(){return _pChkbxCreateTaskRight->isChecked();}
private:
    QLabel *_pLblLogin;
    QLabel *_pLblResetedPasswordLbl;
    QLabel *_pLblResetedPassword;
    QLabel *_pLblFirstName;
    QLabel *_pLblSecondName;
    QLabel *_pLblLastName;
    QLabel *_pLblPosition;
    QLineEdit *_pLtxtLogin;
    QPushButton *_pBtnResetPassword;
    QLineEdit *_pLtxtFirstName;
    QLineEdit *_pLtxtSecondName;
    QLineEdit *_pLtxtLastName;
    QLineEdit *_pLtxtPosition;
    QCheckBox *_pChkbxCreateTaskRight;
    QPushButton *_pBtnSave;
    QPushButton *_pBtnCancel;
    QGridLayout *_pGltMain;
    QAction *_pActionCopyResetedPassword;
private slots:
    void _onBtnSaveClicked();
    void _onBtnCancelClicked();
    void _onBtnResetPasswordClicked();
    void _copyResetedPassword();
};

#endif // EDITUSERDIALOG_H
