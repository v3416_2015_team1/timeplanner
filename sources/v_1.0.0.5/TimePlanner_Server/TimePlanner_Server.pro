#-------------------------------------------------
#
# Project created by QtCreator 2015-10-11T17:10:33
#
#-------------------------------------------------

QT       += core gui sql network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TimePlanner_Server
TEMPLATE = app


SOURCES += main.cpp\
    sqlconnector.cpp \
    adduserdialog.cpp \
    edituserdialog.cpp \
    timeplannerserver.cpp

HEADERS  += sqlconnector.h \
    adduserdialog.h \
    edituserdialog.h \
    task.h \
    role.h \
    signinpasswordtype.h \
    sqlerrorcodes.h \
    sqlqueries.h \
    timeplannerserver.h

RESOURCES += \
    resources.qrc

RC_FILE = application.rc
