#include "timeplannerserver.h"
#include <QApplication>
#include <QtSql/QSqlDatabase>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath("plugins");
    QApplication a(argc, argv);
    TimePlannerServer w;
    w.show();
    return a.exec();
}
