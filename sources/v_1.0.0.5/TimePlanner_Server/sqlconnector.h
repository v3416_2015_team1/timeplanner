#ifndef SQLCONNECTOR_H
#define SQLCONNECTOR_H

#include <QObject>
#include <QMessageBox>
#include <QInputDialog>
#include <QtSql>
#include <QSqlDatabase>
#include <QCryptographicHash>
#include "sqlqueries.h"
#include "sqlerrorcodes.h"
#include "task.h"
#include "role.h"

Q_DECLARE_METATYPE(Task)

class SqlConnector : public QWidget
{
    Q_OBJECT
public:
    SqlConnector(QWidget *parent = 0);
    ~SqlConnector();
    bool connectDB(QString strMysqlLogin,
                   QString strMysqlPassword,
                   QString strDatabaseName,
                   int mysqlPort,
                   QString &strLog);
    void disconnectDB();
    QVector<QVariant> signIn(QString strLogin,
                             QString strPassword,
                             quint32 clientIP,
                             QString &strLog);
    QVector<QVariant> signInWithNewPassword(QString strLogin,
                                            QString strPassword,
                                            quint32 clientIP,
                                            QString &strLog);
    bool checkTempPassword(QString strLogin,
                           QString strPassword,
                           quint32 clientIP,
                           QString &strLog);
    void signOut(QByteArray btarrSession,
                 QString &strLog);
    void forceSignOut(QString strLogin,
                      QString &strLog);
    int checkSession(QByteArray btarrSession,
                     QString &strLog);
    bool createTask(QByteArray btarrSession,
                    QString strCaption,
                    QString strContent,
                    QString strAssignee,
                    QDateTime dateDeadline,
                    QString &strLog);
    bool editTask(QByteArray btarrSession,
                  quint32 taskId,
                  QString strCaption,
                  QString strContent,
                  QString strAssignee,
                  QDateTime dateDeadline,
                  QString &strLog);
    bool editTaskStatus(QByteArray btarrSession,
                        quint32 taskId,
                        QString strStatus,
                        QString &strLog);
    bool getTasks(QByteArray btarrSession,
                  QVector<Task> &vecData,
                  Role role,
                  QString &strLog);
    bool getUsers(QByteArray btarrSession,
                   QVector<QVariant> &vecData,
                   QString &strLog);
    QByteArray getSessionByLogin(QString strLogin,
                                 QString &strLog);
    void adminInsertUser(QString strLogin,
                         QString strPassword,
                         QString strFirstName,
                         QString strSecondName,
                         QString strLastName,
                         QString strPosition,
                         bool createTaskRight,
                         QString &strLog);
    void adminEditUser(QString strOldLogin,
                       QString strNewLogin,
                       QString strNewPassword,
                       QString strFirstName,
                       QString strSecondName,
                       QString strLastName,
                       QString strPosition,
                       bool createTaskRight,
                       QString &strLog);
    void adminDeleteUser(QString strLogin,
                         QString &strLog);
    QVector<QVariant> adminGetUserByLogin(QString strLogin,
                                          QString &strLog);
private:
    QSqlDatabase _sqlDB;
    QByteArray _generateSessionId(quint32 clientIP);
};

#endif // SQLCONNECTOR_H
