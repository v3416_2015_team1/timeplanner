#ifndef TASK_H
#define TASK_H

struct Task
{
    qint32 taskId;
    QString caption;
    QString content;
    QDateTime modified;
    QDateTime deadline;
    QString anotherUserLogin;
    QString status;
};

#endif // TASK_H
