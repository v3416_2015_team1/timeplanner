#include "timeplannerserver.h"
// TODO: automatic sign_out
TimePlannerServer::TimePlannerServer(QWidget *parent)
    : QMainWindow(parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
    setWindowIcon(QIcon(":/Icons/TimePlanne_Server.ico"));
    setWindowTitle(tr("TimePlanner Server"));
    _pCentralWidget = new QWidget;
    _pTxtLog = new QTextEdit;
    _pTxtLog->setReadOnly(true);
    _pSqlConnector = new SqlConnector;
    _pTcpServer = new QTcpServer(this);
    _pVltCentralWidget = new QVBoxLayout;
    _pVltCentralWidget->addWidget(_pTxtLog);
    _pVltCentralWidget->addWidget(_pSqlConnector);
    _pCentralWidget->setLayout(_pVltCentralWidget);
    setCentralWidget(_pCentralWidget);
    _createActions();
    _createMenus();
    _pLblMySQLPort = new QLabel(STATUS_LBL_MYSQL_PORT, this);
    _pLblMySQLUser = new QLabel(STATUS_LBL_MYSQL_USER, this);
    _pLblMySQLDataBase = new QLabel(STATUS_LBL_MYSQL_DATABASE, this);
    _pLblTCPPort = new QLabel(STATUS_LBL_TCP_PORT, this);
    statusBar()->addWidget(_pLblMySQLPort);
    statusBar()->addWidget(_pLblMySQLUser);
    statusBar()->addWidget(_pLblMySQLDataBase);
    statusBar()->addWidget(_pLblTCPPort);
    resize(640, 480);
    _pSystemTrayIcon = new QSystemTrayIcon(this);
    _pSystemTrayIcon->setContextMenu(_pSystemTrayIconContextMenu);
    _pSystemTrayIcon->setToolTip("TimePlanner Server");
    _pSystemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner_Server.ico"));
    _pSystemTrayIcon->show();
    connect(_pSystemTrayIcon, &QSystemTrayIcon::activated,
            this, &TimePlannerServer::onTrayIconActivated);
}

void TimePlannerServer::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}
void TimePlannerServer::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
        showHide();
}
void TimePlannerServer::closeEvent(QCloseEvent *event)
{
    if (_pSystemTrayIcon->isVisible())
        hide();
    event->ignore();
}
void TimePlannerServer::onAppQuit()
{
    QMessageBox::StandardButton confirmQuit;
    confirmQuit = QMessageBox::question(this,
                                        tr("Quit TimePlanner Server"),
                                        tr("Are you sure you want to quit TimePlanner Server?"),
                                        QMessageBox::Yes|QMessageBox::No);
    if (confirmQuit == QMessageBox::Yes)
    {
        logNow(tr("TimePlanner Server closed"));
        QDir logsDir(QDir::currentPath()
                     .append("/logs/"));
        if (!logsDir.exists()) {
            logsDir.mkpath(".");
        }
        _saveLogStr(logsDir.path()
                 .append("/")
                 .append(QDateTime::currentDateTimeUtc().toString("dd.MM.yyyy_hh.mm.ss.zzz"))
                 .append("_TimePlannerServerQuit.log"));
        qApp->quit();
    }
}

void TimePlannerServer::newNotification(QString caption, QString content)
{
   _pSystemTrayIcon->showMessage(caption, content);
}

void TimePlannerServer::_createActions()
{
    _pActionShowHide = new QAction(tr("Show/Hide"), this);
    connect(_pActionShowHide, &QAction::triggered,
            this, &TimePlannerServer::showHide);
    _pActionQuit = new QAction(tr("Quit"), this);
    connect(_pActionQuit,&QAction::triggered,
            this, &TimePlannerServer::onAppQuit);
    _pActionSaveLog = new QAction(tr("&Save"), this);
    _pActionSaveLog->setShortcuts(QKeySequence::Save);
    _pActionSaveLog->setStatusTip(tr("Save the log to disk"));
    connect(_pActionSaveLog, &QAction::triggered,
            this, &TimePlannerServer::_saveLog);
    _pActionPrintLog = new QAction(tr("&Print"), this);
    _pActionPrintLog->setShortcuts(QKeySequence::Print);
    _pActionPrintLog->setStatusTip(tr("Print the log"));
    connect(_pActionPrintLog, &QAction::triggered,
            this, &TimePlannerServer::_printLog);
    _pActionClearLog = new QAction(tr("&Clear"), this);
    _pActionClearLog->setStatusTip(tr("Clear the log"));
    connect(_pActionClearLog, &QAction::triggered,
            this, &TimePlannerServer::_clearLog);
    _pActionConnectMySQL = new QAction(tr("&Connect MySQL"), this);
    _pActionConnectMySQL->setStatusTip(tr("Connect to working MySQL Server"));
    connect(_pActionConnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::_connectMySQL);
    _pActionDisconnectMySQL = new QAction(tr("&Disconnect MySQL"), this);
    _pActionDisconnectMySQL->setStatusTip(tr("Disconnect from working MySQL Server"));
    connect(_pActionDisconnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::_disconnectMySQL);
    _pActionRunTCP = new QAction(tr("&Run TCP"), this);
    _pActionRunTCP->setStatusTip(tr("Run TCP Server"));
    connect(_pActionRunTCP, &QAction::triggered,
            this, &TimePlannerServer::_runTCP);
    _pActionStopTCP = new QAction(tr("&Stop TCP"), this);
    _pActionStopTCP->setStatusTip(tr("Stop TCP Server"));
    connect(_pActionStopTCP, &QAction::triggered,
            this, &TimePlannerServer::_stopTCP);
    _pActionAddUser = new QAction(tr("&Add User"), this);
    _pActionAddUser->setStatusTip(tr("Create new user"));
    _pActionAddUser->setShortcuts(QKeySequence::New);
    connect(_pActionAddUser, &QAction::triggered,
            this, &TimePlannerServer::_addUser);
    _pActionEditUser = new QAction(tr("&Edit User"), this);
    _pActionEditUser->setStatusTip(tr("Edit existing user"));
    _pActionEditUser->setShortcuts(QKeySequence::Find);
    connect(_pActionEditUser, &QAction::triggered,
            this, &TimePlannerServer::_editUser);
    _pActionDeleteUser = new QAction(tr("&Delete User"), this);
    _pActionDeleteUser->setStatusTip(tr("Delete existing user"));
    _pActionDeleteUser->setShortcuts(QKeySequence::Delete);
    connect(_pActionDeleteUser, &QAction::triggered,
            this, &TimePlannerServer::_deleteUser);

    _pActionShowLicense = new QAction(tr("&License"), this);
    _pActionShowLicense->setStatusTip(tr("Show license"));
    connect(_pActionShowLicense, &QAction::triggered,
            this, &TimePlannerServer::_showLicense);
    _pActionShowAbout = new QAction(tr("&About"), this);
    _pActionShowAbout->setStatusTip(tr("About TimePlanner Server"));
    _pActionShowAbout->setShortcuts(QKeySequence::HelpContents);
    connect(_pActionShowAbout, &QAction::triggered,
            this, &TimePlannerServer::_showAbout);
}

void TimePlannerServer::_createMenus()
{
    _pSystemTrayIconContextMenu = new QMenu(this);
    _pSystemTrayIconContextMenu->addAction(_pActionShowHide);
    _pSystemTrayIconContextMenu->addAction(_pActionQuit);
    _pMenuLog = menuBar()->addMenu(tr("&Log"));
    _pMenuLog->addAction(_pActionSaveLog);
    _pMenuLog->addAction(_pActionPrintLog);
    _pMenuLog->addSeparator();
    _pMenuLog->addAction(_pActionClearLog);
    _pMenuMySQL = menuBar()->addMenu(tr("&MySQL"));
    _pMenuMySQL->addAction(_pActionConnectMySQL);
    _pMenuMySQL->addAction(_pActionDisconnectMySQL);
    _pMenuTCP = menuBar()->addMenu(tr("TCP"));
    _pMenuTCP->addAction(_pActionRunTCP);
    _pMenuTCP->addAction(_pActionStopTCP);
    _pMenuUsers = menuBar()->addMenu(tr("Users"));
    _pMenuUsers->addAction(_pActionAddUser);
    _pMenuUsers->addAction(_pActionEditUser);
    _pMenuUsers->addSeparator();
    _pMenuUsers->addAction(_pActionDeleteUser);
    _pMenuAbout = menuBar()->addMenu(tr("About"));
    _pMenuAbout->addAction(_pActionShowLicense);
    _pMenuAbout->addAction(_pActionShowAbout);
    _pMenuMySQL->actions().at(1)->setEnabled(false);
    _pMenuTCP->actions().at(1)->setEnabled(false);
}

void TimePlannerServer::_saveLog()
{
    QString logFileName = QFileDialog::getSaveFileName(this,
                                                       tr("Save log as"),
                                                       "",
                                                       tr("Log files (*.log)"));
    if (!logFileName.isEmpty())
        _saveLogStr(logFileName);
}

void TimePlannerServer::_printLog()
{
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog printDialog(&printer, this);
    printDialog.setWindowTitle(tr("Print log"));
    if (_pTxtLog->textCursor().hasSelection())
        printDialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    if (printDialog.exec() == QDialog::Accepted)
        _pTxtLog->document()->print(&printer);
}

void TimePlannerServer::_clearLog()
{
    QMessageBox::StandardButton confirmClearDialog;
    confirmClearDialog = QMessageBox::question(this,
                                               tr("Clear the log"),
                                               tr("Are you sure you want to clear the log?"),
                                               QMessageBox::Yes|QMessageBox::No);
    if (confirmClearDialog == QMessageBox::Yes)
        _pTxtLog->clear();
}

void TimePlannerServer::_connectMySQL()
{
    QString databaseName;
    QString log_string;
    int MySQL_port = 3306;
    bool getLoginOk = false;
    QString mysql_login = QInputDialog::getText(this,
                                                   tr("Connect to MySQL"),
                                                   tr("MySQL login"),
                                                   QLineEdit::Normal,
                                                   "root",
                                                   &getLoginOk);
    bool getPasswordOk = false;
    QString mysql_password;
    if (getLoginOk)
        mysql_password = QInputDialog::getText(this,
                                               tr("Connect to MySQL"),
                                               tr("MySQL password"),
                                               QLineEdit::Normal,
                                               "",
                                               &getPasswordOk);
    if (getLoginOk && getPasswordOk)
    {
        bool getDatabaseNameOk = false;
        databaseName = QInputDialog::getText(this,
                                               tr("Connect to MySQL"),
                                               tr("MySQL database"),
                                               QLineEdit::Normal,
                                               "timeplanner_db",
                                               &getDatabaseNameOk);
        if (getDatabaseNameOk)
        {
            if(!_pSqlConnector->connectDB(mysql_login,
                                         mysql_password,
                                         databaseName,
                                         MySQL_port,
                                         log_string))
            {
                QMessageBox::critical(0,
                                      tr("Connection to MySQL"),
                                      tr("Unable to establish a database connection."),
                                      QMessageBox::Cancel
                                      );
                logNow(log_string);
                return;
            }
        }
        else
        {
            return;
        }
    }
    else
    {
        return;
    }
    logNow(log_string);
    _pLblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT).append("%1").arg(MySQL_port));
    _pLblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER).append(mysql_login));
    _pLblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE).append(databaseName));
    _pMenuMySQL->actions().at(0)->setEnabled(false);
    _pMenuMySQL->actions().at(1)->setEnabled(true);
    log(tr("Successful connection to MySQL Server"));
}

void TimePlannerServer::_disconnectMySQL()
{
    QMessageBox::StandardButton confirmDisconnectMySQLDialog;
    confirmDisconnectMySQLDialog = QMessageBox::question(this,
                                                         tr("Disconnect from MySQL Server"),
                                                         tr("Are you sure you want to disconnect from MySQL Server?"),
                                                         QMessageBox::Yes|QMessageBox::No);
    if (confirmDisconnectMySQLDialog == QMessageBox::Yes)
    {
        _pSqlConnector->disconnectDB();
        _pLblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT));
        _pLblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER));
        _pLblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE));
        _pMenuMySQL->actions().at(0)->setEnabled(true);
        _pMenuMySQL->actions().at(1)->setEnabled(false);
        logNow("Disconnected from MySQL Server");
    }
}

void TimePlannerServer::_runTCP()
{
    bool tcp_port_selected = false;
    quint16 tcp_port = QInputDialog::getInt(this,
                                            tr("Select TCP port"),
                                            tr("TCP port:"),
                                            INITIAL_PORT,
                                            0,
                                            65535,
                                            1,
                                            &tcp_port_selected);
    if (tcp_port_selected)
    {
        logNow(tr("Starting TCP Server at %1 port.").arg(tcp_port));
        connect(_pTcpServer, &QTcpServer::newConnection,
                this, &TimePlannerServer::newConncetion);
        if (!_pTcpServer->listen(QHostAddress::Any, tcp_port))
        {
            QMessageBox::critical(0,
                                  tr("Cannot start TCP server"),
                                  tr("Unable to start a TCP server."),
                                  QMessageBox::Cancel);
            _pTcpServer->close();
            return;
        }
    }
    else
    {
        return;
    }
    _pLblTCPPort->setText(QString(STATUS_LBL_TCP_PORT).append("%1").arg(tcp_port));
    _pMenuTCP->actions().at(0)->setEnabled(false);
    _pMenuTCP->actions().at(1)->setEnabled(true);
    log(tr("TCP server has been run successfully"));
}

void TimePlannerServer::_stopTCP()
{
    QMessageBox::StandardButton confirmStopTCPDialog;
    confirmStopTCPDialog = QMessageBox::question(this,
                                                 tr("Stop TCP Server"),
                                                 tr("Are you sure you want to stop TCP Server?"),
                                                 QMessageBox::Yes|QMessageBox::No);
    if (confirmStopTCPDialog == QMessageBox::Yes)
    {
        _pTcpServer->close();
        _pLblTCPPort->setText(QString(STATUS_LBL_TCP_PORT));
        _pMenuTCP->actions().at(0)->setEnabled(true);
        _pMenuTCP->actions().at(1)->setEnabled(false);
        logNow(tr("TCP Server stoped"));
    }
}

void TimePlannerServer::_addUser()
{
    AddUserDialog addUserDialog;
    addUserDialog.setModal(true);
    if (addUserDialog.exec())
    {
        QString strLog;
        _pSqlConnector->adminInsertUser(addUserDialog.getLogin(),
                                        addUserDialog.getPassword(),
                                        addUserDialog.getFirstName(),
                                        addUserDialog.getSecondame(),
                                        addUserDialog.getLastName(),
                                        addUserDialog.getPosition(),
                                        addUserDialog.getCreateTaskRight(),
                                        strLog);
        logNow(strLog);
    }
}

void TimePlannerServer::_editUser()
{
    QString strLog;
    bool getLoginOk = false;
    QString login = QInputDialog::getText(this, tr("Edit user"), tr("Login"), QLineEdit::Normal, "", &getLoginOk);
    if (getLoginOk)
    {
        QVector<QVariant> user = _pSqlConnector->adminGetUserByLogin(login, strLog);
        logNow(strLog);
        if (user.empty())
        {
            QMessageBox::warning(0,
                                 tr("Edit user"),
                                 tr("User %1 not found.").arg(login),
                                 QMessageBox::Cancel);
            return;
        }
        else
        {
            EditUserDialog editUserDialog(user);
            editUserDialog.setModal(true);
            if (editUserDialog.exec())
            {
                strLog.clear();
                QByteArray forceSignOutSession = _pSqlConnector->getSessionByLogin(login, strLog);
                if (!forceSignOutSession.isEmpty())
                {
                    QTcpSocket *socket = _mapSockets[forceSignOutSession];
                    if (socket != NULL)
                    {
                        _sendToClient(socket, MSG_FORCE_SIGN_OUT, QVector<QVariant>());
                        socket->waitForBytesWritten();
                        socket->close();
                        _mapSockets.remove(forceSignOutSession);
                    }
                }
                _pSqlConnector->forceSignOut(login,
                                             strLog);
                _pSqlConnector->adminEditUser(login,
                                              editUserDialog.getLogin(),
                                              editUserDialog.getResetedPassword(),
                                              editUserDialog.getFirstName(),
                                              editUserDialog.getSecondame(),
                                              editUserDialog.getLastName(),
                                              editUserDialog.getPosition(),
                                              editUserDialog.getCreateTaskRight(),
                                              strLog);
                logNow(strLog);
            }
        }
    }
}

void TimePlannerServer::_deleteUser()
{
    bool getLoginOk = false;
    QString strLogin = QInputDialog::getText(this,
                                          tr("Delete user"),
                                          tr("Login"),
                                          QLineEdit::Normal,
                                          "",
                                          &getLoginOk);

    if (getLoginOk)
    {
        QString strLog;
        QVector<QVariant> user = _pSqlConnector->adminGetUserByLogin(strLogin,
                                                                     strLog);
        logNow(strLog);
        if (user.empty())
        {
            QMessageBox::warning(0,
                                 tr("Delete user"),
                                 tr("User %1 not found.").arg(strLogin),
                                 QMessageBox::Cancel);
            return;
        }
        else
        {
            QMessageBox::StandardButton confirmDeleteUser;
            confirmDeleteUser = QMessageBox::question(this,
                                                      tr("Delete user"),
                                                      tr("Are you sure you want to delete %1 (%2: %3 %4 %5)?")
                                                      .arg(user[0].toString())
                                                      .arg(user[4].toString())
                                                      .arg(user[3].toString())
                                                      .arg(user[1].toString())
                                                      .arg(user[2].toString()),
                                                      QMessageBox::Yes|QMessageBox::No);
            if (confirmDeleteUser == QMessageBox::Yes)
            {
                strLog.clear();
                QByteArray forceSignOutSession = _pSqlConnector->getSessionByLogin(strLogin,
                                                                                   strLog);
                if (!forceSignOutSession.isEmpty())
                {
                    QTcpSocket *pSocket = _mapSockets[forceSignOutSession];
                    if (pSocket != NULL)
                    {
                        logNow(QString("Force sign out %1 user with %2 session").arg(strLogin).arg(QString(forceSignOutSession)));
                        _sendToClient(pSocket, MSG_FORCE_SIGN_OUT, QVector<QVariant>());
                        pSocket->close();
                        _mapSockets.remove(forceSignOutSession);
                    }
                }
                _pSqlConnector->forceSignOut(strLogin, strLog);
                _pSqlConnector->adminDeleteUser(strLogin, strLog);
                log(strLog);
            }
        }
    }
}

void TimePlannerServer::_showLicense()
{
    QMessageBox::information(this,
                             tr("License"),
                             tr("GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007"),
                             QMessageBox::Ok);
}

void TimePlannerServer::_showAbout()
{
    QMessageBox::information(this,
                             tr("About TimePlanner Server"),
                             tr("This is TimePlanner Server application"),
                             QMessageBox::Ok);
}

void TimePlannerServer::newConncetion()
{
    QTcpSocket *pClientSocket = _pTcpServer->nextPendingConnection();\
    connect(pClientSocket, &QTcpSocket::disconnected,
            pClientSocket, &QTcpSocket::deleteLater);
    connect(pClientSocket, &QTcpSocket::readyRead,
            this, &TimePlannerServer::receiveClientQuery);
    logNow(tr("New connection from %7")
           .arg(pClientSocket->peerAddress().toString()));
    QVector<QVariant> vecData;
    vecData.push_back(QVariant("Server response: Connected."));
    _sendToClient(pClientSocket, MSG_MESSAGE, vecData);
}

void TimePlannerServer::receiveClientQuery()
{
    QTcpSocket *pClientSocket = (QTcpSocket*)sender();
    QDataStream inputDataStream(pClientSocket);
    inputDataStream.setVersion(QDataStream::Qt_5_5);
    qint64 blocksize = 0;
    qint8 clientQueryType;
    logNow(tr("New query from %7")
           .arg(pClientSocket->peerAddress().toString()));
    if (pClientSocket->bytesAvailable() < sizeof(qint64) + sizeof(qint8))
    {
        return;
    }
    QString strLogin;
    QString strPassword, strTempPassword;
    quint32 clientIP = pClientSocket->peerAddress().toIPv4Address();
    QByteArray btarrSession;
    QString strCaption;
    QString strContent;
    QDateTime dateDeadline;
    QString strAssignee;
    QVector<QVariant> vecReturnToClient;
    QString strLog;
    QVector<Task> vecTasks;
    Role role;
    QVariant task;
    qint32 taskId;
    QString strStatus;
    inputDataStream >> blocksize >> clientQueryType;
    switch(clientQueryType)
    {
    case MSG_SIGN_IN:
        inputDataStream >> strLogin >> strPassword;
        if (_pSqlConnector->checkTempPassword(strLogin,
                                              strPassword,
                                              clientIP,
                                              strLog))
        {
            logNow(strLog);
            _sendToClient(pClientSocket,
                          MSG_TEMP_PASSWORD,
                          vecReturnToClient);
            return;
        }
        _clientSignIn(pClientSocket,
                      strLogin,
                      strPassword,
                      NORMAL,
                      clientIP,
                      strLog);
        break;
    case MSG_TEMP_PASSWORD:
        inputDataStream >> strLogin
                        >> strTempPassword
                        >> strPassword;
        if (_pSqlConnector->checkTempPassword(strLogin,
                                              strTempPassword,
                                              clientIP,
                                              strLog))
        {
            _clientSignIn(pClientSocket,
                          strLogin,
                          strPassword,
                          NEW,
                          clientIP,
                          strLog);
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    case MSG_SIGN_OUT:
        inputDataStream >> btarrSession;
        _pSqlConnector->signOut(btarrSession,
                                strLog);
        _mapSockets.remove(btarrSession);
        logNow(tr("Sign out %1")
               .arg(QString(btarrSession.toHex())));
        break;
    case MSG_USERS:
        inputDataStream >> btarrSession;
        if (_pSqlConnector->getUsers(btarrSession,
                                     vecReturnToClient,
                                     strLog))
        {
            _sendToClient(pClientSocket,
                          MSG_USERS,
                          vecReturnToClient);
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    case MSG_TASK_CREATE:
        inputDataStream >> btarrSession
                        >> strAssignee
                        >> strCaption
                        >> strContent
                        >> dateDeadline;
        if (_pSqlConnector->createTask(btarrSession,
                                       strCaption,
                                       strContent,
                                       strAssignee,
                                       dateDeadline,
                                       strLog))
        {
            vecReturnToClient.push_back(QVariant("Task successfully created!"));
            _sendToClient(pClientSocket,
                          MSG_MESSAGE,
                          vecReturnToClient);
            QByteArray btarrNotifySession = _pSqlConnector->getSessionByLogin(strAssignee, strLog);
            logNow(strLog);
            if (!btarrNotifySession.isEmpty())
            {
                QVector<QVariant> vecTask;
                vecTask.push_back(strCaption);
                vecTask.push_back(strContent);
                vecTask.push_back(dateDeadline);
                QMap<QByteArray, QTcpSocket*>::Iterator it = _mapSockets.find(btarrNotifySession);
                if (it != _mapSockets.end())
                {
                    _sendToClient(it.value(),
                                  MSG_NOTIFY_NEW_TASK,
                                  vecTask);
                }
            }
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    case MSG_TASK_EDIT:
        inputDataStream >> btarrSession
                        >> taskId
                        >> strCaption
                        >> strContent
                        >> dateDeadline
                        >> strAssignee;
        if (_pSqlConnector->editTask(btarrSession,
                                     taskId,
                                     strCaption,
                                     strContent,
                                     strAssignee,
                                     dateDeadline,
                                     strLog))
        {
            logNow(strLog);
            vecReturnToClient.push_back(tr("Task edited successfully!"));
            _sendToClient(pClientSocket,
                          MSG_MESSAGE,
                          vecReturnToClient);
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    case MSG_TASK_EDIT_STATUS:
        inputDataStream >> btarrSession
                        >> taskId
                        >> strStatus;
        if (_pSqlConnector->editTaskStatus(btarrSession,
                                           taskId,
                                           strStatus,
                                           strLog))
        {
            logNow(strLog);
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        inputDataStream >> btarrSession;
        if (clientQueryType == MSG_TASK_USER_CREATOR)
            role = CREATOR_ROLE;
        else if (clientQueryType == MSG_TASK_USER_ASSIGNEE)
            role = ASSIGNEE_ROLE;
        if (_pSqlConnector->getTasks(btarrSession,
                                     vecTasks,
                                     role,
                                     strLog))
        {
            vecReturnToClient.push_back(vecTasks.size());
            for (int i = 0; i < vecTasks.size(); ++i)
            {
                task.setValue(vecTasks[i]);
                vecReturnToClient.push_back(task);
            }
            _sendToClient(pClientSocket,
                          clientQueryType,
                          vecReturnToClient);
        }
        else
        {
            vecReturnToClient.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(pClientSocket,
                          MSG_ERROR,
                          vecReturnToClient);
        }
        break;
    default:
        inputDataStream.skipRawData(pClientSocket->bytesAvailable());
    }
}

void TimePlannerServer::_sendToClient(QTcpSocket *pSocket,
                                      quint8 serverAnswerType,
                                      QVector<QVariant> vecData)
{
    if (pSocket->state() == QAbstractSocket::UnconnectedState)
        return;
    QByteArray btarrBlock;
    QDataStream out(&btarrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);
    out << quint64(0)
        << serverAnswerType;
    qint32 tasksNum;
    switch (serverAnswerType)
    {
    case MSG_ERROR:
        logNow(tr("Sending %1 error to %2\n")
            .arg(vecData[0].toInt())
            .arg(pSocket->peerAddress().toString())
                );
        out << vecData[0].toInt()          // Error code
                ;
        break;
    case MSG_SIGN_IN:
        logNow(tr("Sending successful sign in to %1\n")
            .arg(pSocket->peerAddress().toString())
                );
        out << vecData[0].toString()       // First name
            << vecData[1].toString()       // Second name
            << vecData[2].toString()       // Last name
            << vecData[3].toString()       // Position
            << vecData[4].toBool()         // Create tasks right
            << vecData[5].toByteArray()    // Session ID
                ;
        break;
    case MSG_TEMP_PASSWORD:
        break;
    case MSG_SIGN_OUT:
        break;
    case MSG_FORCE_SIGN_OUT:
        break;
    case MSG_MESSAGE:
        logNow(tr("Sending \"%1\" to %2\n")
            .arg(vecData[0].toString())
            .arg(pSocket->peerAddress().toString())
                );
        out << vecData[0].toString()       // Message
                ;
        break;
    case MSG_USERS:
        logNow(tr("Sending users to %1\n")
                .arg(pSocket->peerAddress().toString())
                );
        out << vecData.size();
        for(int i = 0; i < vecData.size(); ++i)
            out << vecData[i].toString();
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        tasksNum = vecData[0].toInt();
        logNow(tr("Sending %1 tasks to %2\n")
            .arg(tasksNum)
            .arg(pSocket->peerAddress().toString()));
        out << tasksNum;
        for (int i = 1; i <= tasksNum; ++i)
            out << vecData[i].value<Task>().taskId
                << vecData[i].value<Task>().caption
                << vecData[i].value<Task>().content
                << vecData[i].value<Task>().modified
                << vecData[i].value<Task>().deadline
                << vecData[i].value<Task>().anotherUserLogin
                << vecData[i].value<Task>().status;
        break;
    case MSG_NOTIFY_NEW_TASK:
        logNow(tr("Sending new task notification to %1").arg(pSocket->peerAddress().toString()));
        out << vecData[0].toString()
            << vecData[1].toString()
            << vecData[2].toString();
        break;
    }
    out.device()->seek(0);
    out << quint64(btarrBlock.size() - sizeof(quint64));
    if (pSocket->isWritable())
        pSocket->write(btarrBlock);
    pSocket->waitForBytesWritten();
}

void TimePlannerServer::log(QString strMsg)
{
    _pTxtLog->append(strMsg);
}

void TimePlannerServer::logNow(QString strMsg)
{
    QDateTime dateNow = QDateTime::currentDateTimeUtc();
    log(dateNow.toString("dd.MM.yyyy hh:mm:ss.zzz %1").arg(strMsg));
}

void TimePlannerServer::_saveLogStr(QString strLogFilename)
{
    QFile logFile(strLogFilename);
    if (logFile.open(QIODevice::WriteOnly))
    {
        QTextStream logStream(&logFile);
        logStream << _pTxtLog->toPlainText();
        logFile.close();
    }
    else
        QMessageBox::warning(0,
                             tr("Save log"),
                             tr("Unable to save %1").arg(strLogFilename),
                             QMessageBox::Cancel);
}

void TimePlannerServer::_clientSignIn(QTcpSocket *pClientSocket,
                                      QString strLogin,
                                      QString strPassword,
                                      SignInPasswordType passwordType,
                                      quint32 clientIP,
                                      QString &strLog)
{
    QVector<QVariant> vecReturnToClient;
    switch (passwordType)
    {
    case NORMAL:
        vecReturnToClient = _pSqlConnector->signIn(strLogin,
                                                   strPassword,
                                                   clientIP,
                                                   strLog);
        break;
    case NEW:
        vecReturnToClient = _pSqlConnector->signInWithNewPassword(strLogin,
                                                                  strPassword,
                                                                  clientIP,
                                                                  strLog);
        break;
    }
    logNow(strLog);
    strLog.clear();
    if (vecReturnToClient.isEmpty())
    {
        // User (with such login and password) not found
        vecReturnToClient.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
        _sendToClient(pClientSocket,
                      MSG_ERROR,
                      vecReturnToClient);
    }
    else
    {
        _mapSockets.insert(vecReturnToClient[5].toByteArray(),
                           pClientSocket);
        _sendToClient(pClientSocket,
                      MSG_SIGN_IN,
                      vecReturnToClient);
    }
}

TimePlannerServer::~TimePlannerServer()
{
}
