#ifndef TASKLINE
#define TASKLINE
#define CREATOR 100
#define ASSIGNEE 101


#include <QWidget>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QDateEdit>
#include <QTextEdit>
#include <QMessageBox>


class TaskLine : public QWidget
{
    Q_OBJECT

public:
    TaskLine(int rowId,
             int taskId,
             int taskType,
             QString strCaption,
             QString strContent,
             QDateTime dateModified,
             QDateTime dateDeadline,
             QString strSssigneeLogin,
             QString strStatus,
             QWidget *parent = 0);
    QGridLayout * getTaskLayout();
    QString getStatusText();
    QString getTaskName();
    QString getDescription();
    QDateTime getDeadline();
    QString getAssignee();
    uint getTaskId();
    ~TaskLine();
protected:
private:
    uint num;
    uint row;
    bool rights;
    QGridLayout *_pGltTask;
    QHBoxLayout *_pHltStatus;
    QPushButton *_pBtnWatchTask;
    QPushButton *_pBtnEditTask;
    QPushButton *_pBtnSaveTask;
    QLineEdit *_pLtxtTaskName;
    QTextEdit *_pTxtTaskDescription;
    QDateEdit *_pDateDeadline;
    QLineEdit *_pLtxtAssignee;
    QPushButton *_pBtnChangeStatus;
    QComboBox *_pCbxStatus;
public slots:
    void onChangeStatusPress();
    void onTaskChanged();
    void showHideDescription();
    void askEditTask();
signals:
    void changeStatusSignal(uint num);
    void taskChanged(uint num, uint row);
};

#endif // TASKLINE

