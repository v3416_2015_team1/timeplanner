#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QApplication>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>
#include <QComboBox>
#include <QDateEdit>
#include <QTextEdit>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QScrollBar>
#include <QRadioButton>
#include "taskline.h"


class PlannerWidget : public QWidget
{
    Q_OBJECT
public:
    PlannerWidget(QWidget *parent = 0);
    QString getTaskUser();
    QString getTaskDescriptionText();
    QString getTaskNameText();
    QString getTaskName(uint rowId);
    QString getDescription(uint rowId);
    QDate getDeadlineDate();
    uint getTasksSize();
    uint getTaskId(uint rowId);
    QDateTime getDeadline(uint rowId);
    QString getStatusText(uint rowId);
    QString getAssignee(uint rowId);
    TaskLine * getTask(uint row);
    void resizeVecTasks(int numTasks);
    void addUserToList(QString &strInfo);
    void cleanLayout(QVBoxLayout *_vltLayout);
    void setUserName(QString strUsername);
    void emitSignalOut();
    void emitRefreshClicked();
    void addTaskLine(int row,
                     int taskId,
                     QString caption,
                     QString content,
                     QDateTime modified,
                     QDateTime deadline,
                     QString assigneeLogin,
                     QString status);
    void setTaskListTypeSwitchVisible(bool visible);
    void setCreateTaskVisible(bool visible);
    ~PlannerWidget();
protected:
    virtual void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *event);
private:
    QVector<TaskLine *> _vecTasks;
    int taskType;
    QSystemTrayIcon *systemTrayIcon;
    QMenu *menu;
    QAction *actionQuit;
    QAction *actionShowHide;
    QVBoxLayout *_pVltBase;
    QVBoxLayout *_pVltTask;
    QHBoxLayout *_pHltControl;
    QHBoxLayout *_pHltButtons;
    QHBoxLayout *_pHltTaskType;
    QGridLayout *_pGltCreate;
    QGroupBox *_pGbxEdit;
    QGroupBox *_pGbxTypeTasks;
    QRadioButton *_pRbtnCreatedTasks;
    QRadioButton *_pRbtnAssigneeTasks;
    QPushButton *_pBtnCreate;
    QPushButton *_pBtnLogOut;
    QPushButton *_pBtnRefresh;
    QPushButton *_pBtnCreateCurrentTask;
    QScrollArea *_pScrollArea;
    QLabel *_pLblUserName;
    QLabel *_pLblCurrentTaskType;
    QLabel *_pLblNameEmployee;
    QLabel *_pLblNewTaskName;
    QLabel *_pLblDescription;
    QLabel *_pLblDeadline;
    QWidget *_pTaskWidget;
    QWidget *_pCreateWidget;
    QSpacerItem *_pSpaserEdit;
    QComboBox *_pCbxEmployees;
    QTextEdit *_pTxtDescription;
    QLineEdit *_pLtxtTaskName;
    QDateEdit *_pDateDeadline;
public slots:
    void onCreatePress();
    void showHide();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason);
    void onMyTaskBPressed();
    void onAssigneeTaskBPressed();
    void logOut();
    void getNewNotification(QString strCaption, QString strContent);
    void onRefreshPress();
    void onAppQuit();
    void askCreateTask();
signals:
    void askDescription(uint );
    void askAssigneeTasks();
    void askCreateTaskSignal();
    void askCreatedTasks();
    void logOutSignal();
    void appQuit();
    void allUsersSignal();
};

#endif // MAINWIDGET
