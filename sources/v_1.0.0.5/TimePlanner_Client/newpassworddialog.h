#ifndef NEWPASSWORDDIALOG_H
#define NEWPASSWORDDIALOG_H

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

class NewPasswordDialog : public QDialog
{
    Q_OBJECT
private:
    QLabel *_pLblNewPassword;
    QLabel *_pLblConfirmNewPassword;
    QLineEdit *_pLtxtNewPassword;
    QLineEdit *_pLtxtConfirmNewPassword;
    QPushButton *_pBtnOk;
    QPushButton *_pBtnCancel;
    QGridLayout *_pGlt;
public:
    NewPasswordDialog(QWidget *parent = 0);
    ~NewPasswordDialog();
    inline QString getNewPassword(){return _pLtxtConfirmNewPassword->text();}
signals:
public slots:
    void onBtnOkClicked();
    void onBtnCancelClicked();
};

#endif // NEWPASSWORDDIALOG_H
