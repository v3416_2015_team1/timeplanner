#ifndef CLIENT_H
#define CLIENT_H
#include "plannerwidget.h"
#include <QErrorMessage>
#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableView>
#include <QComboBox>
#include <QTcpSocket>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDateEdit>
#include <QMap>
#include <QRegExp>
#include "newpassworddialog.h"
#include "taskline.h"

#define CLIENT_ERROR                   0
#define MESSAGE                        10
#define CLIENT_SEND_ASK_USER           1
#define SIGN_OUT                       11
#define TEMP_PASSWORD                  21
#define FORCE_SIGN_OUT                 31
#define CLIENT_SEND_ASK_CREATE_TASK    2
#define CLIENT_SEND_ASK_TASK_CREATOR   12
#define CLIENT_SEND_ASK_TASK_ASSIGNEE  22
#define CLIENT_SEND_ASK_EDIT_TASK      32
#define CLIENT_SEND_ASK_ALL_USER       13
#define CLIENT_SEND_ASK_NOTIFICATION   14
#define SERVER_SEND_NEW_TASK_NOTIFICATION 24
#define CLIENT_SEND_CHANGE_STATUS      52
#define SETTINGS_FILE       "settings.ini"
#define SETTING_SERVER_IP   "server_ip"
#define SETTING_TCP_PORT    "tcp_port"

class Client : public QWidget
{
    Q_OBJECT
public:
    Client(QWidget *parent = 0);
    void startApplication();
    void askForUser();
    void keyPressEvent(QKeyEvent * event);
    void performLogOut();
    void askForUserWithNewPassword(QString strNewPassword);
    void forceSignOut();
    ~Client();
protected:
private:
    QLineEdit *_pLtxtLogin;
    QLineEdit *_pLtxtPassword;
    QVBoxLayout *_pVltBase;
    QHBoxLayout *_pHltControlBtns;
    QHBoxLayout *_pHltLogin;
    QHBoxLayout *_pHltPassword;
    QLabel *_pLblLogin;
    QLabel *_pLblPassword;
    QSpacerItem *_pSpaserButtons;
    QPushButton *_pBtnSignIn;
    QPushButton *_pBtnCancel;
    QTcpSocket *_pSocket;
    PlannerWidget *_pPlannerWidget;
    QByteArray _btarrIdSession;
    QString _strName;
    QString _strFamilyName;
    QString _strStepName;
    QString _strPost;
    QMap<QString, QString> _mapSettings;
public slots:
    void askAllUser();
    void askCreateTask();
    void askEditTask(uint taskId, uint rowId);
    void connectToServer();
    void connected();
    void connectCancel();
    void error(QAbstractSocket::SocketError error);
    void readServerAnswer();
    void onSignInPress();
    void onChangeStatusPress(uint taskId);
    void askForTasksCreator();
    void askForTasksAssignee();
    void onLogOutSignal();
    void onAppQuit();
    void askNotification();
    void readSettings(QString strSettingsFileName);
signals:
    void sendNewNotification(QString, QString);
};

#endif // CLIENT_H
