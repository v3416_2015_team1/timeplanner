#include "taskline.h"

TaskLine::TaskLine(int rowId,
                   int taskId,
                   int taskType,
                   QString strCaption,
                   QString strContent,
                   QDateTime dateModified,
                   QDateTime dateDeadline,
                   QString strAssigneeLogin,
                   QString strStatus,
                   QWidget *parent)
    : QWidget(parent)
{
    num = taskId;
    row = rowId;
    _pGltTask = new QGridLayout;
    _pLtxtTaskName = new QLineEdit;
    _pLtxtTaskName->setReadOnly(true);
    _pLtxtTaskName->setText(strCaption);
    _pGltTask->addWidget(_pLtxtTaskName, 0,0);
    uint yId = 0;
    if (taskType == CREATOR)
    {
        _pLtxtAssignee = new QLineEdit;
        _pLtxtAssignee->setReadOnly(true);
        _pLtxtAssignee->setText(strAssigneeLogin);
        _pGltTask->addWidget(_pLtxtAssignee, 0,1);
        yId = 2;
        _pBtnSaveTask = new QPushButton(tr("Save"));
        _pBtnSaveTask->setToolTip(tr("Save task"));
        _pGltTask->addWidget(_pBtnSaveTask, 0,yId);
        connect(_pBtnSaveTask, &QPushButton::pressed,
                this, &TaskLine::onTaskChanged);
    }
    else
    {
        _pLtxtAssignee = NULL;
        _pBtnSaveTask = NULL;
    }
    _pBtnWatchTask = new QPushButton;
    _pBtnWatchTask->setIcon(QIcon(":/Icons/read.ico"));
    _pBtnWatchTask->setToolTip(tr("Read Task"));
    _pGltTask->addWidget(_pBtnWatchTask, 0,yId + 1);
    connect(_pBtnWatchTask, &QPushButton::pressed, this,
            &TaskLine::showHideDescription);
    _pDateDeadline = new QDateEdit(dateDeadline.date());
    _pDateDeadline->setMinimumDateTime(QDateTime::currentDateTime());
    if (dateDeadline.date() <= QDate::currentDate())
    {
        _pLtxtTaskName->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
        _pDateDeadline->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
    }
    _pDateDeadline->setReadOnly(true);
    _pGltTask->addWidget(_pDateDeadline, 0, yId + 3);
    _pBtnEditTask = new QPushButton();
    _pBtnEditTask->setIcon(QIcon(":/Icons/edit.ico"));
    _pBtnEditTask->setToolTip(tr("Edit Task"));
    _pBtnEditTask->setDisabled(true);
    _pGltTask->addWidget(_pBtnEditTask, 0, yId + 4);
    _pTxtTaskDescription = new QTextEdit(strContent);
    _pTxtTaskDescription->setReadOnly(true);
    _pGltTask->addWidget(_pTxtTaskDescription, 1, 0);
    _pTxtTaskDescription->setVisible(false);
    if (taskType == CREATOR)
    {
        _pBtnEditTask->setDisabled(false);
        connect(_pBtnEditTask, &QPushButton::clicked,
                this, &TaskLine::askEditTask);
    }
    _pBtnChangeStatus = new QPushButton(tr("Change status"));
    _pBtnChangeStatus->setToolTip(tr("Change task status"));
    _pBtnChangeStatus ->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _pCbxStatus = new QComboBox;
    _pCbxStatus->addItem(strStatus);
    _pCbxStatus->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    connect(_pBtnChangeStatus, &QPushButton::clicked,
            this, &TaskLine::onChangeStatusPress);
    _pHltStatus = new QHBoxLayout;
    _pHltStatus->addWidget(_pCbxStatus);
    _pHltStatus->addWidget(_pBtnChangeStatus);
    _pGltTask->addLayout(_pHltStatus, 3, 0, 1, 2, Qt::AlignLeft);
    _pBtnChangeStatus->setVisible(false);
    _pCbxStatus->setVisible(false);
    if (strStatus == "Done" )
    {
        if (taskType == ASSIGNEE)
        {
            _pLtxtTaskName->setVisible(false);
            _pDateDeadline->setVisible(false);
            _pBtnWatchTask->setVisible(false);
            _pBtnEditTask->setVisible(false);
        }
       _pCbxStatus->addItem(tr("In progress"));
    }
    else
        _pCbxStatus->addItem(tr("Done"));
}


void TaskLine::showHideDescription()
{
    if(!_pTxtTaskDescription->isVisible())
    {
        _pTxtTaskDescription->setVisible(true);
        _pBtnChangeStatus->setVisible(true);
        _pCbxStatus->setVisible(true);
        _pBtnWatchTask->setIcon(QIcon(":/Icons/up.ico"));
    }
    else
    {
        _pTxtTaskDescription->setVisible(false);
        _pBtnChangeStatus->setVisible(false);
        _pCbxStatus->setVisible(false);
        _pBtnWatchTask->setIcon(QIcon(":/Icons/read.ico"));
        if (!_pTxtTaskDescription->isReadOnly())
        {
            _pTxtTaskDescription->setReadOnly(true);
        }
        if (!_pLtxtTaskName->isReadOnly())
        {
            _pLtxtTaskName->setReadOnly(true);
        }
        if (!_pDateDeadline->isReadOnly())
        {
            _pDateDeadline->setReadOnly(true);
        }
    }
}

QGridLayout * TaskLine::getTaskLayout()
{
    return _pGltTask;
}

QString TaskLine::getStatusText()
{
    return _pCbxStatus->currentText();
}

QString TaskLine::getTaskName()
{
    return _pLtxtTaskName->text();
}

QString TaskLine::getDescription()
{
    return _pTxtTaskDescription->toPlainText();
}

QDateTime TaskLine::getDeadline()
{
    return _pDateDeadline->dateTime();
}

QString TaskLine::getAssignee()
{
    return _pLtxtAssignee->text();
}

uint TaskLine::getTaskId()
{
    return num;
}


void TaskLine::onChangeStatusPress()
{
    emit changeStatusSignal(this->num);
}

void TaskLine::onTaskChanged()
{
    emit taskChanged(this->num, this->row);
}

void TaskLine::askEditTask()
{
    showHideDescription();
    if (_pTxtTaskDescription->isReadOnly())
    {
        _pTxtTaskDescription->setReadOnly(false);
    }
    if (_pDateDeadline->isReadOnly())
    {
        _pDateDeadline->setReadOnly(false);
    }
    if (_pLtxtTaskName->isReadOnly())
    {
        _pLtxtTaskName->setReadOnly(false);
    }
}

TaskLine::~TaskLine()
{
}
