#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>
#include <QComboBox>
#include <QTcpSocket>
#include <QDateEdit>
#include <QTextEdit>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QScrollBar>
#include <QRadioButton>
#include <vector>
#define CREATOR 100
#define ASSIGNEE 101

class TaskLine : public QWidget
{
    Q_OBJECT
public:
    TaskLine(QWidget *parent = 0);
    uint num;
    QString status;
    QString taskStatus;
    QGridLayout * taskLayout;
    QPushButton * watchTask;
    QPushButton * editTask;
    QLineEdit * task;
    QTextEdit * taskDescription;
    QDateEdit * dateEdit;
    QLineEdit * assignee;

    QHBoxLayout* statusLayout;
    QPushButton * changeStatus;
    QComboBox * statusBox;

    bool rights;
    ~TaskLine();
public slots:
    void onChangeStatusPress();
    void onTextChanged();

    void showHideDescription();
    void askEditTask();
signals:
    void changeStatusSignal(uint);
    void taskDescrChanged(uint);
};

class Widget : public QWidget
{
    Q_OBJECT
private:
    QSystemTrayIcon *systemTrayIcon;
    QMenu *menu;
    QAction *actionQuit;
    QAction *actionShowHide;

    QVBoxLayout * vl;

    QGroupBox * taskGB;
    QHBoxLayout * taskBL;
    QRadioButton * myTaskB;
    QRadioButton * assigneeTaskB;
    QPushButton * showTaskB;

    QSpacerItem *editItem;
    QSpacerItem *taskItem;
    QSpacerItem *nameItem;

    QPushButton * editTask;

    QHBoxLayout *  taskNamesL ;
    QLabel * userName;
    QGridLayout * taskGrid;

    QScrollArea * scrollArea;
    QLabel *curTask;
    QWidget * taskWidget;

    QSpacerItem *statusItem ;
    int taskType;


protected:
    virtual void closeEvent(QCloseEvent *);

public:

    Widget(QWidget *parent = 0);
    QVector<TaskLine *> tasks;
    QHBoxLayout * editLayout;

    int tdHeight;
    void cleanLayout(QGridLayout*);
    void setUserName(QString);
    void addTaskLine(int taskId, QString caption, QString content, QDateTime modified, QDateTime deadline, QString assigneeLogin, QString status);
    //void addCreatorFunctions();

    ~Widget();
signals:
    void askDescription(uint );
    void askAssigneeTasks();
    void askCreatedTasks();
    void logOutSignal();

public slots:
    void showHide();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason);
    void onCheckTask();////
    void onshowTaskBPressed();
    void logOut();
    void getNewNotification(QString, QString);
    //todo void onNewTaskButtonPressed();
};


#endif // MAINWIDGET

