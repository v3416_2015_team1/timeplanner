#include "userdatadialog.h"

UserDataDialog::UserDataDialog(QWidget *parent) : QDialog(parent)
{
    lblUsername = new QLabel(tr("Login"));
    lblPassword = new QLabel(tr("Password"));
    lblFirstName = new QLabel(tr("First name"));
    lblSecondName = new QLabel(tr("Second name"));
    lblLastName = new QLabel(tr("Last name"));
    lblPosition = new QLabel(tr("Position"));
    txtUsername = new QLineEdit;
    txtUsername->setPlaceholderText(tr("Login"));
    txtPassword = new QLineEdit;
    txtPassword->setPlaceholderText(tr("Password"));
    txtPassword->setEchoMode(QLineEdit::Password);
    txtFirstName = new QLineEdit;
    txtFirstName->setPlaceholderText(tr("First name"));
    txtSecondName = new QLineEdit;
    txtSecondName->setPlaceholderText(tr("Second name"));
    txtLastName = new QLineEdit;
    txtLastName->setPlaceholderText(tr("Last name"));
    txtPosition = new QLineEdit;
    txtPosition->setPlaceholderText(tr("Position"));
    btnSave = new QPushButton(tr("Save user"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnSave, &QPushButton::clicked,
            this, &QDialog::accept);
    connect(btnCancel, &QPushButton::clicked,
            this, &QDialog::reject);
    layout = new QGridLayout(this);
    layout->addWidget(lblUsername, 0, 0);
    layout->addWidget(txtUsername, 0, 1);
    layout->addWidget(lblPassword, 1, 0);
    layout->addWidget(txtPassword, 1, 1);
    layout->addWidget(lblFirstName, 2, 0);
    layout->addWidget(txtFirstName, 2, 1);
    layout->addWidget(lblSecondName, 3, 0);
    layout->addWidget(txtSecondName, 3, 1);
    layout->addWidget(lblLastName, 4, 0);
    layout->addWidget(txtLastName, 4, 1);
    layout->addWidget(lblPosition, 5, 0);
    layout->addWidget(txtPosition, 5, 1);
    layout->addWidget(btnSave, 6, 0);
    layout->addWidget(btnCancel, 6, 1);
}

UserDataDialog::~UserDataDialog()
{

}

void UserDataDialog::onBtnSaveClicked()
{
    destroy();
}

void UserDataDialog::onBtnCancelClicked()
{
    destroy();
}
