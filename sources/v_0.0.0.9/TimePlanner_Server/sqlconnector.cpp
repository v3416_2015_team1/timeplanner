#include "sqlconnector.h"

SqlConnector::SqlConnector(QWidget *parent)
    : QWidget(parent)
{

}
SqlConnector::~SqlConnector()
{
    disconnect_db();
}

bool SqlConnector::connect_db(QString mysql_username, QString mysql_password, QString database_name, int mysql_port, QString &log_string)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setPort(mysql_port);
    db.setUserName(mysql_username);
    db.setPassword(mysql_password);
    db.open();
    log_string.append(QString("Establishing database connection:\nDriver name: %1\nMySQL Host: %2\nMySQL Port: %3\nMySQL User: %4\nMySQL Database: %5.\n")
                     .arg(db.driverName())
                     .arg(db.hostName())
                     .arg(db.port())
                     .arg(db.userName())
                     .arg(database_name)
                     );
    QSqlQuery check_connection_query;
    check_connection_query.exec(SQL_USE_UNEXISTING_DATABASE);
    log_string.append(QString("Trying to find running MySQL Server\n(1049 error — server found,\n 2006 error — server not found):\n %1 %2 %3.\n")
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_NO_CONNECTION)
    {
        return false;
    }
    check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
    log_string.append(QString("Trying to find %1 database:\n %2 %3. %4.\n")
                     .arg(database_name)
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_UNKNOWN_DATABASE)
    {
        check_connection_query.exec(QString(SQL_CREATE_DATABASE).arg(database_name));
        log_string.append(QString("Creating new %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
        log_string.append(QString("Use %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_USERS);
        log_string.append(QString("Creating `users` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_SESSIONS);
        log_string.append(QString("Creating `sessions` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASKS);
        log_string.append(QString("Creating `tasks` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASK_STATUS);
        log_string.append(QString("Creating `task_status` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("Done"));
        check_connection_query.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("In progress"));
        // TODO: log_string.append
        check_connection_query.exec(SQL_FILL_DEFAULT_USER);
        log_string.append(QString("Register initial user:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
    }
    return true;
}

void SqlConnector::disconnect_db()
{
    db.close();
}

QVector<QVariant> SqlConnector::sign_in(QString username,
                                        QString password,
                                        quint32 clientIP,
                                        QString &log_string
                                        )
{
    // Get hash of the password
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    // Perform query
    QSqlQuery query_sign_in;

    QString query_string =
            QString(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)
            .arg(username)
            .arg(QString(hash_password.toHex()));
    query_sign_in.exec(query_string);
    log_string.append(QString("Sign in query:\n %1 %2. %3.\n")
                     .arg(query_sign_in.lastError().nativeErrorCode())
                     .arg(query_sign_in.lastError().driverText())
                     .arg(query_sign_in.lastError().databaseText())
                     );
    // Read query answer
    QVector<QVariant> data;
    quint32 user_id;
    if (query_sign_in.next())
    {
        // Generate new session and insert it into sessions database table
        QByteArray session = _generate_session_id(clientIP);
        user_id = query_sign_in.value(0).toInt();
        data.push_back(query_sign_in.value(3)); // First name
        data.push_back(query_sign_in.value(4)); // Second name
        data.push_back(query_sign_in.value(5)); // Last name
        data.push_back(query_sign_in.value(6)); // Position
        data.push_back(session); // Session Id
        query_string =
                QString(SQL_INSERT_NEW_SESSION)
                .arg(QString(session.toHex()))
                .arg(user_id)
                .arg(clientIP)
                ;
        query_sign_in.exec(query_string);
    }
    // If user not found the data vector will be empty.
    return data;
}

void SqlConnector::sign_out(QByteArray session, QString &log_string)
{
    QSqlQuery query_sign_out;
    QString query_string = QString(SQL_DELETE_SESSION)
                            .arg(QString(session.toHex()));
    query_sign_out.exec(query_string);
    log_string.append(QString("Sign out %1 query:\n %2 %3. %4.\n")
                      .arg(QString(session.toHex()))
                      .arg(query_sign_out.lastError().nativeErrorCode())
                      .arg(query_sign_out.lastError().driverText())
                      .arg(query_sign_out.lastError().databaseText())
                      );
}
bool SqlConnector::get_users(QByteArray session, QVector<QVariant> &data, QString &log_string)
{
    QSqlQuery query_get_users;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_GET_USERS);
        query_get_users.exec(query_string);
        log_string.append(QString("Getting users with session %1:\n %2 %3. %4.\n")
                          .arg(QString(session.toHex()))
                          .arg(query_get_users.lastError().nativeErrorCode())
                          .arg(query_get_users.lastError().driverText())
                          .arg(query_get_users.lastError().databaseText())
                          );
        while(query_get_users.next())
        {
            data.push_back( query_get_users.value(0).toString());
        }
    }
    return correct_session;
}

void SqlConnector::insert_user(QString username, QString password, QString firstName, QString secondName, QString lastName, QString position, QString &log_string)
{
    QSqlQuery query_insert_user;
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    QString query_string = QString(SQL_INSERT_USER)
            .arg(username)
            .arg(QString(hash_password.toHex()))
            .arg(firstName)
            .arg(secondName)
            .arg(lastName)
            .arg(position)
            .arg(1)             // TODO: insert rights
            ;
    query_insert_user.exec(query_string);
    log_string.append(QString("Inserting new user:\n %1 %2. %3.\n")
                      .arg(query_insert_user.lastError().nativeErrorCode())
                      .arg(query_insert_user.lastError().driverText())
                      .arg(query_insert_user.lastError().databaseText())
                      );
}

bool SqlConnector::create_task(QByteArray session, QString caption, QString content, QString assignee, QDateTime deadline, QString &log_string)
{
    QSqlQuery query_create_task;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_GET_UID_BY_LOGIN)
                                .arg(assignee);
        query_create_task.exec(query_string);
        qint32 assignee_id;
        while(query_create_task.next())
        {
            assignee_id = query_create_task.value(0).toInt();
        }
        query_string = QString(SQL_INSERT_NEW_TASK)
                                .arg(user_id)
                                .arg(assignee_id)
                                .arg(caption)
                                .arg(content)
                                .arg(1)
                                .arg(deadline.toString("yyyy-MM-dd HH:mm:ss"))
                                ;
        qDebug() << query_string;
        query_create_task.exec(query_string);
        log_string.append(QString("Creating task query with session %1:\n %2 %3. %4.\n")
                          .arg(QString(session.toHex()))
                          .arg(query_create_task.lastError().nativeErrorCode())
                          .arg(query_create_task.lastError().driverText())
                          .arg(query_create_task.lastError().databaseText())
                          );
    }
    return correct_session;
}

bool SqlConnector::get_tasks(QByteArray session, QVector<Task> &data, Role role, QString &log_string)
{
    QSqlQuery query_task_creator;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string;
        switch (role)
        {
        case CREATOR_ROLE:
            query_string = QString(SQL_TASKS_CREATOR).arg(user_id);
            break;
        case ASSIGNEE_ROLE:
            query_string = QString(SQL_TASKS_ASSIGNEE).arg(user_id);
            break;
        }


        query_task_creator.exec(query_string);
        log_string.append(QString("Get tasks from session %1:\n %2 %3. %4.\n")
                          .arg(QString(session.toHex()))
                          .arg(query_task_creator.lastError().nativeErrorCode())
                          .arg(query_task_creator.lastError().driverText())
                          .arg(query_task_creator.lastError().databaseText())
                          );
        while(query_task_creator.next())
        {
            Task task;
            task.task_id = query_task_creator.value(0).toInt();
            task.caption = query_task_creator.value(1).toString();
            task.content = query_task_creator.value(2).toString();
            task.modified = query_task_creator.value(3).toDateTime();
            task.deadline = query_task_creator.value(4).toDateTime();
            task.another_user_login = query_task_creator.value(5).toString();
            task.status = query_task_creator.value(6).toString();
            data.push_back(task);
        }
    }
    return correct_session;
}

QByteArray SqlConnector::_generate_session_id(quint32 clientIP)
{
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream << clientIP << QDateTime::currentMSecsSinceEpoch() << qrand();
    return QCryptographicHash::hash(data, QCryptographicHash::Sha3_256);
}

int SqlConnector::check_session(QByteArray session, QString &log_string)
{
    QSqlQuery query_check_session;
    QString query_string = QString(SQL_CHECK_SESSION)
                            .arg(QString(session.toHex()));
    query_check_session.exec(query_string);
    log_string.append(QString("Checking session %1:\n %2 %3. %4.\n")
                      .arg(QString(session.toHex()))
                      .arg(query_check_session.lastError().nativeErrorCode())
                      .arg(query_check_session.lastError().driverText())
                      .arg(query_check_session.lastError().databaseText())
                      );
    // Returns -1 if wrong session.
    if (query_check_session.lastError().isValid())
        return -1;
    if (query_check_session.next())
        return query_check_session.value(0).toInt();
    else return -1;
}
