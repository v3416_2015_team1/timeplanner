#ifndef USERDATADIALOG_H
#define USERDATADIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>

class UserDataDialog : public QDialog
{
    Q_OBJECT
public:
    UserDataDialog(QWidget *parent = 0);
    ~UserDataDialog();
    QString getUsername(){return txtUsername->text();}
    QString getPassword(){return txtPassword->text();}
    QString getFirstName(){return txtFirstName->text();}
    QString getSecondame(){return txtSecondName->text();}
    QString getLastName(){return txtLastName->text();}
    QString getPosition(){return txtPosition->text();}
signals:
public slots:
private slots:
    void onBtnSaveClicked();
    void onBtnCancelClicked();
private:
    QLabel *lblUsername;
    QLabel *lblPassword;
    QLabel *lblFirstName;
    QLabel *lblSecondName;
    QLabel *lblLastName;
    QLabel *lblPosition;
    QLineEdit *txtUsername;
    QLineEdit *txtPassword;
    QLineEdit *txtFirstName;
    QLineEdit *txtSecondName;
    QLineEdit *txtLastName;
    QLineEdit *txtPosition;
    QPushButton *btnSave;
    QPushButton *btnCancel;
    QGridLayout *layout;

    QString username;
    QString password;
    QString firstName;
    QString secondName;
    QString lastName;
    QString position;
};

#endif // USERDATADIALOG_H
