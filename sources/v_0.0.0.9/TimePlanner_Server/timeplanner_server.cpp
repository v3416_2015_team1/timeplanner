#include "timeplanner_server.h"
// TODO: automatic sign_out
TimePlannerServer::TimePlannerServer(QWidget *parent)
    : QMainWindow(parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());

    setWindowIcon(QIcon(":/Icons/TimePlanne_Server.ico"));
    setWindowTitle(tr("TimePlanner Server"));

    centralWidget = new QWidget;
    txtLog = new QTextEdit;
    txtLog->setReadOnly(true);
    sqlConnector = new SqlConnector;
    tcpServer = new QTcpServer(this);
    vbltCentralWidgetLayout = new QVBoxLayout;
    vbltCentralWidgetLayout->addWidget(txtLog);
    vbltCentralWidgetLayout->addWidget(sqlConnector);
    centralWidget->setLayout(vbltCentralWidgetLayout);
    setCentralWidget(centralWidget);

    _createActions();
    _createMenus();

    lblMySQLPort = new QLabel(STATUS_LBL_MYSQL_PORT, this);
    lblMySQLUser = new QLabel(STATUS_LBL_MYSQL_USER, this);
    lblMySQLDataBase = new QLabel(STATUS_LBL_MYSQL_DATABASE, this);
    lblTCPPort = new QLabel(STATUS_LBL_TCP_PORT, this);
    statusBar()->addWidget(lblMySQLPort);
    statusBar()->addWidget(lblMySQLUser);
    statusBar()->addWidget(lblMySQLDataBase);
    statusBar()->addWidget(lblTCPPort);
}

void TimePlannerServer::_createActions()
{
    actSaveLog = new QAction(tr("&Save"), this);
    actSaveLog->setShortcuts(QKeySequence::Save);
    actSaveLog->setStatusTip(tr("Save the log to disk"));
    connect(actSaveLog, &QAction::triggered,
            this, &TimePlannerServer::saveLog);

    actPrintLog = new QAction(tr("&Print"), this);
    actPrintLog->setShortcuts(QKeySequence::Print);
    actPrintLog->setStatusTip(tr("Print the log"));
    connect(actPrintLog, &QAction::triggered,
            this, &TimePlannerServer::printLog);

    actClearLog = new QAction(tr("&Clear"), this);
    actClearLog->setStatusTip(tr("Clear the log"));
    connect(actClearLog, &QAction::triggered,
            this, &TimePlannerServer::clearLog);

    actConnectMySQL = new QAction(tr("&Connect MySQL"), this);
    actConnectMySQL->setStatusTip(tr("Connect to working MySQL Server"));
    connect(actConnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::connectMySQL);

    actDisconnectMySQL = new QAction(tr("&Disconnect MySQL"), this);
    actDisconnectMySQL->setStatusTip(tr("Disconnect from working MySQL Server"));
    connect(actDisconnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::disconnectMySQL);

    actRunTCP = new QAction(tr("&Run TCP"), this);
    actRunTCP->setStatusTip(tr("Run TCP Server"));
    connect(actRunTCP, &QAction::triggered,
            this, &TimePlannerServer::runTCP);

    actStopTCP = new QAction(tr("&Stop TCP"), this);
    actStopTCP->setStatusTip(tr("Stop TCP Server"));
    connect(actStopTCP, &QAction::triggered,
            this, &TimePlannerServer::stopTCP);

    actAddUser = new QAction(tr("&Add User"), this);
    actAddUser->setStatusTip(tr("Create new user"));
    actAddUser->setShortcuts(QKeySequence::New);
    connect(actAddUser, &QAction::triggered,
            this, &TimePlannerServer::addUser);

    actEditUser = new QAction(tr("&Edit User"), this);
    actEditUser->setStatusTip(tr("Edit existing user"));
    actEditUser->setShortcuts(QKeySequence::Find);
    connect(actEditUser, &QAction::triggered,
            this, &TimePlannerServer::editUser);

    actDeleteUser = new QAction(tr("&Delete User"), this);
    actDeleteUser->setStatusTip(tr("Delete existing user"));
    actDeleteUser->setShortcuts(QKeySequence::Delete);
    connect(actDeleteUser, &QAction::triggered,
            this, &TimePlannerServer::deleteUser);

    actShowLicense = new QAction(tr("&License"), this);
    actShowLicense->setStatusTip(tr("Show license"));
    connect(actShowLicense, &QAction::triggered,
            this, &TimePlannerServer::showLicense);

    actShowAbout = new QAction(tr("&About"), this);
    actShowAbout->setStatusTip(tr("About TimePlanner Server"));
    actShowAbout->setShortcuts(QKeySequence::HelpContents);
    connect(actShowAbout, &QAction::triggered,
            this, &TimePlannerServer::showAbout);
}

void TimePlannerServer::_createMenus()
{
    menuLog = menuBar()->addMenu(tr("&Log"));
    menuLog->addAction(actSaveLog);
    menuLog->addAction(actPrintLog);
    menuLog->addAction(actClearLog);
    menuMySQL = menuBar()->addMenu(tr("&MySQL"));
    menuMySQL->addAction(actConnectMySQL);
    menuMySQL->addAction(actDisconnectMySQL);
    menuTCP = menuBar()->addMenu(tr("TCP"));
    menuTCP->addAction(actRunTCP);
    menuTCP->addAction(actStopTCP);
    menuUsers = menuBar()->addMenu(tr("Users"));
    menuUsers->addAction(actAddUser);
    menuUsers->addAction(actEditUser);
    menuUsers->addSeparator();
    menuUsers->addAction(actDeleteUser);
    menuAbout = menuBar()->addMenu(tr("About"));
    menuAbout->addAction(actShowLicense);
    menuAbout->addAction(actShowAbout);
}

void TimePlannerServer::saveLog()
{

}

void TimePlannerServer::printLog()
{

}

void TimePlannerServer::clearLog()
{
    txtLog->clear();
}

void TimePlannerServer::connectMySQL()
{
    QString log_string;
    QString databaseName = "timeplanner_db";
    int MySQL_port = 3306;
    bool getUsernameOk = false;
    QString mysql_username = QInputDialog::getText(this, tr("Enter MySQL username"), tr("MySQL username"), QLineEdit::Normal, "root", &getUsernameOk);
    bool getPasswordOk = false;
    QString mysql_password = QInputDialog::getText(this, tr("Enter MySQL password"), tr("MySQL password"), QLineEdit::Normal, "", &getPasswordOk);
    if (getUsernameOk && getPasswordOk)
    {
        if(!sqlConnector->connect_db(mysql_username, mysql_password, databaseName, MySQL_port, log_string))
        {
            QMessageBox::critical(0,
                                  qApp->tr("Cannot open database"),
                                  qApp->tr("Unable to establish a database connection.\n"
                                           "Click Cancel to exit."),
                                  QMessageBox::Cancel
                                  );
            log(log_string);
            return;
        }
    }
    else
    {
        return;
    }
    log(log_string);
    lblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT).append("%1").arg(MySQL_port));
    lblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER).append(mysql_username));
    lblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE).append(databaseName));
}

void TimePlannerServer::disconnectMySQL()
{
    sqlConnector->disconnect_db();
    lblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT));
    lblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER));
    lblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE));
}

void TimePlannerServer::runTCP()
{
    bool tcp_port_selected = false;
    quint16 tcp_port = QInputDialog::getInt(this, "Select TCP port", "TCP port:", INITIAL_PORT, 0, 65535,1,&tcp_port_selected);
    if (tcp_port_selected)
    {
        log(QString("Starting TCP Server at %1 port.\n").arg(tcp_port));
        connect(tcpServer, &QTcpServer::newConnection,
                this, &TimePlannerServer::newConncetion);
        if (!tcpServer->listen(QHostAddress::Any, tcp_port))
        {
            QMessageBox::critical(0, qApp->tr("Cannot start TCP server"),
                       qApp->tr("Unable to start a TCP server.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
            tcpServer->close();
            return;
        }
    }
    else
    {
        return;
    }
    lblTCPPort->setText(QString(STATUS_LBL_TCP_PORT).append("%1").arg(tcp_port));
}

void TimePlannerServer::stopTCP()
{
    tcpServer->close();
    lblTCPPort->setText(QString(STATUS_LBL_TCP_PORT));
}

void TimePlannerServer::addUser()
{
    UserDataDialog *userDataDialog = new UserDataDialog;
    userDataDialog->setModal(true);
    if (userDataDialog->exec())
    {
        QString log_string;
        sqlConnector->insert_user(userDataDialog->getUsername(),
                                  userDataDialog->getPassword(),
                                  userDataDialog->getFirstName(),
                                  userDataDialog->getSecondame(),
                                  userDataDialog->getLastName(),
                                  userDataDialog->getPosition(),
                                  log_string);
        log(log_string);
    }
}

void TimePlannerServer::editUser()
{
    QString log_string;
    bool getUsernameOk = false;
    QString username = QInputDialog::getText(this, tr("Enter username"), tr("User"), QLineEdit::Normal, "root", &getUsernameOk);
}

void TimePlannerServer::deleteUser()
{

}

void TimePlannerServer::showLicense()
{

}

void TimePlannerServer::showAbout()
{

}

TimePlannerServer::~TimePlannerServer()
{

}

void TimePlannerServer::newConncetion()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, &QTcpSocket::disconnected,
            clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead,
            this, &TimePlannerServer::receiveClientQuery);
    QDateTime now = QDateTime::currentDateTimeUtc();
    log(QString("%1.%2.%3 %4:%5:%6 New connection from %7")
        .arg(now.date().day())
        .arg(now.date().month())
        .arg(now.date().year())
        .arg(now.time().hour())
        .arg(now.time().minute())
        .arg(now.time().second())
        .arg(clientSocket->peerAddress().toString())
        );
    QVector<QVariant> data;
    data.push_back(QVariant("Server response: Connected."));
    _sendToClient(clientSocket, MSG_MESSAGE, data);
}

void TimePlannerServer::log(QString msg)
{
    txtLog->append(msg);
}

void TimePlannerServer::receiveClientQuery()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QDataStream inputDataStream(clientSocket);
    inputDataStream.setVersion(QDataStream::Qt_5_5);
    qint64 blocksize = 0;
    qint8 clientQueryType;
    QDateTime now = QDateTime::currentDateTimeUtc();
    log(QString("%1.%2.%3 %4:%5:%6 New query from %7")
        .arg(now.date().day())
        .arg(now.date().month())
        .arg(now.date().year())
        .arg(now.time().hour())
        .arg(now.time().minute())
        .arg(now.time().second())
        .arg(clientSocket->peerAddress().toString())
        );
    if (clientSocket->bytesAvailable() < sizeof(qint64) + sizeof(qint8))
    {
        return;
    }
    QString login;
    QString password;
    quint32 clientIP = clientSocket->peerAddress().toIPv4Address();
    QByteArray session;
    QString caption;
    QString content;
    QDateTime deadline;
    QString assignee;
    QVector<QVariant> return_to_client;
    QString log_string;
    QVector<Task> tasks;
    inputDataStream >> blocksize >> clientQueryType;
    switch(clientQueryType)
    {
    case MSG_SIGN_IN:
        qsrand(qrand());
        inputDataStream >> login >> password;
        return_to_client = sqlConnector->sign_in(login, password, clientIP, log_string);
        log(log_string);
        log_string.clear();
        log("Sign in: " + login);
        if (return_to_client.isEmpty())
        {
            // User (with such username and password) not found
            return_to_client.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        else
        {
            _sendToClient(clientSocket, MSG_SIGN_IN, return_to_client);
        }
        break;
    case MSG_SIGN_OUT:
        inputDataStream >> session;
        sqlConnector->sign_out(session, log_string);
        break;
    case MSG_USERS:
        inputDataStream >> session;
        if (sqlConnector->get_users(session, return_to_client, log_string))
        {
            _sendToClient(clientSocket, MSG_USERS, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_CREATE:

        inputDataStream >> session >> assignee >> caption >> content >> deadline;
        if (sqlConnector->create_task(session, caption, content, assignee, deadline, log_string))
        {
            return_to_client.push_back(QVariant("Task successfully created!"));
            _sendToClient(clientSocket, MSG_MESSAGE, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        inputDataStream >> session;
        Role role;
        if (clientQueryType == MSG_TASK_USER_CREATOR)
            role = CREATOR_ROLE;
        else if (clientQueryType == MSG_TASK_USER_ASSIGNEE)
            role = ASSIGNEE_ROLE;
        if (sqlConnector->get_tasks(session, tasks, role, log_string))
        {
            return_to_client.push_back(tasks.size());
            QVariant task;
            for (int i = 0; i < tasks.size(); ++i)
            {
                task.setValue(tasks[i]);
                return_to_client.push_back(task);
            }
            _sendToClient(clientSocket, clientQueryType, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    // TODO: other client query types
    }
}

void TimePlannerServer::_sendToClient(QTcpSocket *socket,
                  quint8 serverAnswerType,
                  QVector<QVariant> data)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
        return;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);
    out << quint64(0) << serverAnswerType;
    qint32 tasks_num;
    switch (serverAnswerType)
    {
    case MSG_ERROR:
        log(QString("Return %1 error to %2\n")
            .arg(data[0].toInt())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toInt()          // Error code
                ;
        break;
    case MSG_SIGN_IN:
        log(QString("Return successful sign in to %1\n")
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // First name
            << data[1].toString()       // Second name
            << data[2].toString()       // Last name
            << data[3].toString()       // Position
            << data[4].toByteArray()    // Session ID
                ;
        break;
    case MSG_SIGN_OUT:
        // Server do not answer on sign out query
        break;
    case MSG_MESSAGE:
        log(QString("Sending \"%1\" to %2\n")
            .arg(data[0].toString())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // Message
                ;
        break;
    case MSG_USERS:
        log(QString("Sending users to %1\n")
                .arg(socket->peerAddress().toString())
                );
        out << data.size();
        for(int i = 0; i < data.size(); ++i)
            out << data[i].toString();
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        tasks_num = data[0].toInt();
        log(QString("Sending %1 tasks to %2\n")
            .arg(tasks_num)
            .arg(socket->peerAddress().toString()));
        out << tasks_num;
        for (int i = 1; i <= tasks_num; ++i)
            out << data[i].value<Task>().task_id
                << data[i].value<Task>().caption
                << data[i].value<Task>().content
                << data[i].value<Task>().modified
                << data[i].value<Task>().deadline
                << data[i].value<Task>().another_user_login
                << data[i].value<Task>().status;
        break;
        // TODO: other server answer types
    }
    // Считаем размер отпрвляемых данных
    out.device()->seek(0);
    out << quint64(block.size() - sizeof(quint64));
    // Отправляем данных
    if (socket->isWritable())
        socket->write(block);
    socket->waitForBytesWritten();
}
