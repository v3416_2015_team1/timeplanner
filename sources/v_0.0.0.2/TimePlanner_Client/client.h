#ifndef CLIENT_H
#define CLIENT_H
#include "mainwidget.h"
#define CLIENT_ERROR                   0
#define CLIENT_SEND_ASK_USER           1
#define CLIENT_SEND_ASK_TASK           2
#define CLIENT_SEND_ASK_EDIT_TASK      3


#include <QErrorMessage>
#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableView>
#include <QComboBox>
#include <QTcpSocket>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDateEdit>

class Client  : public QWidget
{
    Q_OBJECT
private:
protected:

    QVBoxLayout * vl;
    QHBoxLayout * buttons;
    QHBoxLayout *nameLayout;
    QHBoxLayout* passwordLayout;
    QLabel * nameLabel;
    QLabel * passwordLabel;




    QSpacerItem * buttonItem;

    QPushButton * signIn;
    QPushButton * cancel;


    QTcpSocket * socket;

    Widget * mainWidget;
    QByteArray idSession;

    QString name, familyName, stepName; //имя, фамилия, отчество
    QString post;



    QPushButton * editTask;
    QPushButton * quit;


 public:
     Client(QWidget *parent = 0);
     void startApplication();
     void askForUser();
     void askForTasks();
     void askEditTask(/**/);

     QLineEdit * login; //плохо, наверное
     QLineEdit * password;

    ~Client();
public slots:
   void connectToServer();
   void connected();
   void connectCancel();
   void error(QAbstractSocket::SocketError);
   void readServerAnswer();
   void onSignInPress();
   void onEditPress();

};




#endif // CLIENT_H

