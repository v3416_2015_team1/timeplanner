#include "sqlconnector.h"

SqlConnector::SqlConnector(QWidget *parent)
    : QWidget(parent)
{

}
SqlConnector::~SqlConnector()
{
    disconnect_db();
}

bool SqlConnector::connect_db(QString database_name, int mysql_port, QString &log_string)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setPort(mysql_port);
    db.setUserName("root");
    db.open();

    log_string.append(QString("Establishing database connection:\nDriver name: %1\nMySQL Host: %2\nMySQL Port: %3\nMySQL User: %4\nMySQL Database: %5.\n")
                     .arg(db.driverName())
                     .arg(db.hostName())
                     .arg(db.port())
                     .arg(db.userName())
                     .arg(database_name)
                     );
    QSqlQuery check_connection_query;
    check_connection_query.exec(SQL_USE_UNEXISTING_DATABASE);
    log_string.append(QString("Trying to find running MySQL Server\n(1049 error — server found,\n 2006 error — server not found):\n %1 %2 %3.\n")
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_NO_CONNECTION)
    {
        return false;
    }
    check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
    log_string.append(QString("Trying to find %1 database:\n %2 %3. %4.\n")
                     .arg(database_name)
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_UNKNOWN_DATABASE)
    {
        check_connection_query.exec(QString(SQL_CREATE_DATABASE).arg(database_name));
        log_string.append(QString("Creating new %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
        log_string.append(QString("Use %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_USERS);
        log_string.append(QString("Creating `users` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_SESSIONS);
        log_string.append(QString("Creating `sessions` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASKS);
        log_string.append(QString("Creating `tasks` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASK_STATUS);
        log_string.append(QString("Creating `task_status` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_FILL_TABLE_TASK_STATUS);
        // TODO: log_string.append
        check_connection_query.exec(SQL_FILL_DEFAULT_USER);
        log_string.append(QString("Register initial user:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
    }
    return true;
}

void SqlConnector::disconnect_db()
{
    db.close();
}

QVector<QVariant> SqlConnector::sign_in(QString username,
                                        QString password,
                                        quint32 clientIP,
                                        QString &log_string
                                        )
{
    // Get hash of the password
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    // Perform query
    QSqlQuery query;

    QString query_string =
            QString(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)
            .arg(username)
            .arg(QString(hash_password.toHex()));
    query.exec(query_string);
    log_string.append(QString("Sign in query:\n %1 %2. %3.\n")
                     .arg(query.lastError().nativeErrorCode())
                     .arg(query.lastError().driverText())
                     .arg(query.lastError().databaseText())
                     );
    // Read query answer
    QVector<QVariant> data;
    quint32 user_id;
    if (query.next())
    {
        QByteArray session = _generate_session_id(clientIP);
        user_id = query.value(0).toInt();
        data.push_back(query.value(3)); // First name
        data.push_back(query.value(4)); // Second name
        data.push_back(query.value(5)); // Last name
        data.push_back(query.value(6)); // Position
        data.push_back(session); // Session Id
        query_string =
                QString(SQL_INSERT_NEW_SESSION)
                .arg(QString(session.toHex()))
                .arg(user_id)
                .arg(clientIP)
                ;
        query.exec(query_string);
    }
    // If user not found the data vector will be empty.
    return data;
}

QByteArray SqlConnector::_generate_session_id(quint32 clientIP)
{
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream << clientIP << QDateTime::currentMSecsSinceEpoch() << qrand();
    return QCryptographicHash::hash(data, QCryptographicHash::Sha3_256);
}
