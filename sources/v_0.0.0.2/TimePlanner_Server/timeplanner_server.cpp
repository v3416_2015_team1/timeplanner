#include "timeplanner_server.h"

TimePlannerServer::TimePlannerServer(QWidget *parent)
    : QMainWindow(parent)
{

    qsrand(QDateTime::currentDateTime().toTime_t());

    txtLog = new QTextEdit;
    txtLog->setReadOnly(true);
    setCentralWidget(txtLog);

    sqlConnector = new SqlConnector(this);
    tcpServer = new QTcpServer(this);
}

bool TimePlannerServer::connectToMySQL(QString databaseName, int MySQL_port, QString &log_string)
{
    if(!sqlConnector->connect_db(databaseName, MySQL_port, log_string))
    {
        QMessageBox::critical(0, qApp->tr("Cannot open database"),
                   qApp->tr("Unable to establish a database connection.\n"
                            "Click Cancel to exit."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

bool TimePlannerServer::startTCPServer()
{
    bool tcp_port_selected = false;
    quint16 tcp_port = QInputDialog::getInt(this, "Select TCP port", "TCP port:", INITIAL_PORT, 0, 65535,1,&tcp_port_selected);
    if (tcp_port_selected)
    {
        log(QString("Starting TCP Server at %1 port.\n").arg(tcp_port));
        connect(tcpServer, &QTcpServer::newConnection,
                this, &TimePlannerServer::newConncetion);
        if (!tcpServer->listen(QHostAddress::Any, tcp_port))
        {
            QMessageBox::critical(0, qApp->tr("Cannot start TCP server"),
                       qApp->tr("Unable to start a TCP server.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
            tcpServer->close();
            return false;
        }
    }
    else
    {
        return false;
    }
    return true;
}

TimePlannerServer::~TimePlannerServer()
{

}

void TimePlannerServer::newConncetion()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, &QTcpSocket::disconnected,
            clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead,
            this, &TimePlannerServer::receiveClientQuery);
    QDateTime now = QDateTime::currentDateTimeUtc();
    log(QString("%1.%2.%3 %4:%5:%6 New connection from %7")
        .arg(now.date().day())
        .arg(now.date().month())
        .arg(now.date().year())
        .arg(now.time().hour())
        .arg(now.time().minute())
        .arg(now.time().second())
        .arg(clientSocket->peerAddress().toString())
        );
    QVector<QVariant> data;
    data.push_back(QVariant("Server response: Connected."));
    _sendToClient(clientSocket, MSG_MESSAGE, data);
}

void TimePlannerServer::log(QString msg)
{
    txtLog->append(msg);
}

void TimePlannerServer::receiveClientQuery()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QDataStream inputDataStream(clientSocket);
    inputDataStream.setVersion(QDataStream::Qt_5_5);
    qint64 blocksize = 0;
    qint8 clientQueryType;
    QString login;
    QString password;
    quint32 clientIP = clientSocket->peerAddress().toIPv4Address();
    QDateTime now = QDateTime::currentDateTimeUtc();
    log(QString("%1.%2.%3 %4:%5:%6 New query from %7")
        .arg(now.date().day())
        .arg(now.date().month())
        .arg(now.date().year())
        .arg(now.time().hour())
        .arg(now.time().minute())
        .arg(now.time().second())
        .arg(clientSocket->peerAddress().toString())
        );
    if (clientSocket->bytesAvailable() < sizeof(qint64) + sizeof(qint8))
    {
        return;
    }

    inputDataStream >> blocksize >> clientQueryType;
    switch(clientQueryType)
    {
    case MSG_SIGN_IN:
        qsrand(qrand());
        inputDataStream >> login >> password;
        QString log_string;
        QVector<QVariant> user_info = sqlConnector->sign_in(login, password, clientIP, log_string);
        log(log_string);
        log_string.clear();
        log("Sign in: " + login);
        if (user_info.isEmpty())
        {
            // User (with such username and password) not found
            user_info.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, user_info);
        }
        else
        {
            _sendToClient(clientSocket, MSG_SIGN_IN, user_info);
        }
        break;
    // TODO: other client query types
    }
}

void TimePlannerServer::_sendToClient(QTcpSocket *socket,
                  quint8 serverAnswerType,
                  QVector<QVariant> data)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
        return;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);
    out << quint64(0) << serverAnswerType;
    switch (serverAnswerType)
    {
    case MSG_ERROR:
        log(QString("Return %1 error to %2\n")
            .arg(data[0].toInt())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toInt()          // Error code
                ;
        break;
    case MSG_SIGN_IN:
        log(QString("Return successful sign in to %1\n")
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // First name
            << data[1].toString()       // Second name
            << data[2].toString()       // Last name
            << data[3].toString()       // Position
            << data[4].toByteArray()    // Session ID
                ;
        break;
    case MSG_MESSAGE:
        log(QString("Sending \"%1\" to %2\n")
            .arg(data[0].toString())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // Message
                ;
        break;
        // TODO: other server answer types
    }
    // Считаем размер отпрвляемых данных
    out.device()->seek(0);
    out << quint64(block.size() - sizeof(quint64));
    // Отправляем данных
    if (socket->isWritable())
        socket->write(block);
    socket->waitForBytesWritten();
}




