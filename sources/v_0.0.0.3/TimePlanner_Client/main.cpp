#include "client.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath("plugins");
    QApplication a(argc, argv);
    Client cli;
    cli.startApplication();
    return a.exec();
}
