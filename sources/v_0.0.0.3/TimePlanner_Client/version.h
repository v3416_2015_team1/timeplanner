#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             0,0,0,3
#define VER_FILEVERSION_STR         "0.0.0.3\0"

#define VER_PRODUCTVERSION          0,0,0,3
#define VER_PRODUCTVERSION_STR      "0.0.0.3\0"

#define VER_COMPANYNAME_STR         "v3416 2015 Team #1"
#define VER_FILEDESCRIPTION_STR     "Your personal tasks manager"
#define VER_INTERNALNAME_STR        "TimePlanner Client"
#define VER_LEGALCOPYRIGHT_STR      "v3416 2015 Team #1"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "TimePlanner_Client.exe"
#define VER_PRODUCTNAME_STR         "TimePlanneer Client"

#define VER_COMPANYDOMAIN_STR       "https://vk.com/club42621139"

#endif // VERSION_H