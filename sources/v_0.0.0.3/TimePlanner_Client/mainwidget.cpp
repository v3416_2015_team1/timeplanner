#include "mainwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    setWindowIcon(QIcon(":/Icons/icon.ico"));
    setWindowTitle("Time Planner");
    actionShowHide = new QAction("Show/Hide Time Planner", this);
    connect(actionShowHide, &QAction::triggered,
            this, &Widget::showHide);
    actionQuit = new QAction("Quit", this);
    connect(actionQuit,&QAction::triggered,
            this, &QApplication::quit);

    menu = new QMenu(this);
    menu->addAction(actionShowHide);
    menu->addAction(actionQuit);
    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(menu);
    systemTrayIcon->setToolTip("Time Planner");
    systemTrayIcon->setIcon(QIcon(":/Icons/icon.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &Widget::onTrayIconActivated);



    resize(640, 320);
    adjustSize();



    vl = new QVBoxLayout;


    /*editTask = new QPushButton(parent);
    editTask->setIcon(QIcon(":/Icons/edit.ico"));
    editTask->setToolTip("Edit");*/
//    quit = new QPushButton(parent);
//    quit->setIcon(QIcon(":/Icons/exit.ico"));



//    connect(quit, &QPushButton::clicked, this, &QApplication::quit);
    editLayout = new QHBoxLayout;

    userName = new QLabel;
    editLayout->addWidget(userName);   // Получить из базы данных при входе
    editItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    editLayout->addSpacerItem(editItem);
 //   editLayout->addWidget(editTask);
   // editLayout->addWidget(quit);

    vl->addLayout(editLayout);


    taskTable = new QTableWidget;
    taskTable->setColumnCount(2);

    taskTable->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    taskTable->setHorizontalHeaderLabels(QString("Current Task;DeadLine").split(";"));

   // taskTable->show();


    vl->addWidget(taskTable);

    statusLabel = new QLabel(" Status");
    status = new QComboBox;
    statusLayout = new QHBoxLayout;

    statusLayout->addWidget(statusLabel);
    statusLayout->addWidget(status);
    statusItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    statusLayout->addSpacerItem(statusItem);

    vl->addLayout(statusLayout);
    newTask = new QPushButton("New Task");
    QHBoxLayout * newTaskLayout = new QHBoxLayout;
    taskItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    newTaskLayout->addSpacerItem(taskItem);

    newTaskLayout->addWidget(newTask);

    vl->addLayout(newTaskLayout);
    this->setLayout(vl);
    //newTask->resize(50, 10);

}

void Widget::setUserName(QString username)
{
    userName->setText(username);
}

void Widget::closeEvent(QCloseEvent *event)
{
    if (systemTrayIcon->isVisible())
    {
        hide();
    }
    event->ignore();
}

void Widget::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}

void Widget::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
    {
        showHide();
    }
}

void Widget::onCheckTask()
{
   // todo заполнить комбо бокс
}


Widget::~Widget()
{

}
