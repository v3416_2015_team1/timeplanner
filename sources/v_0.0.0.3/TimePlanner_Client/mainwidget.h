#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>
#include <QComboBox>
#include <QTcpSocket>
#include <QDesktopWidget>
#include <QMessageBox>


class Widget : public QWidget
{
    Q_OBJECT
private:
    QSystemTrayIcon *systemTrayIcon;
    QMenu *menu;
    QAction *actionQuit;
    QAction *actionShowHide;

    QVBoxLayout * vl;


    QHBoxLayout* statusLayout;
    QLabel * statusLabel;
    QComboBox * status;

    QSpacerItem *editItem;
    QSpacerItem *taskItem;
    QSpacerItem *statusItem;


    QPushButton * newTask;
    QPushButton * editTask;
    //QPushButton * save;
//    QPushButton * quit;
    QLabel * userName;
protected:
    virtual void closeEvent(QCloseEvent *);

public:

    Widget(QWidget *parent = 0);

    QTableWidget * taskTable;
    QHBoxLayout * editLayout;

    void setUserName(QString);
    ~Widget();

public slots:
    void showHide();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason);
    void onCheckTask();////
    //todo void onNewTaskButtonPressed();
};


#endif // MAINWIDGET

