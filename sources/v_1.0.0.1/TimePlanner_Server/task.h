#ifndef TASK
#define TASK
#include <QString>
#include <QDateTime>
struct User
{
    QString first_name;
    QString second_name;
    QString last_name;
    QString position;
};

struct Task
{
    User creator;
    User assignee;
    QString caption;
    QString content;
    QString status;
    uint created;
    uint deadline;
    uint modified;
};

#endif // TASK

