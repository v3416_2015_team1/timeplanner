#include "edituserdialog.h"

EditUserDialog::EditUserDialog(QVector<QString> &initData)
{
    setWindowTitle(tr("Edit user"));
    quint8 layoutRow = 0;
    lblLogin = new QLabel(tr("Login"));
    lblFirstName = new QLabel(tr("First name"));
    lblSecondName = new QLabel(tr("Second name"));
    lblLastName = new QLabel(tr("Last name"));
    lblPosition = new QLabel(tr("Position"));
    txtLogin = new QLineEdit;
    txtLogin->setPlaceholderText(tr("Login"));
    txtLogin->setText(initData[layoutRow++]);
    txtFirstName = new QLineEdit;
    txtFirstName->setPlaceholderText(tr("First name"));
    txtFirstName->setText(initData[layoutRow++]);
    txtSecondName = new QLineEdit;
    txtSecondName->setPlaceholderText(tr("Second name"));
    txtSecondName->setText(initData[layoutRow++]);
    txtLastName = new QLineEdit;
    txtLastName->setPlaceholderText(tr("Last name"));
    txtLastName->setText(initData[layoutRow++]);
    txtPosition = new QLineEdit;
    txtPosition->setPlaceholderText(tr("Position"));
    txtPosition->setText(initData[layoutRow++]);
    btnSave = new QPushButton(tr("Save user"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnSave, &QPushButton::clicked,
            this, &EditUserDialog::onBtnSaveClicked);
    connect(btnCancel, &QPushButton::clicked,
            this, &EditUserDialog::onBtnCancelClicked);
    layoutRow = 0;
    layout = new QGridLayout(this);
    layout->addWidget(lblLogin, layoutRow, 0);
    layout->addWidget(txtLogin, layoutRow++, 1);
    layout->addWidget(lblFirstName, layoutRow, 0);
    layout->addWidget(txtFirstName, layoutRow++, 1);
    layout->addWidget(lblSecondName, layoutRow, 0);
    layout->addWidget(txtSecondName, layoutRow++, 1);
    layout->addWidget(lblLastName, layoutRow, 0);
    layout->addWidget(txtLastName, layoutRow++, 1);
    layout->addWidget(lblPosition, layoutRow, 0);
    layout->addWidget(txtPosition, layoutRow++, 1);
    layout->addWidget(btnSave, layoutRow, 0);
    layout->addWidget(btnCancel, layoutRow++, 1);
}

EditUserDialog::~EditUserDialog()
{

}

void EditUserDialog::onBtnSaveClicked()
{
    if (txtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Login"),
                            tr("No login entered."),
                            QMessageBox::Ok
                            );
        return;
    }
    this->accept();
}

void EditUserDialog::onBtnCancelClicked()
{
    this->reject();
}
