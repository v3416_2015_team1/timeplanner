#include "mainwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    taskType = ASSIGNEE;

    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle("Time Planner");
    actionShowHide = new QAction("Show/Hide Time Planner", this);
    connect(actionShowHide, &QAction::triggered,
            this, &Widget::showHide);
    actionQuit = new QAction("Quit", this);
    connect(actionQuit,&QAction::triggered,
            this, &Widget::onAppQuit);

    menu = new QMenu(this);
    menu->addAction(actionShowHide);
    menu->addAction(actionQuit);
    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(menu);
    systemTrayIcon->setToolTip("Time Planner");
    systemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &Widget::onTrayIconActivated);

    resize(640, 320);

    adjustSize();
    vl = new QVBoxLayout(this);
    QGroupBox * edit = new QGroupBox(this);
    editLayout = new QHBoxLayout;
    userName = new QLabel;
    QFont f( "Calibri", 10, QFont::Bold);
    userName->setFont(f);
    editLayout->addWidget(userName);
    editItem = new QSpacerItem(1,0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    editLayout->addSpacerItem(editItem);

    edit->setLayout(editLayout);
    vl->addWidget(edit);
    QGroupBox * task = new QGroupBox(this);
    taskNamesL = new QHBoxLayout;

    curTask = new QLabel("Assigned tasks");
    curTask->setFont(f);
    taskNamesL->addWidget(curTask);
    task->setLayout(taskNamesL);
    vl->addWidget(task);


    scrollArea = new QScrollArea(this);
    vl->addWidget(scrollArea);
    taskWidget = new QWidget(this);
    taskVBox = new QVBoxLayout(taskWidget);
    taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
    taskWidget->setLayout(taskVBox);

    scrollArea->setWidget(taskWidget);
    scrollArea->setWidgetResizable(true);

    scrollArea->setGeometry(0,320,640,200);
    scrollArea->adjustSize();
    scrollArea->show();

    taskGB = new QGroupBox(this);
    taskBL = new QHBoxLayout;
    myTaskB = new QRadioButton("Created tasks");
    assigneeTaskB = new QRadioButton("Assigneed tasks");
    showTaskB = new QPushButton("Show");
    connect(showTaskB, &QPushButton::clicked, this, Widget::onshowTaskBPressed);
    QSpacerItem * taskBItem = new QSpacerItem(350,0, QSizePolicy::Fixed, QSizePolicy::Minimum);
    taskBL->addSpacerItem(taskBItem);
    taskBL->addWidget(showTaskB);
    taskBL->addWidget(myTaskB);
    taskBL->addWidget(assigneeTaskB);

    taskGB->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Minimum);
    taskGB->setLayout(taskBL);

    vl->addWidget(taskGB);
    this->setLayout(vl);
}

void Widget::logOut()
{
    taskType = ASSIGNEE;
    for (int i = 0; i < tasks.size(); i++)
    {
        delete tasks[i];
    }
    tasks.clear();
    scrollArea->update();
    emit logOutSignal();
}

void Widget::setUserName(QString username)
{
    userName->setText(username);
}


void clearLayout(QLayout *layout)
{
    QLayoutItem *child;
    while ((child = layout->takeAt(0)) != 0)
  {
        if(child->layout() != 0)
        clearLayout( child->layout() );
        else if(child->widget() != 0)
         delete child->widget();
         delete child;
     }

}

void Widget::cleanLayout(QVBoxLayout * layout)
{
    QLayoutItem *child;
    while ((child = layout->takeAt(0)) != 0)
  {
        if(child->layout() != 0)
            clearLayout( child->layout() );
        else if(child->widget() != 0)
            delete child->widget();
        delete child;
     }
    for (int i = 0; i < tasks.size(); i++)
    {
        delete tasks[i];
    }
    tasks.clear();
}

void Widget::onRefreshPress()
{
    if (taskType == CREATOR)
    {
        cleanLayout(taskVBox);
        delete taskWidget->layout();
        taskVBox = NULL;
        taskVBox = new QVBoxLayout(taskWidget);
        taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
        taskWidget->setLayout(taskVBox);
        emit askCreatedTasks();
    }
    else if (taskType == ASSIGNEE)
    {
        cleanLayout(taskVBox);
        delete taskWidget->layout();
        taskVBox = NULL;
        taskVBox = new QVBoxLayout(taskWidget);
        taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
        taskWidget->setLayout(taskVBox);
        emit askAssigneeTasks();
    }
}

void Widget::onshowTaskBPressed()
{
    if (myTaskB->isChecked())
    {
        if (taskType != CREATOR)
        {
            taskType = CREATOR;
            curTask->setText("Created tasks");
            cleanLayout(taskVBox);
            delete taskWidget->layout();
            taskVBox = NULL;
            taskVBox = new QVBoxLayout(taskWidget);
            taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
            taskWidget->setLayout(taskVBox);
            emit askCreatedTasks();
        }

    }
    else if (assigneeTaskB->isChecked())
    {
        if (taskType != ASSIGNEE)
        {
            taskType = ASSIGNEE;
            curTask->setText("Assigned tasks");
            cleanLayout(taskVBox);
            delete taskWidget->layout();
            taskVBox = NULL;
            taskVBox = new QVBoxLayout(taskWidget);
            taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
            taskWidget->setLayout(taskVBox);
            emit askAssigneeTasks();
        }

    }
}

void Widget::addTaskLine(int row, int taskId, QString caption, QString content, QDateTime modified, QDateTime deadline, QString assigneeLogin, QString status)
{

     tasks[row] = new TaskLine;
     tasks[row]->num = taskId;

     tasks[row]->taskLayout = new QGridLayout;


     tasks[row]->task = new QLineEdit;
     tasks[row]->task->setReadOnly(true);
     tasks[row]->task->setText(caption);
     tasks[row]->taskLayout->addWidget(tasks[row]->task, 0,0);
     int y_id = 0;
     if (taskType == CREATOR)
     {
         tasks[row]->assignee = new QLineEdit;
         tasks[row]->assignee->setReadOnly(true);
         tasks[row]->assignee->setText(assigneeLogin);
         tasks[row]->taskLayout->addWidget(tasks[row]->assignee, 0,1);
         y_id = 1;
     }

     tasks[row]->watchTask = new QPushButton;
     tasks[row]->watchTask->setIcon(QIcon(":/Icons/read.ico"));
     tasks[row]->watchTask->setToolTip("Read Task");
     tasks[row]->taskLayout->addWidget(tasks[row]->watchTask, 0,y_id + 1);

     connect(tasks[row]->watchTask, &QPushButton::pressed, tasks[row], &TaskLine::showHideDescription);

     tasks[row]->dateEdit = new QDateEdit(deadline.date());
     if (deadline.date() <= QDate::currentDate())
     {
         tasks[row]->task->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
         tasks[row]->dateEdit->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
     }
     tasks[row]->dateEdit->setReadOnly(true);
     tasks[row]->taskLayout->addWidget(tasks[row]->dateEdit, 0, y_id + 2);

     tasks[row]->editTask = new QPushButton();
     tasks[row]->editTask->setIcon(QIcon(":/Icons/edit.ico"));
     tasks[row]->editTask->setToolTip("Edit Task");
     tasks[row]->editTask->setDisabled(true);
     tasks[row]->taskLayout->addWidget(tasks[row]->editTask, 0, y_id + 3);

     tasks[row]->taskDescription = new QTextEdit(content);
     tasks[row]->taskDescription->setReadOnly(true);
     tasks[row]->taskLayout->addWidget(tasks[row]->taskDescription, 1, 0);
     tasks[row]->taskDescription->setVisible(false);


     if (taskType == CREATOR)
     {
          tasks[row]->editTask->setDisabled(false);
          tasks[row]->taskDescription->append(modified.toString());
          connect(tasks[row]->editTask, &QPushButton::clicked, tasks[row], &TaskLine::askEditTask);
          connect(tasks[row]->taskDescription, SIGNAL(textChanged()), tasks[row], SLOT(onTextChanged()));
     }


     tasks[row]->changeStatus = new QPushButton("Change status");
     tasks[row]->changeStatus->setToolTip("Change task status");
     tasks[row]->changeStatus ->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
     tasks[row]->statusBox = new QComboBox;

     tasks[row]->statusBox->addItem(status);
     if (status == "Done")
            tasks[row]->statusBox->addItem(tr("In process"));
     else
            tasks[row]->statusBox->addItem(tr("Done"));


     tasks[row]->statusBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
     connect(tasks[row]->changeStatus, &QPushButton::clicked,tasks[row], &TaskLine::onChangeStatusPress);


     tasks[row]->statusLayout = new QHBoxLayout;
     tasks[row]->statusLayout->addWidget(tasks[row]->changeStatus);
     tasks[row]->statusLayout->addWidget(tasks[row]->statusBox);

     statusItem = new QSpacerItem(150,1, QSizePolicy::Fixed, QSizePolicy::Minimum);
     tasks[row]->statusLayout->addSpacerItem(statusItem);

     tasks[row]->taskLayout->addLayout(tasks[row]->statusLayout,3,0);
     tasks[row]->changeStatus->setVisible(false);
     tasks[row]->statusBox->setVisible(false);

     taskVBox->addLayout(tasks[row]->taskLayout);
}

void Widget::getNewNotification(QString caption, QString content)
{
//   QString content = "You have ";
//   content += tn;
//   if ( tn.toInt() == 1)
//          content += "new task!\n";
//   else
//    content += "new tasks!\n";
//    content += "Please, press <Refresh>!";
//   QString caption = "New Tasks";

   systemTrayIcon->showMessage(caption, content);
}

void Widget::closeEvent(QCloseEvent *event)
{
    if (systemTrayIcon->isVisible())
    {
        hide();
    }
    event->ignore();
}

void Widget::keyPressEvent(QKeyEvent * e)
{
    QWidget::keyPressEvent(e);
    if ( e->key() == Qt::Key_Escape)//Esc
    {
        if (systemTrayIcon->isVisible())
        {
            hide();
        }
        e->ignore();
    }
}

TaskLine::TaskLine(QWidget *parent)
    : QWidget(parent)
{
}
TaskLine::~TaskLine()
{
}


void TaskLine::showHideDescription()
{
   if(!taskDescription->isVisible())
   {
       taskDescription->setVisible(true);
       changeStatus->setVisible(true);
       statusBox->setVisible(true);
       watchTask->setIcon(QIcon(":/Icons/up.ico"));
   }
   else
   {
       taskDescription->setVisible(false);
       changeStatus->setVisible(false);
       statusBox->setVisible(false);
       watchTask->setIcon(QIcon(":/Icons/read.ico"));
       if ( !taskDescription->isReadOnly())
       {
           taskDescription->setReadOnly(true);
       }
       if ( !task->isReadOnly())
       {
           task->setReadOnly(true);
       }
   }
}

void TaskLine::onChangeStatusPress()
{
    emit changeStatusSignal(this->num);
}

void TaskLine::onTextChanged()
{
    emit taskDescrChanged(this->num);
}


void TaskLine::askEditTask()
{
   showHideDescription();
   if ( taskDescription->isReadOnly())
   {
        taskDescription->setReadOnly(false);
    }
    if ( task->isReadOnly())
    {
        task->setReadOnly(false);
    }

}

void Widget::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}

void Widget::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
    {
        showHide();
    }
}

void Widget::onAppQuit()
{
    taskType = ASSIGNEE;
    cleanLayout(taskVBox);

    delete taskWidget->layout();
    taskVBox = NULL;
 /*
    for (int i = 0; i < tasks.size(); i++)
    {
        delete tasks[i];
    }
    tasks.clear();
    scrollArea->update();*/
    emit appQuit();
}

Widget::~Widget()
{

}
