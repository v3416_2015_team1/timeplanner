#ifndef CLIENT_H
#define CLIENT_H
#include "mainwidget.h"
#define CLIENT_ERROR                   0
#define MESSAGE                        10//

#define CLIENT_SEND_ASK_USER           1
#define SIGN_OUT                       11

#define CLIENT_SEND_ASK_CREATE_TASK    2
#define CLIENT_SEND_ASK_TASK_CREATOR   12
#define CLIENT_SEND_ASK_TASK_ASSIGNEE  22//
#define CLIENT_SEND_ASK_EDIT_TASK      32
#define CLIENT_SEND_ASK_ALL_USER       13
#define CLIENT_SEND_ASK_NOTIFICATION   14
#define SERVER_SEND_NEW_TASK_NOTIFICATION 24
#define SERVER_SEND_USER_DELETE        30
#define SERVER_SEND_USER_EDITED        31

#include <QErrorMessage>
#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableView>
#include <QComboBox>
#include <QTcpSocket>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDateEdit>
#include <QTimer>

class Client  : public QWidget
{
    Q_OBJECT
private:
protected:
    void keyPressEvent(QKeyEvent *);

    //QTimer * timer;
    QVBoxLayout * vl;
    QHBoxLayout * buttons;
    QHBoxLayout *nameLayout;
    QHBoxLayout* passwordLayout;
    QLabel * nameLabel;
    QLabel * passwordLabel;

    QSpacerItem * buttonItem;

    QPushButton * signIn;
    QPushButton * cancel;

    QTcpSocket * socket;

    Widget * mainWidget;
    QByteArray idSession;

    QString name, familyName, stepName; //имя, фамилия, отчество
    QString post;

    QPushButton * refresh;
    QPushButton * create;
    QPushButton * quit;
    QWidget * createWidget;

 public:
     Client(QWidget *parent = 0);
     void startApplication();
     void askAllUser();
     void askForUser();


     void askCreateTask(/**/);

     QLineEdit * login; //плохо, наверное
     QLineEdit * password;

     QComboBox * employees;
     QTextEdit * descr;
     QLineEdit * nametask;
     QDateEdit * date;

    ~Client();
public slots:
   void askEditTask(uint);
   void connectToServer();
   void connected();
   void connectCancel();
   void error(QAbstractSocket::SocketError);
   void readServerAnswer();
   void onSignInPress();

   void onCreatePress();
   void onChangeStatusPress(uint);
   void askForTasksCreator();
   void askForTasksAssignee();
   void onLogOutSignal();
   void onAppQuit();
    void askNotification();
signals:

   void sendNewNotification(QString, QString);

};




#endif // CLIENT_H


