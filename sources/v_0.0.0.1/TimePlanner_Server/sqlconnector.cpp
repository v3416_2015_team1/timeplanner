#include "sqlconnector.h"

SqlConnector::SqlConnector(QWidget *parent)
    : QWidget(parent)
{

}
SqlConnector::~SqlConnector()
{
    disconnect_db();
}

bool SqlConnector::connect_db(QString database_name, int mysql_port)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setPort(mysql_port);
    db.setUserName("root");
    db.open();

    QSqlQuery check_connection_query;
    check_connection_query.exec(SQL_USE_UNEXISTING_DATABASE);
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_NO_CONNECTION)
    {
        return false;
    }
    check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_UNKNOWN_DATABASE)
    {
        check_connection_query.exec(QString(SQL_CREATE_DATABASE).arg(database_name));
        check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
        check_connection_query.exec(SQL_CREATE_TABLE_USERS);
        check_connection_query.exec(SQL_CREATE_TABLE_SESSIONS);
        check_connection_query.exec(SQL_CREATE_TABLE_TASKS);
        check_connection_query.exec(SQL_CREATE_TABLE_TASK_STATUS);
        check_connection_query.exec(SQL_FILL_TABLE_TASK_STATUS);
        check_connection_query.exec(SQL_FILL_DEFAULT_USER);
    }
    return true;
}

void SqlConnector::disconnect_db()
{
    db.close();
}

QVector<QVariant> SqlConnector::sign_in(QString username,
                                              QString password,
                                              quint32 clientIP)
{
    // Get hash of the password
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    // Perform query
    QSqlQuery query;

    QString query_string =
            QString(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)
            .arg(username)
            .arg(QString(hash_password.toHex()));
    query.exec(query_string);
    // Read query answer
    QVector<QVariant> data;
    quint32 user_id;
    if (query.next())
    {
        QByteArray session = _generate_session_id(clientIP);
        user_id = query.value(0).toInt();
        data.push_back(query.value(3)); // First name
        data.push_back(query.value(4)); // Second name
        data.push_back(query.value(5)); // Last name
        data.push_back(query.value(6)); // Position
        data.push_back(session); // Session Id
        query_string =
                QString(SQL_INSERT_NEW_SESSION)
                .arg(QString(session.toHex()))
                .arg(user_id)
                .arg(clientIP)
                ;
        query.exec(query_string);
    }
    // If user not found the data vector will be empty.
    return data;
}

QByteArray SqlConnector::_generate_session_id(quint32 clientIP)
{
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream << clientIP << QDateTime::currentMSecsSinceEpoch() << qrand();
    return QCryptographicHash::hash(data, QCryptographicHash::Sha3_256);
}
