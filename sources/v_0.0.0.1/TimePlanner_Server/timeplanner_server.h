#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>

#include <QInputDialog>
#include "sqlconnector.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <QDateTime>

#define INITIAL_PORT            3333
#define SERVER_SEND_PART_NOTIFY 1

#define MSG_ERROR       0
#define MSG_SIGN_IN     1
#define MSG_MESSAGE     10

#define ERROR_SIGN_IN_FAILURE 4001




class TimePlannerServer : public QMainWindow
{
    Q_OBJECT
    SqlConnector *sqlConnector;
    QTcpServer *tcpServer;
    bool _failed;
    void _sendToClient(QTcpSocket *socket,
                      quint8 reason,
                      QVector<QVariant> data);
public:
    TimePlannerServer(QWidget *parent = 0);
    ~TimePlannerServer();
    bool failed();

public slots:
    void newConncetion();
    void receiveClientQuery();
};

#endif // MAINWINDOW_H
