#include "timeplanner_server.h"

TimePlannerServer::TimePlannerServer(QWidget *parent)
    : QMainWindow(parent)
{
    _failed = false;
    qsrand(QDateTime::currentDateTime().toTime_t());
    sqlConnector = new SqlConnector(this);
    if(!sqlConnector->connect_db("timeplanner_db", 3306))
    {
        QMessageBox::critical(0, qApp->tr("Cannot open database"),
                   qApp->tr("Unable to establish a database connection.\n"
                            "Click Cancel to exit."), QMessageBox::Cancel);
        _failed = true;
        return;
    }
    bool tcp_port_selected = false;
    quint16 tcp_port = QInputDialog::getInt(this, "Select TCP port", "TCP port:", INITIAL_PORT, 0, 65535,1,&tcp_port_selected);
    if (tcp_port_selected)
    {
        tcpServer = new QTcpServer(this);
        connect(tcpServer, &QTcpServer::newConnection,
                this, &TimePlannerServer::newConncetion);
        if (!tcpServer->listen(QHostAddress::Any, tcp_port))
        {
            QMessageBox::critical(0, qApp->tr("Cannot start TCP server"),
                       qApp->tr("Unable to start a TCP server.\n"
                                "Click Cancel to exit."), QMessageBox::Cancel);
            tcpServer->close();
            _failed = true;
            return;
        }
    }
    else
    {
        _failed = true;
        return;
    }
}

TimePlannerServer::~TimePlannerServer()
{

}

bool TimePlannerServer::failed()
{
    return _failed;
}

void TimePlannerServer::newConncetion()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, &QTcpSocket::disconnected,
            clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead,
            this, &TimePlannerServer::receiveClientQuery);
    QVector<QVariant> data;
    data.push_back(QVariant("Server response: Connected."));
    _sendToClient(clientSocket, MSG_MESSAGE, data);
}

void TimePlannerServer::receiveClientQuery()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QDataStream inputDataStream(clientSocket);
    inputDataStream.setVersion(QDataStream::Qt_5_5);
    qint64 blocksize = 0;
    qint8 clientQueryType;
    QString login;
    QString password;

    if (clientSocket->bytesAvailable() < sizeof(qint64) + sizeof(qint8))
    {
        return;
    }

    inputDataStream >> blocksize >> clientQueryType;
    switch(clientQueryType)
    {
    case MSG_SIGN_IN:
        qsrand(qrand());
        inputDataStream >> login >> password;
        quint32 clientIP = clientSocket->peerAddress().toIPv4Address();
        QVector<QVariant> user_info = sqlConnector->sign_in(login, password, clientIP);
        if (user_info.isEmpty())
        {
            // User (with such username and password) not found
            user_info.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, user_info);
        }
        else
        {
            _sendToClient(clientSocket, MSG_SIGN_IN, user_info);
        }
        break;
    // TODO: other client query types
    }
}

void TimePlannerServer::_sendToClient(QTcpSocket *socket,
                  quint8 serverAnswerType,
                  QVector<QVariant> data)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
        return;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);
    out << quint64(0) << serverAnswerType;
    switch (serverAnswerType)
    {
    case MSG_ERROR:
        out << data[0].toInt()          // Error code
                ;
        break;
    case MSG_SIGN_IN:
        out << data[0].toString()       // First name
            << data[1].toString()       // Second name
            << data[2].toString()       // Last name
            << data[3].toString()       // Position
            << data[4].toByteArray()    // Session ID
                ;
        break;
    case MSG_MESSAGE:
        out << data[0].toString()       // Message
                ;
        break;
        // TODO: other server answer types
    }
    // Считаем размер отпрвляемых данных
    out.device()->seek(0);
    out << quint64(block.size() - sizeof(quint64));
    // Отправляем данных
    if (socket->isWritable())
        socket->write(block);
    socket->waitForBytesWritten();
}




