#include "timeplanner_server.h"
#include <QApplication>
#include <QtSql/QSqlDatabase>
#include <QDebug>
#include <QPluginLoader>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TimePlannerServer w;
    if (w.failed())
    {
        return 0;
    }
    w.show();
    return a.exec();
}
