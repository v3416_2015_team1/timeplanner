#ifndef SQLCONNECTOR_H
#define SQLCONNECTOR_H

#include <QObject>
#include <QMessageBox>

#include <QtSql>
#include <QSqlDatabase>
#include <QCryptographicHash>




#define SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD "SELECT * FROM `users` WHERE `login` = \"%1\" AND HEX(`hash`) = \"%2\" LIMIT 1"
#define SQL_INSERT_NEW_SESSION "INSERT INTO `sessions` (`session`, `user_id`, `sign_in_ip`) VALUES (X\'%1\', %2, %3)"

//#define SQL_USE_DATABASE "USE testtest"
#define SQL_USE_DATABASE "USE %1"
#define SQL_USE_UNEXISTING_DATABASE "USE _____unexisting_database_____" //
#define SQL_SHOW_TABLES "SHOW TABLES IN %1"
//#define SQL_CREATE_DATABASE "CREATE DATABASE testtest"
#define SQL_CREATE_DATABASE "CREATE DATABASE %1"
#define SQL_CREATE_TABLE_USERS "CREATE TABLE `users` (user_id int AUTO_INCREMENT, login varchar(32), hash binary(32), first_name varchar(32), second_name varchar(32), last_name varchar(32), position varchar(64), rights int, PRIMARY KEY(user_id))"
#define SQL_CREATE_TABLE_SESSIONS "CREATE TABLE `sessions` (session_id int AUTO_INCREMENT, session binary(32), user_id int, sign_in_ip int, last_action timestamp, PRIMARY KEY(session_id))"
#define SQL_CREATE_TABLE_TASKS "CREATE TABLE `tasks` (task_id int AUTO_INCREMENT, creator_id int, assignee int, caption varchar(64), content varchar(1024), status int, created datetime, deadline datetime, done datetime, PRIMARY KEY(task_id))"
#define SQL_CREATE_TABLE_TASK_STATUS "CREATE TABLE `task_status` (task_status_id int AUTO_INCREMENT, status varchar(32), PRIMARY KEY(task_status_id))"
#define SQL_FILL_TABLE_TASK_STATUS "" // TODO: insert task staus
#define SQL_FILL_DEFAULT_USER "INSERT INTO `users` (`login`, `hash`, `first_name`, `second_name`, `last_name`, `position`, `rights`) VALUES ('qwerty', X'2cd9bf92c5e20b1b410f5ace94d963a96e89156fbe65b70365e8596b37f1f165', 'Ivan', 'Ivanovich', 'Ivanov', 'Ofisnyi Plankton', 1)"

#define SQL_ERROR_NO_CONNECTION "2006"
#define SQL_ERROR_UNKNOWN_DATABASE "1049"


class SqlConnector : public QWidget
{
    Q_OBJECT
    QSqlDatabase db;
    QByteArray _generate_session_id(quint32 clientIP);

public:
    SqlConnector(QWidget *parent = 0);
    ~SqlConnector();
    bool connect_db(QString database_name, int mysql_port);
    void disconnect_db();
    bool haveConnection();

    QVector<QVariant> sign_in(QString username,
                              QString password,
                              quint32 clientIP);

signals:

public slots:
};

#endif // SQLCONNECTOR_H
