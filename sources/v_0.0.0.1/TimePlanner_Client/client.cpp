#include "client.h"

Client::Client(QWidget *parent)
    :QWidget(parent)
{
    setWindowIcon(QIcon(":/Icons/icon.ico"));
    setWindowTitle("Time Planner");
    resize(320, 240);
    adjustSize();
    vl = new QVBoxLayout;
    nameLayout = new QHBoxLayout;
    nameLabel = new QLabel("      Name");//сделать, чтобы было не кривот так))
    login = new QLineEdit(this);
    login->setPlaceholderText("Enter your login");

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(login);
    vl->addLayout(nameLayout);

    passwordLayout = new QHBoxLayout;
    passwordLabel = new QLabel("Password");
    password = new QLineEdit(this);
    password->setPlaceholderText("Enter your password");
    passwordLayout->addWidget(passwordLabel);
    passwordLayout->addWidget(password);
    vl->addLayout(passwordLayout);

    buttons = new QHBoxLayout;

    signIn = new QPushButton("Sign In");
    cancel = new QPushButton("Cancel");
    buttons->insertSpacing(0,75);
    buttons->addWidget(signIn);
    buttons->addWidget(cancel);
    buttonItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    buttons->addSpacerItem(buttonItem);


   // connect(signIn, &QPushButton::clicked, this, &AuthWidget::onSignInPress);

    connect(signIn, &QPushButton::clicked, this, &Client::onSignInPress);

    connect(cancel, &QPushButton::clicked, this, &QApplication::quit);

    //connect данные взять из полей
    vl->addLayout(buttons);
    this->setLayout(vl);





}

void Client::startApplication()
{
     this->show();
}

void Client::onSignInPress()
{
       /*-------включить для сервера--------*/
  this->connectToServer();

    /*---------------temporary------------*/
    /*-------убрать для сервера--------*/

//     mainWidget = new Widget();///
//     mainWidget->setUserName("Admin");
//     editTask = new QPushButton;
//     editTask->setIcon(QIcon(":/Icons/edit.ico"));
//     editTask->setToolTip("Edit");

//     connect(editTask, QPushButton::clicked, this, &Client::onEditPress);
//     mainWidget->editLayout->addWidget(editTask);

//     quit = new QPushButton;
//     quit->setIcon(QIcon(":/Icons/exit.ico"));
//     connect(quit, &QPushButton::clicked, this, &QApplication::quit);
//     mainWidget->editLayout->addWidget(quit);

//     mainWidget->show();

}
void Client::onEditPress()
{
    askEditTask();//

}

void Client::askEditTask()
{
    /*todo Идентификатор задания , пользователя*/
    // Проверяем наличие соединения
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    // Отправляем серверу запрос на получение Задач пользователя
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_EDIT_TASK);
   // out << idSession << this->name;

    socket->write(block);

}

void Client::askForUser()
{

    // Проверяем наличие соединения
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    // Отправляем серверу запрос на получение Данных пользователя
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_USER);

    out << this->login->text() << this->password->text();
    out.device()->seek(0);
    socket->write(block);

}

void Client::askForTasks()
{

    // Проверяем наличие соединения
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    // Отправляем серверу запрос на получение Задач пользователя
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK);
    out << idSession << this->name;

    socket->write(block);

}



void Client::connectToServer()
{

    // Создаём новый сокет
        socket = new QTcpSocket();//new QTcpSocket(this) ??
        connect(socket, &QTcpSocket::connected, this, &Client::connected);
        connect(socket, &QTcpSocket::disconnected,socket, &QTcpSocket::deleteLater);

        connect(socket, &QTcpSocket::readyRead, this, &Client::readServerAnswer);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(error(QAbstractSocket::SocketError)));


        // и подключаемся к серверу
           /*-------порт! и ip!!--------*/
        socket->connectToHost("127.0.0.1",3333);
        socket->waitForConnected();
}
void Client::connected()
{
    askForUser();
    askForTasks();

}

void Client::connectCancel()
{
    socket->disconnectFromHost();
    if (socket->state() != QAbstractSocket::UnconnectedState)
        socket->waitForDisconnected();
}

void Client::readServerAnswer()
{

    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_5);
    qint8 answerCode = 0;
    qint64 sizeData;
    QString info;
    QString time, taskName, taskDescription;
    QErrorMessage * err;
    int row = 0;
    int numTasks = 0;
    int errorCode = 0;
    bool unknownAnswerCode = false;
    while (!in.atEnd() && !unknownAnswerCode)
    {
        in >> sizeData >> answerCode;
        switch(answerCode)
        {
        case CLIENT_SEND_ASK_USER:

            in >> name >> stepName >> familyName;
            in >> post;
            in >> idSession; ///

            this->hide();

            info = familyName + " " + name + " " + stepName + " (" +post + ")";
            mainWidget = new Widget();///
            mainWidget->setUserName("Admin");
            editTask = new QPushButton;
            editTask->setIcon(QIcon(":/Icons/edit.ico"));
            editTask->setToolTip("Edit");

            connect(editTask, QPushButton::clicked, this, &Client::onEditPress);
            mainWidget->editLayout->addWidget(editTask);

            quit = new QPushButton;
            quit->setIcon(QIcon(":/Icons/exit.ico"));
            connect(quit, &QPushButton::clicked, this, &QApplication::quit);
            mainWidget->editLayout->addWidget(quit);



            mainWidget->setUserName(info);
            mainWidget->show();///

            break;

        case CLIENT_SEND_ASK_TASK:

            in >> numTasks;
            mainWidget->taskTable->setRowCount(numTasks);

            while (true)
            {
                if (row == numTasks)   break;

                in >> taskName >> taskDescription >> time;

              /*  QDateEdit date(QDate::fromString(time));

                QTextEdit * txt = new QTextEdit(taskDescription);
                QWidget * newWidget = new QWidget(txt);
                QWidget * dateWidget = new QWidget(date);
                mainWidget->taskTable->setCellWidget(row, 0, newWidget);
                mainWidget->taskTable->setCellWidget(row, 1, dateWidget);*/
               // mainWidget->taskTable->setCellWidget();
                row++;
            }
            break;

        case CLIENT_SEND_ASK_EDIT_TASK:

            break;
        case CLIENT_ERROR:
            in >> errorCode;
            if (errorCode == 4001)
            {
                err = new QErrorMessage;
                err->showMessage("Указан неверный логин/пароль");
                //??
            }
            break;
        case 10: // QString message from server
            in >> info;
            qDebug() << info;
            break;
        default:
            unknownAnswerCode = true;
            break;
        }

    }

}

void Client::error(QAbstractSocket::SocketError error)
{
    // Обрабатываем найденные ошибки, связанные с подключением
    QString strError = "Error: ";
    switch(error)
    {
    case QAbstractSocket::ConnectionRefusedError:
        strError.append("The connection was refused.");
        break;
    case QAbstractSocket::RemoteHostClosedError:
        strError.append("The remote host is closed.");
        break;
    case QAbstractSocket::HostNotFoundError:
        strError.append("The host was not found.");
        break;
    default:
        strError.append(QString(socket->errorString()));
    }

    ////???
    /*QMessageBox * mbx = new QMessageBox();
    mbx->setText(strError);
    mbx->setStandardButtons(QMessageBox::Ok);
    mbx->setIcon(QMessageBox::Critical);

    mbx->exec();*/
    QErrorMessage * err = new QErrorMessage;
    err->showMessage(strError);


}

Client::~Client()
{
    ///
}


