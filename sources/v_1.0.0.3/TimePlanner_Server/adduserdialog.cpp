#include "adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Add new user"));
    lblLogin = new QLabel(tr("Login"));
    lblTempPasswordLbl = new QLabel(tr("Temporary password"));
    lblTempPassword = new QLabel();
    _generateTempPassword();
    QFont tempPasswordFont = lblTempPassword->font();
    tempPasswordFont.setBold(true);
    tempPasswordFont.setPointSize(14);
    lblTempPassword->setFont(tempPasswordFont);
//    actCopyTempPassword = new QAction(tr("Copy temporary password"), this);
//    lblTempPassword->addAction(actCopyTempPassword);
//    connect(actCopyTempPassword, &QAction::triggered,
//            this, &AddUserDialog::copyTempPassword);
    lblFirstName = new QLabel(tr("First name"));
    lblSecondName = new QLabel(tr("Second name"));
    lblLastName = new QLabel(tr("Last name"));
    lblPosition = new QLabel(tr("Position"));
    txtLogin = new QLineEdit;
    txtLogin->setPlaceholderText(tr("Login"));
    txtFirstName = new QLineEdit;
    txtFirstName->setPlaceholderText(tr("First name"));
    txtSecondName = new QLineEdit;
    txtSecondName->setPlaceholderText(tr("Second name"));
    txtLastName = new QLineEdit;
    txtLastName->setPlaceholderText(tr("Last name"));
    txtPosition = new QLineEdit;
    txtPosition->setPlaceholderText(tr("Position"));
    chkbxCreateTaskRight = new QCheckBox("This user is allowed to create task");
    btnSave = new QPushButton(tr("Save user"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnSave, &QPushButton::clicked,
            this, &AddUserDialog::onBtnSaveClicked);
    connect(btnCancel, &QPushButton::clicked,
            this, &AddUserDialog::onBtnCancelClicked);
    quint8 layoutRow = 0;
    layout = new QGridLayout(this);
    layout->addWidget(lblLogin, layoutRow, 0);
    layout->addWidget(txtLogin, layoutRow++, 1);
    layout->addWidget(lblTempPasswordLbl, layoutRow, 0);
    layout->addWidget(lblTempPassword, layoutRow++, 1);
    layout->addWidget(lblFirstName, layoutRow, 0);
    layout->addWidget(txtFirstName, layoutRow++, 1);
    layout->addWidget(lblSecondName, layoutRow, 0);
    layout->addWidget(txtSecondName, layoutRow++, 1);
    layout->addWidget(lblLastName, layoutRow, 0);
    layout->addWidget(txtLastName, layoutRow++, 1);
    layout->addWidget(lblPosition, layoutRow, 0);
    layout->addWidget(txtPosition, layoutRow++, 1);
    layout->addWidget(chkbxCreateTaskRight, layoutRow++, 0, 1, 2);
    layout->addWidget(btnSave, layoutRow, 0);
    layout->addWidget(btnCancel, layoutRow++, 1);
}

AddUserDialog::~AddUserDialog()
{

}

void AddUserDialog::onBtnSaveClicked()
{
    if (txtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Login"),
                            tr("No login entered."),
                            QMessageBox::Ok
                            );
        return;
    }
    this->accept();
}

void AddUserDialog::onBtnCancelClicked()
{
    this->reject();
}

void AddUserDialog::_generateTempPassword()
{
    QString newPassword("");
    lblTempPassword->setText(newPassword);
    for (quint8 i = 0; i < 8; ++i)
    {
        quint8 symbolCategory = qrand() % 3;
        switch (symbolCategory)
        {
        case 0:
            newPassword.push_back(QChar('0' + qrand() % 10));
            break;
        case 1:
            newPassword.push_back(QChar('A' + qrand() % 26));
            break;
        case 2:
            newPassword.push_back(QChar('a' + qrand() % 26));
            break;
        }
    }
    lblTempPassword->setText(newPassword);
}

void AddUserDialog::copyTempPassword()
{
    QApplication::clipboard()->setText(lblTempPassword->text());
}
