#ifndef ADDUSERDIALOG_H
#define ADDUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>
#include <QCheckBox>
#include <QAction>
#include <QClipboard>
#include <QApplication>

class AddUserDialog : public QDialog
{
    Q_OBJECT
public:
    AddUserDialog(QWidget *parent = 0);
    ~AddUserDialog();
    inline QString getLogin(){return txtLogin->text();}
    inline QString getPassword(){return lblTempPassword->text();}
    inline QString getFirstName(){return txtFirstName->text();}
    inline QString getSecondame(){return txtSecondName->text();}
    inline QString getLastName(){return txtLastName->text();}
    inline QString getPosition(){return txtPosition->text();}
    inline bool getCreateTaskRight(){return chkbxCreateTaskRight->isChecked();}
signals:
public slots:
private slots:
    void onBtnSaveClicked();
    void onBtnCancelClicked();
    void copyTempPassword();
private:
    QLabel *lblLogin;
    QLabel *lblTempPasswordLbl;
    QLabel *lblFirstName;
    QLabel *lblSecondName;
    QLabel *lblLastName;
    QLabel *lblPosition;
    QLabel *lblCreateTaskRight;
    QLineEdit *txtLogin;
    QLabel *lblTempPassword;
    QLineEdit *txtFirstName;
    QLineEdit *txtSecondName;
    QLineEdit *txtLastName;
    QLineEdit *txtPosition;
    QCheckBox *chkbxCreateTaskRight;
    QPushButton *btnSave;
    QPushButton *btnCancel;
    QGridLayout *layout;
    QAction *actCopyTempPassword;
    void _generateTempPassword();
};

#endif // ADDUSERDIALOG_H
