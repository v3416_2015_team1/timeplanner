#ifndef SQLCONNECTOR_H
#define SQLCONNECTOR_H

#include <QObject>
#include <QMessageBox>
#include <QInputDialog>

#include <QtSql>
#include <QSqlDatabase>
#include <QCryptographicHash>

#include "sql_queries.h"
#include "sql_error_codes.h"

struct Task
{
    qint32 task_id;
    QString caption;
    QString content;
    QDateTime modified;
    QDateTime deadline;
    QString another_user_login;
    QString status;
};
enum Role{CREATOR_ROLE, ASSIGNEE_ROLE};
Q_DECLARE_METATYPE(Task)

class SqlConnector : public QWidget
{
    Q_OBJECT
    QSqlDatabase db;
    QByteArray _generate_session_id(quint32 clientIP);

public:
    SqlConnector(QWidget *parent = 0);
    ~SqlConnector();
    bool connect_db(QString mysql_login,
                    QString mysql_password,
                    QString database_name,
                    int mysql_port,
                    QString &log_string);
    void disconnect_db();
    QVector<QVariant> sign_in(QString login,
                              QString password,
                              quint32 clientIP,
                              QString &log_string);
    QVector<QVariant> sign_in_with_new_password(QString login,
                              QString password,
                              quint32 clientIP,
                              QString &log_string);
    bool check_temp_password(QString login,
                             QString password,
                             quint32 clientIP,
                             QString &log_string);
    void sign_out(QByteArray session,
                  QString &log_string);
    void force_sign_out(QString login,
                        QString &log_string);
    int check_session(QByteArray session,
                      QString &log_string);
    bool create_task(QByteArray session,
                     QString caption,
                     QString content,
                     QString assignee,
                     QDateTime deadline,
                     QString &log_string);
    bool edit_task_status(QByteArray session,
                         quint32 task_id,
                         QString status,
                         QString &log_string);
    bool get_tasks(QByteArray session,
                   QVector<Task> &data,
                   Role role,
                   QString &log_string
                   );
    bool get_users(QByteArray session,
                   QVector<QVariant> &data,
                   QString &log_string);
    QByteArray get_session_by_login(QString login,
                                    QString &log_string);
    void admin_insert_user(QString login,
                           QString password,
                           QString firstName,
                           QString secondName,
                           QString lastName,
                           QString position,
                           bool createTaskRight,
                           QString &log_string);
    void admin_edit_user(QString oldLogin,
                         QString newLogin,
                         QString newPassword,
                         QString firstName,
                         QString secondName,
                         QString lastName,
                         QString position,
                         bool createTaskRight,
                         QString &log_string);
    void admin_delete_user(QString login,
                           QString &log_string);
    QVector<QVariant> admin_get_user_by_login(QString login,
                                             QString &log_string);
signals:

public slots:
};

#endif // SQLCONNECTOR_H
