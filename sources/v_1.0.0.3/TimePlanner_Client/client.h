#ifndef CLIENT_H
#define CLIENT_H
#include "mainwidget.h"
#define CLIENT_ERROR                   0
#define MESSAGE                        10//

#define CLIENT_SEND_ASK_USER           1
#define SIGN_OUT                       11
#define TEMP_PASSWORD                  21
#define FORCE_SIGN_OUT                 31

#define CLIENT_SEND_ASK_CREATE_TASK    2
#define CLIENT_SEND_ASK_TASK_CREATOR   12
#define CLIENT_SEND_ASK_TASK_ASSIGNEE  22//
#define CLIENT_SEND_ASK_EDIT_TASK      32
#define CLIENT_SEND_ASK_ALL_USER       13
#define CLIENT_SEND_ASK_NOTIFICATION   14
#define SERVER_SEND_NEW_TASK_NOTIFICATION 24
#define CLIENT_SEND_CHANGE_STATUS      52

#include <QErrorMessage>
#include <QApplication>
#include <QMainWindow>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableView>
#include <QComboBox>
#include <QTcpSocket>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QDateEdit>
#include <QTimer>
#include "newpassworddialog.h"

class Client  : public QWidget
{
    Q_OBJECT
private:
    void performLogOut();
    void askForUserWithNewPassword(QString newPassword);
    void forceSignOut();
protected:
    void keyPressEvent(QKeyEvent *);

    //QTimer * timer;
    QVBoxLayout * vl;
    QHBoxLayout * buttons;
    QHBoxLayout *nameLayout;
    QHBoxLayout* passwordLayout;
    QLabel * nameLabel;
    QLabel * passwordLabel;

    QSpacerItem * buttonItem;

    QPushButton * signIn;
    QPushButton * cancel;

    QTcpSocket * socket;

    Widget * mainWidget;
    QByteArray idSession;

    QString name, familyName, stepName; //имя, фамилия, отчество
    QString post;



 public:
     Client(QWidget *parent = 0);
     void startApplication();

     void askForUser();
     QLineEdit * login; //плохо, наверное
     QLineEdit * password;



    ~Client();
public slots:
   void askAllUser();
   void askCreateTask();
   void askEditTask(uint);
   void connectToServer();
   void connected();
   void connectCancel();
   void error(QAbstractSocket::SocketError);
   void readServerAnswer();
   void onSignInPress();


   void onChangeStatusPress(uint);
   void askForTasksCreator();
   void askForTasksAssignee();
   void onLogOutSignal();
   void onAppQuit();
   void askNotification();
signals:

   void sendNewNotification(QString, QString);

};




#endif // CLIENT_H


