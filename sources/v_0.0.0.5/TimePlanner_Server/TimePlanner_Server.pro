#-------------------------------------------------
#
# Project created by QtCreator 2015-10-11T17:10:33
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TimePlanner_Server
TEMPLATE = app


SOURCES += main.cpp\
    sqlconnector.cpp \
    timeplanner_server.cpp

HEADERS  += \
    sqlconnector.h \
    timeplanner_server.h \
    sql_queries.h \
    sql_error_codes.h

RC_FILE = application.rc
