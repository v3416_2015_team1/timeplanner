#include "client.h"

Client::Client(QWidget *parent)
    :QWidget(parent)
{
    readSettings(SETTINGS_FILE);
    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle(tr("Time Planner"));
    resize(320, 240);
    adjustSize();
    vl = new QVBoxLayout();
    nameLayout = new QHBoxLayout;
    nameLabel = new QLabel(tr("      Name"));
    login = new QLineEdit(this);
    login->setPlaceholderText(tr("Enter your login"));

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(login);
    vl->addLayout(nameLayout);

    passwordLayout = new QHBoxLayout;
    passwordLabel = new QLabel(tr("Password"));
    password = new QLineEdit(this);
    password->setEchoMode(QLineEdit::Password);
    password->setPlaceholderText(tr("Enter your password"));
    passwordLayout->addWidget(passwordLabel);
    passwordLayout->addWidget(password);
    vl->addLayout(passwordLayout);

    buttons = new QHBoxLayout;

    signIn = new QPushButton(tr("Sign In"));

    signIn->setDefault(true);
    signIn->setFocusPolicy(Qt::StrongFocus);

    cancel = new QPushButton(tr("Cancel"));
    buttons->insertSpacing(0,75);
    buttons->addWidget(signIn);
    buttons->addWidget(cancel);
    buttonItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    buttons->addSpacerItem(buttonItem);


    connect(signIn, &QPushButton::clicked, this, &Client::onSignInPress);
    connect(cancel, &QPushButton::clicked, this, &QApplication::quit);

    vl->addLayout(buttons);
    this->setLayout(vl);
}

void Client::keyPressEvent(QKeyEvent * e)
{
    if (e->key() == Qt::Key_Return)
    {
        signIn->click();
    }
}

void Client::startApplication()
{
    this->show();
}

void Client::onChangeStatusPress(uint task_id)
{
    QString stat;
    for (int i = 0; i <  mainWidget->tasks.size(); i++)
    {
        if  (mainWidget->tasks[i]->num == task_id)
        {
            stat = mainWidget->tasks[i]->statusBox->currentText();
            break;
        }
    }
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_CHANGE_STATUS);
    out << idSession << task_id << stat;
    socket->write(block);
    socket->waitForBytesWritten();
    emit mainWidget->onRefreshPress();
}

void Client::onSignInPress()
{
    this->connectToServer();
}

void Client::performLogOut()
{
    name.clear();
    familyName.clear();
    stepName.clear();
    post.clear();
    password->clear();
    login->clear();
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(SIGN_OUT);
    out << idSession;
    idSession = NULL;
    out.device()->seek(0);
    socket->write(block);
    socket->waitForBytesWritten();
    socket->disconnectFromHost();
    socket->close();
    delete socket;
    socket = NULL;
}

void Client::onLogOutSignal()
{
    performLogOut();
    mainWidget->close();
    delete mainWidget;
    mainWidget = NULL;
    this->show();
}
void Client::forceSignOut()
{
    name.clear();
    familyName.clear();
    stepName.clear();
    post.clear();
    password->clear();
    login->clear();
    mainWidget->close();
    delete mainWidget;
    mainWidget = NULL;
    this->setVisible(true);
}

void Client::onAppQuit()
{
    QMessageBox::StandardButton confirmQuit;
    confirmQuit = QMessageBox::question(this,
                                        tr("Quit TimePlanner"),
                                        tr("Are you sure you want to quit TimePlanner?"),
                                        QMessageBox::Yes|QMessageBox::No);
    if (confirmQuit == QMessageBox::Yes)
    {
        performLogOut();
    }
}

void Client::askAllUser()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_ALL_USER);
    out << idSession;
    socket->write(block);
}

void Client::askCreateTask()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    QString user =  mainWidget->getTaskUser();
    QDate dataDeadLine = mainWidget->getDeadlineDate();
    QString tn = mainWidget->getTaskNameText();
    QString description = mainWidget->getTaskDescriptionText();
    out << quint64(0) << qint8(CLIENT_SEND_ASK_CREATE_TASK);
    out << idSession << user << tn << description << dataDeadLine;
    socket->write(block);
}

void Client::askEditTask(uint task_id, uint row_id)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_EDIT_TASK);
    out << idSession << task_id << mainWidget->tasks[row_id]->task->text();
    out << mainWidget->tasks[row_id]->taskDescription->toPlainText();
    out << mainWidget->tasks[row_id]->dateEdit->dateTime() << mainWidget->tasks[row_id]->assignee->text();
    socket->write(block);

}

void Client::askForUser()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_USER);
    out << this->login->text() << this->password->text();
    out.device()->seek(0);
    socket->write(block);
}

void Client::askForUserWithNewPassword(QString newPassword)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(TEMP_PASSWORD);
    out << this->login->text() << this->password->text() << newPassword;
    out.device()->seek(0);
    socket->write(block);
}

void Client::askForTasksCreator()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_CREATOR);
    out << idSession;
    socket->write(block);
}

void Client::askForTasksAssignee()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_ASSIGNEE);
    out << idSession;
    socket->write(block);
}

void Client::connectToServer()
{
    socket = new QTcpSocket();
    connect(socket, &QTcpSocket::connected,
            this, &Client::connected);
    connect(socket, &QTcpSocket::disconnected,
            socket, &QTcpSocket::deleteLater);
    connect(socket, &QTcpSocket::readyRead,
            this, &Client::readServerAnswer);
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(error(QAbstractSocket::SocketError)));

    if (_settings.contains(SETTING_SERVER_IP) && _settings.contains(SETTING_TCP_PORT))
    {
        socket->connectToHost(_settings[SETTING_SERVER_IP], (quint16)_settings[SETTING_TCP_PORT].toInt());
        socket->waitForConnected();
    }
    else
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("Undefined %1 or %2 at %3").arg(SETTING_SERVER_IP, SETTING_TCP_PORT, SETTINGS_FILE));
    }
}

void Client::askNotification()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_NOTIFICATION);
    out << idSession;
    socket->write(block);
}

void Client::connected()
{
    askForUser();
}

void Client::connectCancel()
{
    socket->disconnectFromHost();
    if (socket->state() != QAbstractSocket::UnconnectedState)
        socket->waitForDisconnected();
}


void Client::readServerAnswer()
{
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_5);
    qint8 answerCode = 0;
    qint64 sizeData;
    QString info;
    QString taskName, taskDescription, taskAssigneeLogin, taskStatus;
    QString caption, content, deadline;
    QDateTime taskModified, taskDeadLine;
    qint32 taskId;
    QErrorMessage * err;

    int numUsers = 0;
    int row = 0, count = 0;
    int numTasks = 0;
    int errorCode = 0;
    bool unknownAnswerCode = false;
    bool createTasksRight = false;
    NewPasswordDialog *newPasswordDialog;
    while (!in.atEnd() && !unknownAnswerCode)
    {
        in >> sizeData >> answerCode;
        switch(answerCode)
        {
        case TEMP_PASSWORD:
            newPasswordDialog = new NewPasswordDialog();
            newPasswordDialog->setModal(true);
            if (newPasswordDialog->exec())
            {
                askForUserWithNewPassword(newPasswordDialog->getNewPassword());
            }
            delete newPasswordDialog;
            break;
        case CLIENT_SEND_ASK_USER:
            in >> name >> stepName >> familyName;
            in >> post;
            in >> createTasksRight;
            in >> idSession;
            askForTasksAssignee();
            this->hide();

            info = familyName + " " + name + " " + stepName + " (" +post + ")";
            mainWidget = new Widget();
            connect(mainWidget, &Widget::appQuit, this, &Client::onAppQuit);

            connect(mainWidget, &Widget::askCreateTaskSignal, this, &Client::askCreateTask);
            connect(mainWidget, &Widget::askAssigneeTasks, this, Client::askForTasksAssignee);
            connect(mainWidget, &Widget::askCreatedTasks, this, Client::askForTasksCreator);
            connect(mainWidget, &Widget::logOutSignal, this, Client::onLogOutSignal);
            connect(mainWidget, &Widget::allUsersSignal, this, Client::askAllUser);

            connect(this, SIGNAL(sendNewNotification(QString,QString)),mainWidget,SLOT(getNewNotification(QString,QString)));
            mainWidget->setUserName(info);
            mainWidget->setTaskListTypeSwitchVisible(createTasksRight);
            mainWidget->setCreateTaskVisible(createTasksRight);
            mainWidget->show();
            break;

        case CLIENT_SEND_ASK_TASK_CREATOR:
            in >> numTasks;
            mainWidget->tasks.resize(numTasks);
            while (true)
            {
                if (row == numTasks)
                    break;
                in >> taskId >> taskName >> taskDescription >> taskModified >> taskDeadLine >> taskAssigneeLogin >> taskStatus;
                mainWidget->addTaskLine(row,taskId, taskName, taskDescription, taskModified, taskDeadLine, taskAssigneeLogin, taskStatus);
                connect(mainWidget->tasks[row], SIGNAL(changeStatusSignal(uint)),
                        this, SLOT(onChangeStatusPress(uint)));
                connect(mainWidget->tasks[row], SIGNAL(taskChanged(uint, uint)),
                        this, SLOT(askEditTask(uint, uint)));
                row++;
            }
            break;
        case CLIENT_SEND_ASK_TASK_ASSIGNEE:
            in >> numTasks;
            mainWidget->tasks.resize(numTasks);
            while (true)
            {
                if (row == numTasks)   break;

                in >> taskId >> taskName >> taskDescription >>
                        taskModified >> taskDeadLine >> taskAssigneeLogin >> taskStatus;
                mainWidget->addTaskLine(row, taskId, taskName,
                                        taskDescription, taskModified, taskDeadLine, taskAssigneeLogin, taskStatus);

                connect(mainWidget->tasks[row], SIGNAL(changeStatusSignal(uint)), this, SLOT(onChangeStatusPress(uint)));
                row++;
            }
            break;
        case CLIENT_SEND_ASK_ALL_USER:
            in >> numUsers;
            while (true)
            {
                if (count  == numUsers) break;
                in >> info;
                mainWidget->addUserToList(info);
                count++;
            }
            break;
        case CLIENT_SEND_CHANGE_STATUS:
            //
            break;
        case SERVER_SEND_NEW_TASK_NOTIFICATION:
            in >> caption >> content >> deadline;
            emit sendNewNotification(tr("New task"), QString("%1 \n %2 \n Deadline: %3").arg(caption).arg(content).arg(deadline));
            emit mainWidget->emitRefreshClicked();
            break;
        case SIGN_OUT:
            this->onLogOutSignal();
            break;
        case FORCE_SIGN_OUT:
            this->forceSignOut();
            break;
        case CLIENT_ERROR:
            in >> errorCode;
            if (errorCode == 4001)
            {
                err = new QErrorMessage;
                err->showMessage(tr("Incorrect login or password!"));
            }
            break;
        case MESSAGE:
            in >> info;
            qDebug() << info;
            break;
        default:
            unknownAnswerCode = true;
            in.skipRawData(socket->bytesAvailable());
            break;
        }
    }
}

void Client::error(QAbstractSocket::SocketError error)
{
    QString strError = "Error: ";
    switch(error)
    {
    case QAbstractSocket::ConnectionRefusedError:
        strError.append(tr("The connection was refused."));
        break;
    case QAbstractSocket::RemoteHostClosedError:
        strError.append(tr("The remote host is closed."));
        break;
    case QAbstractSocket::HostNotFoundError:
        strError.append(tr("The host was not found."));
        break;
    default:
        strError.append(QString(socket->errorString()));
    }
    QErrorMessage * err = new QErrorMessage;
    err->showMessage(strError);
}

void Client::readSettings(QString settingsFileName)
{
    QFile settings_file(settingsFileName);
    if (settings_file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream settings_stream(&settings_file);
        while (!settings_stream.atEnd())
        {
            QStringList setting = settings_stream.readLine().split(QRegExp("(\\=)"));
            if (setting.size() == 2)
                _settings.insert(setting.at(0), setting.at(1));
        }
        settings_file.close();
    }
    else
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("Settings file %1 not found!").arg(settingsFileName));
    }
}

Client::~Client()
{
}
