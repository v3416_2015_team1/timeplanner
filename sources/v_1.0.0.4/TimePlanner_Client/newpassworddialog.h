#ifndef NEWPASSWORDDIALOG_H
#define NEWPASSWORDDIALOG_H

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

class NewPasswordDialog : public QDialog
{
    Q_OBJECT
private:
    QLabel *lblNewPassword;
    QLabel *lblConfirmNewPassword;
    QLineEdit *txtNewPassword;
    QLineEdit *txtConfirmNewPassword;
    QPushButton *btnOk;
    QPushButton *btnCancel;
    QGridLayout *layout;
public:
    NewPasswordDialog(QWidget *parent = 0);
    ~NewPasswordDialog();
    inline QString getNewPassword(){return txtConfirmNewPassword->text();}
signals:

public slots:
    void onBtnOkClicked();
    void onBtnCancelClicked();
};

#endif // NEWPASSWORDDIALOG_H
