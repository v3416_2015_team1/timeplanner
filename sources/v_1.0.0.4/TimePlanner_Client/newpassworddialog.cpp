#include "newpassworddialog.h"

NewPasswordDialog::NewPasswordDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Create new password"));
    lblNewPassword = new QLabel(tr("New password"));
    lblConfirmNewPassword = new QLabel(tr("Confirm new password"));
    txtNewPassword = new QLineEdit();
    txtNewPassword->setEchoMode(QLineEdit::Password);
    txtNewPassword->setPlaceholderText(tr("New password"));
    txtConfirmNewPassword = new QLineEdit();
    txtConfirmNewPassword->setEchoMode(QLineEdit::Password);
    txtConfirmNewPassword->setPlaceholderText(tr("Confirm new password"));
    btnOk = new QPushButton(tr("Ok"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnOk, &QPushButton::clicked,
            this, &NewPasswordDialog::onBtnOkClicked);
    connect(btnCancel, &QPushButton::clicked,
            this, &NewPasswordDialog::onBtnCancelClicked);
    layout = new QGridLayout(this);
    int rowNum = 0;
    layout->addWidget(lblNewPassword, rowNum, 0);
    layout->addWidget(txtNewPassword, rowNum++, 1);
    layout->addWidget(lblConfirmNewPassword, rowNum, 0);
    layout->addWidget(txtConfirmNewPassword, rowNum++, 1);
    layout->addWidget(btnOk, rowNum, 0);
    layout->addWidget(btnCancel, rowNum++, 1);
}

NewPasswordDialog::~NewPasswordDialog()
{
}

void NewPasswordDialog::onBtnOkClicked()
{
    if (txtNewPassword->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("No password entered."),
                            QMessageBox::Ok
                            );
        txtNewPassword->clear();
        txtConfirmNewPassword->clear();
        return;
    }
    if (txtNewPassword->text() != txtConfirmNewPassword->text())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("Password mismatch."),
                            QMessageBox::Ok
                            );
        txtNewPassword->clear();
        txtConfirmNewPassword->clear();
        return;
    }
    this->accept();
}

void NewPasswordDialog::onBtnCancelClicked()
{
    this->reject();
}
