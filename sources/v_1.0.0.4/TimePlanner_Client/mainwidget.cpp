#include "mainwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    taskType = ASSIGNEE;

    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle(tr("Time Planner"));
    actionShowHide = new QAction(tr("Show/Hide Time Planner"), this);
    connect(actionShowHide, &QAction::triggered,
            this, &Widget::showHide);
    actionQuit = new QAction(tr("Quit"), this);
    connect(actionQuit,&QAction::triggered,
            this, &Widget::onAppQuit);

    menu = new QMenu(this);
    menu->addAction(actionShowHide);
    menu->addAction(actionQuit);
    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(menu);
    systemTrayIcon->setToolTip(tr("Time Planner"));
    systemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &Widget::onTrayIconActivated);

    resize(640, 320);
    adjustSize();

    vl = new QVBoxLayout(this);
    edit = new QGroupBox(this);
    editLayout = new QHBoxLayout;
    userName = new QLabel;
    QFont f( "Calibri", 10, QFont::Bold);
    userName->setFont(f);
    editLayout->addWidget(userName);
    editItem = new QSpacerItem(1, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    editLayout->addSpacerItem(editItem);

    create = new QPushButton;
    create->setIcon(QIcon(":/Icons/add.ico"));
    create->setToolTip(tr("Create New Task"));
    connect(create, &QPushButton::clicked,
            this, &Widget::onCreatePress);
    editLayout->addWidget(create);

    refresh = new QPushButton;
    refresh->setIcon(QIcon(":/Icons/refresh.ico"));
    refresh->setToolTip(tr("Refresh"));
    connect(refresh, &QPushButton::clicked,
            this, &Widget::onRefreshPress);
    editLayout->addWidget(refresh);

    quit = new QPushButton;
    quit->setIcon(QIcon(":/Icons/exit.ico"));
    quit->setToolTip(tr("Sign out"));
    connect(quit, &QPushButton::clicked,
            this, &Widget::logOut);
    editLayout->addWidget(quit);

    edit->setLayout(editLayout);
    vl->addWidget(edit);
    QGroupBox * task = new QGroupBox(this);

    taskNamesL = new QHBoxLayout;
    curTask = new QLabel(tr("Assigned tasks"));
    curTask->setFont(f);
    taskNamesL->addWidget(curTask);
    task->setLayout(taskNamesL);
    vl->addWidget(task);

    scrollArea = new QScrollArea(this);
    vl->addWidget(scrollArea);
    taskWidget = new QWidget(this);
    taskVBox = new QVBoxLayout(taskWidget);
    taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
    taskVBox->setAlignment(Qt::AlignTop);
    taskWidget->setLayout(taskVBox);

    scrollArea->setWidget(taskWidget);
    scrollArea->setWidgetResizable(true);

    scrollArea->setGeometry(0,320,640,200);
    scrollArea->adjustSize();
    scrollArea->show();

    taskGB = new QGroupBox(this);
    taskBL = new QHBoxLayout;
    myTaskB = new QRadioButton(tr("Created tasks"));
    assigneeTaskB = new QRadioButton(tr("Assigneed tasks"));
    assigneeTaskB->setChecked(true);

    connect(myTaskB, &QRadioButton::toggled,
            this, &Widget::onMyTaskBPressed);
    connect(assigneeTaskB, &QRadioButton::toggled,
            this, &Widget::onAssigneeTaskBPressed);

    taskBL->addWidget(myTaskB);
    taskBL->addWidget(assigneeTaskB);

    taskGB->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Minimum);
    taskGB->setLayout(taskBL);

    vl->addWidget(taskGB);
    this->setLayout(vl);
}

void Widget::setTaskListTypeSwitchVisible(bool visible)
{
    this->taskGB->setVisible(visible);
}

void Widget::setCreateTaskVisible(bool visible)
{
    create->setVisible(visible);
}

void Widget::addUserToList(QString & info)
{
    if (employees != NULL)
        employees->addItem(info);
}

void Widget::onCreatePress()
{
    createWidget = new QWidget;
    createWidget->setWindowTitle(tr("Create new task"));

    gl = new QGridLayout(createWidget);

    nameEmployee = new QLabel(tr("Employee"));
    employees = new QComboBox;
    emit allUsersSignal();

    gl->addWidget(nameEmployee,0,0);
    gl->addWidget(employees,0,1);

    nameLabel = new QLabel(tr("Task"));
    nametask = new QLineEdit;
    nametask->setPlaceholderText(tr("Enter caption of your task"));
    gl->addWidget(nameLabel,1,0);
    gl->addWidget(nametask,1,1);

    nameDescr = new QLabel(tr("Description"));
    descr = new QTextEdit;
    descr->setPlaceholderText("Enter content of your task");
    gl->addWidget(nameDescr,2,0);
    gl->addWidget(descr,2,1);

    nameDate = new QLabel(tr("Deadline"));
    date = new QDateEdit(QDate::currentDate());
    date->setMinimumDateTime(QDateTime::currentDateTime());
    gl->addWidget(nameDate,3,0);
    gl->addWidget(date,3,1);

    createTask = new QPushButton(tr("Create task"));
    gl->addWidget(createTask,4,1);
    createWidget->setLayout(gl);
    createWidget->show();
    connect(createTask, &QPushButton::clicked,
            this, &Widget::askCreateTask);
    connect(createTask, &QPushButton::clicked,
            createWidget, &QWidget::close);
}

QDate Widget::getDeadlineDate()
{
    return date->date();
}
QString Widget::getTaskUser()
{
    return employees->currentText();
}
QString Widget::getTaskNameText()
{
    return nametask->text();
}

QString Widget::getTaskDescriptionText()
{
    return descr->toPlainText();
}

void Widget::askCreateTask()
{
    emit this->askCreateTaskSignal();}

void Widget::logOut()
{
    taskType = ASSIGNEE;
    for (int i = 0; i < tasks.size(); i++)
    {
        delete tasks[i];
    }
    tasks.clear();
    scrollArea->update();
    emit logOutSignal();
}

void Widget::setUserName(QString username)
{
    userName->setText(username);
}


void clearLayout(QLayout *layout)
{
    QLayoutItem *child;
    while ((child = layout->takeAt(0)) != 0)
    {
        if(child->layout() != 0)
            clearLayout( child->layout() );
        else if(child->widget() != 0)
            delete child->widget();
        delete child;
    }

}

void Widget::cleanLayout(QVBoxLayout * layout)
{
    QLayoutItem *child;
    while ((child = layout->takeAt(0)) != 0)
    {
        if(child->layout() != 0)
            clearLayout( child->layout() );
        else if(child->widget() != 0)
            delete child->widget();
        delete child;
    }
    for (int i = 0; i < tasks.size(); i++)
    {
        delete tasks[i];
    }
    tasks.clear();
}

void Widget::onRefreshPress()
{
    if (taskType == CREATOR)
    {
        cleanLayout(taskVBox);
        delete taskWidget->layout();
        taskVBox = NULL;
        taskVBox = new QVBoxLayout(taskWidget);
        taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
        taskVBox->setAlignment(Qt::AlignTop);
        taskWidget->setLayout(taskVBox);
        emit askCreatedTasks();
    }
    else if (taskType == ASSIGNEE)
    {
        cleanLayout(taskVBox);
        delete taskWidget->layout();
        taskVBox = NULL;
        taskVBox = new QVBoxLayout(taskWidget);
        taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
        taskVBox->setAlignment(Qt::AlignTop);
        taskWidget->setLayout(taskVBox);
        emit askAssigneeTasks();
    }
}
void Widget::onMyTaskBPressed()
{
    if (myTaskB->isChecked())
    {
        if (taskType != CREATOR)
        {
            taskType = CREATOR;
            curTask->setText(tr("Created tasks"));
            cleanLayout(taskVBox);
            delete taskWidget->layout();
            taskVBox = NULL;
            taskVBox = new QVBoxLayout(taskWidget);
            taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
            taskVBox->setAlignment(Qt::AlignTop);
            taskWidget->setLayout(taskVBox);
            emit askCreatedTasks();
        }

    }
}

void Widget::onAssigneeTaskBPressed()
{
    if (assigneeTaskB->isChecked())
    {
        if (taskType != ASSIGNEE)
        {
            taskType = ASSIGNEE;
            curTask->setText(tr("Assigned tasks"));
            cleanLayout(taskVBox);
            delete taskWidget->layout();
            taskVBox = NULL;
            taskVBox = new QVBoxLayout(taskWidget);
            taskVBox->setSizeConstraint(QLayout::SetMinimumSize);
            taskVBox->setAlignment(Qt::AlignTop);
            taskWidget->setLayout(taskVBox);

            emit askAssigneeTasks();
        }
    }
}

void Widget::addTaskLine(int row,
                         int taskId,
                         QString caption,
                         QString content,
                         QDateTime modified,
                         QDateTime deadline,
                         QString assigneeLogin,
                         QString status)
{
    tasks[row] = new TaskLine;
    tasks[row]->num = taskId;
    tasks[row]->row = row;
    tasks[row]->taskLayout = new QGridLayout;

    tasks[row]->task = new QLineEdit;
    tasks[row]->task->setReadOnly(true);
    tasks[row]->task->setText(caption);
    tasks[row]->taskLayout->addWidget(tasks[row]->task, 0,0);
    int y_id = 0;
    if (taskType == CREATOR)
    {
        tasks[row]->assignee = new QLineEdit;
        tasks[row]->assignee->setReadOnly(true);
        tasks[row]->assignee->setText(assigneeLogin);
        tasks[row]->taskLayout->addWidget(tasks[row]->assignee, 0,1);

        y_id = 2;
        tasks[row]->saveTask = new QPushButton(tr("Save"));
        tasks[row]->saveTask->setToolTip(tr("Save task"));
        tasks[row]->taskLayout->addWidget(tasks[row]->saveTask, 0,y_id);
        connect(tasks[row]->saveTask, &QPushButton::pressed, tasks[row], &TaskLine::onTaskChanged);
    }
    else
    {
        tasks[row]->assignee = NULL;
        tasks[row]->saveTask = NULL;
    }

    tasks[row]->watchTask = new QPushButton;
    tasks[row]->watchTask->setIcon(QIcon(":/Icons/read.ico"));
    tasks[row]->watchTask->setToolTip(tr("Read Task"));
    tasks[row]->taskLayout->addWidget(tasks[row]->watchTask, 0,y_id + 1);

    connect(tasks[row]->watchTask, &QPushButton::pressed,
            tasks[row], &TaskLine::showHideDescription);

    tasks[row]->dateEdit = new QDateEdit(deadline.date());
    tasks[row]->dateEdit->setMinimumDateTime(QDateTime::currentDateTime());
    if (deadline.date() <= QDate::currentDate())
    {
        tasks[row]->task->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
        tasks[row]->dateEdit->setStyleSheet("* { background-color: rgb(255, 50, 50); }");
    }
    tasks[row]->dateEdit->setReadOnly(true);
    tasks[row]->taskLayout->addWidget(tasks[row]->dateEdit, 0, y_id + 3);

    tasks[row]->editTask = new QPushButton();
    tasks[row]->editTask->setIcon(QIcon(":/Icons/edit.ico"));
    tasks[row]->editTask->setToolTip(tr("Edit Task"));
    tasks[row]->editTask->setDisabled(true);
    tasks[row]->taskLayout->addWidget(tasks[row]->editTask, 0, y_id + 4);

    tasks[row]->taskDescription = new QTextEdit(content);
    tasks[row]->taskDescription->setReadOnly(true);
    tasks[row]->taskLayout->addWidget(tasks[row]->taskDescription, 1, 0);
    tasks[row]->taskDescription->setVisible(false);

    if (taskType == CREATOR)
    {
        tasks[row]->editTask->setDisabled(false);
        connect(tasks[row]->editTask, &QPushButton::clicked, tasks[row], &TaskLine::askEditTask);
    }

    tasks[row]->changeStatus = new QPushButton(tr("Change status"));
    tasks[row]->changeStatus->setToolTip(tr("Change task status"));
    tasks[row]->changeStatus ->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    tasks[row]->statusBox = new QComboBox;

    tasks[row]->statusBox->addItem(status);

    tasks[row]->statusBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    connect(tasks[row]->changeStatus, &QPushButton::clicked,
            tasks[row], &TaskLine::onChangeStatusPress);

    tasks[row]->statusLayout = new QHBoxLayout;
    tasks[row]->statusLayout->addWidget(tasks[row]->statusBox);
    tasks[row]->statusLayout->addWidget(tasks[row]->changeStatus);
    tasks[row]->taskLayout->addLayout(tasks[row]->statusLayout, 3, 0, 1, 2, Qt::AlignLeft);
    tasks[row]->changeStatus->setVisible(false);
    tasks[row]->statusBox->setVisible(false);

    if (status == "Done" )
    {
        if (taskType == ASSIGNEE)
        {
            tasks[row]->task->setVisible(false);
            tasks[row]->dateEdit->setVisible(false);
            tasks[row]->watchTask->setVisible(false);
            tasks[row]->editTask->setVisible(false);
        }
        tasks[row]->statusBox->addItem(tr("In progress"));
    }
    else
        tasks[row]->statusBox->addItem(tr("Done"));

    taskVBox->addLayout(tasks[row]->taskLayout);
}

void Widget::emitSignalOut()
{
    emit quit->clicked();
}

void Widget::emitRefreshClicked()
{
    emit refresh->clicked();
}

void Widget::getNewNotification(QString caption, QString content)
{
    systemTrayIcon->showMessage(caption, content);
}

void Widget::closeEvent(QCloseEvent *event)
{
    if (systemTrayIcon->isVisible())
    {
        hide();
    }
    event->ignore();
}

void Widget::keyPressEvent(QKeyEvent * e)
{
    QWidget::keyPressEvent(e);
    if ( e->key() == Qt::Key_Escape)//Esc
    {
        if (systemTrayIcon->isVisible())
        {
            hide();
        }
        e->ignore();
    }
}

TaskLine::TaskLine(QWidget *parent)
    : QWidget(parent)
{
}

TaskLine::~TaskLine()
{
}

void TaskLine::showHideDescription()
{
    if(!taskDescription->isVisible())
    {
        taskDescription->setVisible(true);
        changeStatus->setVisible(true);
        statusBox->setVisible(true);
        watchTask->setIcon(QIcon(":/Icons/up.ico"));
    }
    else
    {
        taskDescription->setVisible(false);
        changeStatus->setVisible(false);
        statusBox->setVisible(false);
        watchTask->setIcon(QIcon(":/Icons/read.ico"));
        if (!taskDescription->isReadOnly())
        {
            taskDescription->setReadOnly(true);
        }
        if (!task->isReadOnly())
        {
            task->setReadOnly(true);
        }
        if (!dateEdit->isReadOnly())
        {
            dateEdit->setReadOnly(true);
        }
    }
}

void TaskLine::onChangeStatusPress()
{
    emit changeStatusSignal(this->num);
}

void TaskLine::onTaskChanged()
{
    emit taskChanged(this->num, this->row);
}

void TaskLine::askEditTask()
{
    showHideDescription();
    if (taskDescription->isReadOnly())
    {
        taskDescription->setReadOnly(false);
    }
    if (dateEdit->isReadOnly())
    {
        dateEdit->setReadOnly(false);
    }
    if (task->isReadOnly())
    {
        task->setReadOnly(false);
    }
}

void Widget::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}

void Widget::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
    {
        showHide();
    }
}

void Widget::onAppQuit()
{
    taskType = ASSIGNEE;
    cleanLayout(taskVBox);
    delete taskWidget->layout();
    taskVBox = NULL;
    emit appQuit();
    qApp->quit();
}

Widget::~Widget()
{
}
