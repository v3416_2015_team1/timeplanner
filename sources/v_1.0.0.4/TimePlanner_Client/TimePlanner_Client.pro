#-------------------------------------------------
#
# Project created by QtCreator 2015-10-11T18:21:05
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TimePlanner_Client
TEMPLATE = app


SOURCES += main.cpp \
    mainwidget.cpp \
    client.cpp \
    newpassworddialog.cpp

HEADERS  += \
    client.h \
    mainwidget.h \
    newpassworddialog.h

RESOURCES += \
    resources.qrc

RC_FILE = application.rc
