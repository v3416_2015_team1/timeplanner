#ifndef SQL_QUERIES
#define SQL_QUERIES
#define SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD "SELECT * FROM `users` \
                                                WHERE `login` = \"%1\" AND \
                                                      HEX(`hash`) = \"%2\" \
                                                LIMIT 1"
#define SQL_INSERT_NEW_SESSION "INSERT INTO `sessions` \
                                (`session`, `user_id`, `sign_in_ip`) \
                                VALUES (X\'%1\', %2, %3)"
#define SQL_DELETE_SESSIONS "DELETE FROM `sessions` WHERE `sessions`.`user_id` = %1"
#define SQL_DELETE_SESSION "DELETE FROM `sessions` \
                            WHERE HEX(`session`) = \"%1\""
#define SQL_FORCE_DELETE_SESSION "DELETE FROM `sessions` WHERE user_id = %1"
#define SQL_CHECK_SESSION "SELECT `user_id` FROM `sessions` \
                            WHERE HEX(`session`) = \"%1\" LIMIT 1"
#define SQL_GET_UID_BY_LOGIN "SELECT `user_id` FROM `users` \
                                WHERE `users`.`login` = '%1'"
#define SQL_GET_SESSION_BY_UID "SELECT `session` FROM `sessions` WHERE `sessions`.`user_id` = %1 LIMIT 1"
#define SQL_GET_USERS "SELECT `login` FROM `users`"
#define SQL_GET_USER_BY_LOGIN "SELECT `login`, `first_name`, `second_name`, `last_name`, `position`, `right_create_tasks` \
                                FROM `users` \
                                WHERE `users`.`login` = \'%1\'"
#define SQL_INSERT_USER "INSERT INTO `users` \
                            (`login`, `hash`, `first_name`, `second_name`, `last_name`, `position`, `right_create_tasks`) \
                            VALUES (\'%1\', X\'%2\', \'%3\', \'%4\', \'%5\', \'%6\', %7)"
#define SQL_INSERT_TEMP_PASSWORD "INSERT INTO `temp_passwords` \
                                    (`temp_password`, `user_id`) \
                                    VALUES (X\'%1\', (SELECT `user_id` FROM `users` WHERE `login` = \'%2\'))"
#define SQL_DELETE_TEMP_PASSWORD "DELETE FROM `temp_passwords` WHERE `temp_passwords`.`user_id` = (SELECT `users`.`user_id` FROM `users` WHERE `users`.`login` = \'%1\')"
#define SQL_CHECK_TEMP_PASSWORD "SELECT `temp_passwords`.`user_id` FROM `temp_passwords` \
                                WHERE `temp_passwords`.`user_id` = (SELECT `users`.`user_id` FROM `users` WHERE `users`.`login` = \'%1\') AND \
                                      HEX(`temp_passwords`.`temp_password`) = \'%2\'"
#define SQL_EDIT_USER "UPDATE `users` SET `login` = \'%1\', `first_name` = \'%2\', `second_name` = \'%3\', `last_name` = \'%4\', `position` = \'%5\', `right_create_tasks` = %6 WHERE `login` = \'%7\'"
#define SQL_EDIT_USER_PASSWORD "UPDATE `users` SET `hash` = X\'%1\' WHERE `login` = \'%2\'"
#define SQL_DELETE_USER "DELETE FROM `users` WHERE `login` = \'%1\'"
#define SQL_INSERT_NEW_TASK "INSERT INTO `tasks` \
                            (`creator_id`, `assignee`, `caption`, `content`, `status`, `deadline`) \
                            VALUES (%1, %2, \'%3\', \'%4\', %5, \'%6\')"
#define SQL_EDIT_TASK "UPDATE `tasks` AS `T`\
                        SET `T`.`assignee` = (SELECT `A`.`user_id` FROM `users` AS `A` WHERE `A`.`login` = \'%1\'), \
                            `T`.`caption` = \'%2\', \
                            `T`.`content` = \'%3\', \
                            `T`.`deadline` = \'%4\' \
                        WHERE `T`.`task_id` = %5"
#define SQL_EDIT_TASK_STATUS "UPDATE `tasks` AS `T`, (SELECT `task_status_id` FROM `task_status` WHERE `status` = \'%2\') AS `TS`\
                                SET `T`.`status` = `TS`.`task_status_id`\
                                WHERE `T`.`task_id` = %1"
#define SQL_TASKS_CREATOR "SELECT `task_id`,\
                                    `caption`,\
                                    `content`,\
                                    `modified`,\
                                    `deadline`,\
                                    U_ASSIGNEE.`login`,\
                                    TS.`status`\
                            FROM `tasks`\
                            INNER JOIN `users` AS U_CREATOR\
                            ON `tasks`.`creator_id` = U_CREATOR.`user_id`\
                            INNER JOIN `users` AS U_ASSIGNEE\
                            ON `tasks`.`assignee` = U_ASSIGNEE.`user_id`\
                            INNER JOIN `task_status` AS TS\
                            ON `tasks`.`status` = TS.`task_status_id`\
                            WHERE `tasks`.`creator_id` = %1"
#define SQL_TASKS_ASSIGNEE "SELECT `task_id`,\
                                    `caption`,\
                                    `content`,\
                                    `modified`,\
                                    `deadline`,\
                                    U_CREATOR.`login`,\
                                    TS.`status`\
                            FROM `tasks`\
                            INNER JOIN `users` AS U_CREATOR\
                            ON `tasks`.`creator_id` = U_CREATOR.`user_id`\
                            INNER JOIN `users` AS U_ASSIGNEE\
                            ON `tasks`.`assignee` = U_ASSIGNEE.`user_id`\
                            INNER JOIN `task_status` AS TS\
                            ON `tasks`.`status` = TS.`task_status_id`\
                            WHERE `tasks`.`assignee` = %1"

/// Check database connection and existance of the table.
#define SQL_USE_DATABASE "USE `%1`"
#define SQL_USE_UNEXISTING_DATABASE "USE `_____unexisting_database_____`" //
#define SQL_SHOW_TABLES "SHOW TABLES IN `%1`"

/// Creation database
#define SQL_CREATE_DATABASE "CREATE DATABASE `%1`"
#define SQL_CREATE_TABLE_USERS "CREATE TABLE `users` \
                                (\
                                  `user_id` int AUTO_INCREMENT, \
                                  `login` varchar(32) NOT NULL, \
                                  `hash` binary(32) NOT NULL, \
                                  `first_name` varchar(32), \
                                  `second_name` varchar(32), \
                                  `last_name` varchar(32), \
                                  `position` varchar(64), \
                                  `right_create_tasks` BOOLEAN, \
                                  PRIMARY KEY(user_id),\
                                  UNIQUE (login)\
                                )"
#define SQL_CREATE_TABLE_SESSIONS "CREATE TABLE `sessions` \
                                    (\
                                      `session_id` int AUTO_INCREMENT, \
                                      `session` binary(32), \
                                      `user_id` int, \
                                      `sign_in_ip` int, \
                                      `last_action` timestamp DEFAULT CURRENT_TIMESTAMP, \
                                      PRIMARY KEY(session_id)\
                                    )"
#define SQL_CREATE_TABLE_TASKS "CREATE TABLE `tasks` \
                                (\
                                  `task_id` int AUTO_INCREMENT, \
                                  `creator_id` int, \
                                  `assignee` int, \
                                  `caption` varchar(64), \
                                  `content` varchar(1024), \
                                  `status` int, \
                                  `modified` timestamp DEFAULT CURRENT_TIMESTAMP, \
                                  `deadline` timestamp, \
                                  PRIMARY KEY(task_id)\
                                )"
#define SQL_CREATE_TABLE_TASK_STATUS "CREATE TABLE `task_status` \
                                      (\
                                        `task_status_id` int AUTO_INCREMENT, \
                                        `status` varchar(32), \
                                        PRIMARY KEY(task_status_id) \
                                      )"
#define SQL_CREATE_TABLE_TEMP_PASSWORDS "CREATE TABLE `temp_passwords` \
                                        (\
                                            `temp_password_id` int AUTO_INCREMENT, \
                                            `temp_password` binary(32) NOT NULL, \
                                            `user_id` int, \
                                            PRIMARY KEY(temp_password_id)\
                                        )"
#define SQL_FILL_TABLE_TASK_STATUS "INSERT INTO `task_status` \
                                    (`status`)\
                                    VALUES('%1')"

#endif // SQL_QUERIES

