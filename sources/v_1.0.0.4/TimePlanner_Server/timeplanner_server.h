#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QApplication>
#include <QCloseEvent>
#include <QMainWindow>
#include <QInputDialog>
#include "sqlconnector.h"
#include "adduserdialog.h"
#include "edituserdialog.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QDateTime>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QSystemTrayIcon>
#include <QDesktopWidget>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QLabel>
#include <QFileDialog>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include <QFile>
#include <QTextStream>
#include <QMap>

#define INITIAL_PORT            3333
//#define SERVER_SEND_PART_NOTIFY 1

#define MSG_ERROR       0
#define MSG_MESSAGE     10

#define MSG_SIGN_IN     1
#define MSG_SIGN_OUT    11
#define MSG_TEMP_PASSWORD 21
#define MSG_FORCE_SIGN_OUT 31

#define MSG_TASK_CREATE             2
#define MSG_TASK_USER_CREATOR       12
#define MSG_TASK_USER_ASSIGNEE      22
#define MSG_TASK_EDIT               32 //
#define MSG_TASK_DELETE             42 //
#define MSG_TASK_EDIT_STATUS        52

#define MSG_REGISTRATION    3//
#define MSG_USERS           13//

#define MSG_NOTIFICATIONS 14
#define MSG_NOTIFY_NEW_TASK 24

#define ERROR_SIGN_IN_FAILURE 4001
#define ERROR_SESSION_FAILURE 4002

#define STATUS_LBL_MYSQL_PORT "MySQL port: "
#define STATUS_LBL_MYSQL_USER "MySQL user: "
#define STATUS_LBL_MYSQL_DATABASE "MySQL database: "
#define STATUS_LBL_TCP_PORT "TCP port: "

enum SignInPasswordType {NORMAL, NEW};

class TimePlannerServer : public QMainWindow
{
    Q_OBJECT
    QTextEdit *txtLog;
    SqlConnector *sqlConnector;
    QTcpServer *tcpServer;
    QWidget *centralWidget;
    QVBoxLayout *vbltCentralWidgetLayout;
    QSystemTrayIcon *systemTrayIcon;

    QMenu *systemTrayIconContextMenu;
    QAction *actShowHide;
    QAction *actQuit;

    QMenu *menuLog;
    QMenu *menuMySQL;
    QMenu *menuTCP;
    QMenu *menuUsers;
    QMenu *menuAbout;

    QAction *actSaveLog;
    QAction *actPrintLog;
    QAction *actClearLog;
    QAction *actConnectMySQL;
    QAction *actDisconnectMySQL;
    QAction *actRunTCP;
    QAction *actStopTCP;
    QAction *actAddUser;
    QAction *actEditUser;
    QAction *actDeleteUser;
    QAction *actShowLicense;
    QAction *actShowAbout;

    QLabel *lblMySQLPort;
    QLabel *lblMySQLUser;
    QLabel *lblMySQLDataBase;
    QLabel *lblTCPPort;

    QMap<QByteArray, QTcpSocket*> _sockets;
    void _createActions();
    void _createMenus();
    void _sendToClient(QTcpSocket *socket,
                      quint8 reason,
                      QVector<QVariant> data);
    void _saveLog(QString logFilename);
    void _clientSignIn(QTcpSocket *clientSocket,
                       QString login,
                       QString password,
                       SignInPasswordType passwordType,
                       quint32 clientIP,
                       QString &log_string);
protected:
    virtual void closeEvent(QCloseEvent *);
public:
    TimePlannerServer(QWidget *parent = 0);
    ~TimePlannerServer();

public slots:
    void newConncetion();
    void receiveClientQuery();
    void log(QString msg);
    void logNow(QString msg = "");
    void showHide();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason);
    void onAppQuit();
    void newNotification(QString caption, QString content);

private slots:
    void saveLog();
    void printLog();
    void clearLog();
    void connectMySQL();
    void disconnectMySQL();
    void runTCP();
    void stopTCP();
    void addUser();
    void editUser();
    void deleteUser();
    void showLicense();
    void showAbout();
};

#endif // MAINWINDOW_H
