#include "edituserdialog.h"

EditUserDialog::EditUserDialog(QVector<QVariant> &initData)
{
    setWindowTitle(tr("Edit user"));
    quint8 layoutRow = 0;
    lblLogin = new QLabel(tr("Login"));
    lblResetedPasswordLbl = new QLabel(tr("New password"));
    lblResetedPasswordLbl->setVisible(false);
    lblResetedPassword = new QLabel;
    lblResetedPassword->setVisible(false);
    QFont resetPasswordFont = lblResetedPassword->font();
    resetPasswordFont.setBold(true);
    resetPasswordFont.setPointSize(14);
    lblResetedPassword->setFont(resetPasswordFont);
//    actCopyResetedPassword = new QAction(tr("Copy new password"), this);
//    lblResetedPassword->addAction(actCopyResetedPassword);
//    connect(actCopyResetedPassword, &QAction::triggered,
//            this, &EditUserDialog::copyResetedPassword);
    lblFirstName = new QLabel(tr("First name"));
    lblSecondName = new QLabel(tr("Second name"));
    lblLastName = new QLabel(tr("Last name"));
    lblPosition = new QLabel(tr("Position"));
    txtLogin = new QLineEdit;
    txtLogin->setPlaceholderText(tr("Login"));
    txtLogin->setText(initData[layoutRow++].toString());
    btnResetPassword = new QPushButton(tr("Reset password"));
    connect(btnResetPassword, QPushButton::clicked,
            this, &EditUserDialog::onBtnResetPasswordClicked);
    txtFirstName = new QLineEdit;
    txtFirstName->setPlaceholderText(tr("First name"));
    txtFirstName->setText(initData[layoutRow++].toString());
    txtSecondName = new QLineEdit;
    txtSecondName->setPlaceholderText(tr("Second name"));
    txtSecondName->setText(initData[layoutRow++].toString());
    txtLastName = new QLineEdit;
    txtLastName->setPlaceholderText(tr("Last name"));
    txtLastName->setText(initData[layoutRow++].toString());
    txtPosition = new QLineEdit;
    txtPosition->setPlaceholderText(tr("Position"));
    txtPosition->setText(initData[layoutRow++].toString());
    chkbxCreateTaskRight = new QCheckBox("This user is allowed to create tasks");
    chkbxCreateTaskRight->setChecked(initData[layoutRow++].toBool());
    btnSave = new QPushButton(tr("Save user"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnSave, &QPushButton::clicked,
            this, &EditUserDialog::onBtnSaveClicked);
    connect(btnCancel, &QPushButton::clicked,
            this, &EditUserDialog::onBtnCancelClicked);
    layoutRow = 0;
    layout = new QGridLayout(this);
    layout->addWidget(lblLogin, layoutRow, 0);
    layout->addWidget(txtLogin, layoutRow++, 1);
    layout->addWidget(btnResetPassword, layoutRow++, 1);
    layout->addWidget(lblResetedPasswordLbl, layoutRow, 0);
    layout->addWidget(lblResetedPassword, layoutRow++, 1);
    layout->addWidget(lblFirstName, layoutRow, 0);
    layout->addWidget(txtFirstName, layoutRow++, 1);
    layout->addWidget(lblSecondName, layoutRow, 0);
    layout->addWidget(txtSecondName, layoutRow++, 1);
    layout->addWidget(lblLastName, layoutRow, 0);
    layout->addWidget(txtLastName, layoutRow++, 1);
    layout->addWidget(lblPosition, layoutRow, 0);
    layout->addWidget(txtPosition, layoutRow++, 1);
    layout->addWidget(chkbxCreateTaskRight, layoutRow++, 0, 1, 2);
    layout->addWidget(btnSave, layoutRow, 0);
    layout->addWidget(btnCancel, layoutRow++, 1);
}

EditUserDialog::~EditUserDialog()
{

}

void EditUserDialog::onBtnSaveClicked()
{
    if (txtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Login"),
                            tr("No login entered."),
                            QMessageBox::Ok
                            );
        return;
    }
    this->accept();
}

void EditUserDialog::onBtnCancelClicked()
{
    this->reject();
}

void EditUserDialog::onBtnResetPasswordClicked()
{
    QMessageBox::StandardButton confirmResetPassword;
    confirmResetPassword = QMessageBox::question(this,
                                              tr("Reset password"),
                                              tr("Are you sure you want to reset password?"),
                                              QMessageBox::Yes|QMessageBox::No);
    if (confirmResetPassword == QMessageBox::Yes)
    {
        QString newPassword("");
        lblResetedPassword->setText(newPassword);
        for (quint8 i = 0; i < 8; ++i)
        {
            quint8 symbolCategory = qrand() % 3;
            switch (symbolCategory)
            {
            case 0:
                newPassword.push_back(QChar('0' + qrand() % 10));
                break;
            case 1:
                newPassword.push_back(QChar('A' + qrand() % 26));
                break;
            case 2:
                newPassword.push_back(QChar('a' + qrand() % 26));
                break;
            }
        }
        lblResetedPassword->setText(newPassword);
        lblResetedPasswordLbl->setVisible(true);
        lblResetedPassword->setVisible(true);
    }
}

void EditUserDialog::copyResetedPassword()
{
    QApplication::clipboard()->setText(lblResetedPassword->text());
}
