#include "timeplanner_server.h"
// TODO: automatic sign_out
TimePlannerServer::TimePlannerServer(QWidget *parent)
    : QMainWindow(parent)
{
    qsrand(QDateTime::currentDateTime().toTime_t());

    setWindowIcon(QIcon(":/Icons/TimePlanne_Server.ico"));
    setWindowTitle(tr("TimePlanner Server"));

    centralWidget = new QWidget;
    txtLog = new QTextEdit;
    txtLog->setReadOnly(true);
    sqlConnector = new SqlConnector;
    tcpServer = new QTcpServer(this);
    vbltCentralWidgetLayout = new QVBoxLayout;
    vbltCentralWidgetLayout->addWidget(txtLog);
    vbltCentralWidgetLayout->addWidget(sqlConnector);
    centralWidget->setLayout(vbltCentralWidgetLayout);
    setCentralWidget(centralWidget);

    _createActions();
    _createMenus();

    lblMySQLPort = new QLabel(STATUS_LBL_MYSQL_PORT, this);
    lblMySQLUser = new QLabel(STATUS_LBL_MYSQL_USER, this);
    lblMySQLDataBase = new QLabel(STATUS_LBL_MYSQL_DATABASE, this);
    lblTCPPort = new QLabel(STATUS_LBL_TCP_PORT, this);
    statusBar()->addWidget(lblMySQLPort);
    statusBar()->addWidget(lblMySQLUser);
    statusBar()->addWidget(lblMySQLDataBase);
    statusBar()->addWidget(lblTCPPort);
    resize(640, 480);

    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(systemTrayIconContextMenu);
    systemTrayIcon->setToolTip("TimePlanner Server");
    systemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner_Server.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &TimePlannerServer::onTrayIconActivated);
}

void TimePlannerServer::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}
void TimePlannerServer::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
        showHide();
}
void TimePlannerServer::closeEvent(QCloseEvent *event)
{
    if (systemTrayIcon->isVisible())
        hide();
    event->ignore();
}
void TimePlannerServer::onAppQuit()
{
    QMessageBox::StandardButton confirmQuit;
    confirmQuit = QMessageBox::question(this,
                                        tr("Quit TimePlanner Server"),
                                        tr("Are you sure you want to quit TimePlanner Server?"),
                                        QMessageBox::Yes|QMessageBox::No);
    if (confirmQuit == QMessageBox::Yes)
    {
        logNow(tr("TimePlanner Server closed"));
        QDir logsDir(QDir::currentPath()
                     .append("/logs/"));
        if (!logsDir.exists()) {
            logsDir.mkpath(".");
        }
        _saveLog(logsDir.path()
                 .append("/")
                 .append(QDateTime::currentDateTimeUtc().toString("dd.MM.yyyy_hh.mm.ss.zzz"))
                 .append("_TimePlannerServerQuit.log"));
        qApp->quit();
    }
}

void TimePlannerServer::newNotification(QString caption, QString content)
{
   systemTrayIcon->showMessage(caption, content);
}

void TimePlannerServer::_createActions()
{
    actShowHide = new QAction(tr("Show/Hide"), this);
    connect(actShowHide, &QAction::triggered,
            this, &TimePlannerServer::showHide);
    actQuit = new QAction(tr("Quit"), this);
    connect(actQuit,&QAction::triggered,
            this, &TimePlannerServer::onAppQuit);

    actSaveLog = new QAction(tr("&Save"), this);
    actSaveLog->setShortcuts(QKeySequence::Save);
    actSaveLog->setStatusTip(tr("Save the log to disk"));
    connect(actSaveLog, &QAction::triggered,
            this, &TimePlannerServer::saveLog);

    actPrintLog = new QAction(tr("&Print"), this);
    actPrintLog->setShortcuts(QKeySequence::Print);
    actPrintLog->setStatusTip(tr("Print the log"));
    connect(actPrintLog, &QAction::triggered,
            this, &TimePlannerServer::printLog);

    actClearLog = new QAction(tr("&Clear"), this);
    actClearLog->setStatusTip(tr("Clear the log"));
    connect(actClearLog, &QAction::triggered,
            this, &TimePlannerServer::clearLog);

    actConnectMySQL = new QAction(tr("&Connect MySQL"), this);
    actConnectMySQL->setStatusTip(tr("Connect to working MySQL Server"));
    connect(actConnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::connectMySQL);

    actDisconnectMySQL = new QAction(tr("&Disconnect MySQL"), this);
    actDisconnectMySQL->setStatusTip(tr("Disconnect from working MySQL Server"));
    connect(actDisconnectMySQL, &QAction::triggered,
            this, &TimePlannerServer::disconnectMySQL);

    actRunTCP = new QAction(tr("&Run TCP"), this);
    actRunTCP->setStatusTip(tr("Run TCP Server"));
    connect(actRunTCP, &QAction::triggered,
            this, &TimePlannerServer::runTCP);

    actStopTCP = new QAction(tr("&Stop TCP"), this);
    actStopTCP->setStatusTip(tr("Stop TCP Server"));
    connect(actStopTCP, &QAction::triggered,
            this, &TimePlannerServer::stopTCP);

    actAddUser = new QAction(tr("&Add User"), this);
    actAddUser->setStatusTip(tr("Create new user"));
    actAddUser->setShortcuts(QKeySequence::New);
    connect(actAddUser, &QAction::triggered,
            this, &TimePlannerServer::addUser);

    actEditUser = new QAction(tr("&Edit User"), this);
    actEditUser->setStatusTip(tr("Edit existing user"));
    actEditUser->setShortcuts(QKeySequence::Find);
    connect(actEditUser, &QAction::triggered,
            this, &TimePlannerServer::editUser);

    actDeleteUser = new QAction(tr("&Delete User"), this);
    actDeleteUser->setStatusTip(tr("Delete existing user"));
    actDeleteUser->setShortcuts(QKeySequence::Delete);
    connect(actDeleteUser, &QAction::triggered,
            this, &TimePlannerServer::deleteUser);

    actShowLicense = new QAction(tr("&License"), this);
    actShowLicense->setStatusTip(tr("Show license"));
    connect(actShowLicense, &QAction::triggered,
            this, &TimePlannerServer::showLicense);

    actShowAbout = new QAction(tr("&About"), this);
    actShowAbout->setStatusTip(tr("About TimePlanner Server"));
    actShowAbout->setShortcuts(QKeySequence::HelpContents);
    connect(actShowAbout, &QAction::triggered,
            this, &TimePlannerServer::showAbout);
}

void TimePlannerServer::_createMenus()
{
    systemTrayIconContextMenu = new QMenu(this);
    systemTrayIconContextMenu->addAction(actShowHide);
    systemTrayIconContextMenu->addAction(actQuit);

    menuLog = menuBar()->addMenu(tr("&Log"));
    menuLog->addAction(actSaveLog);
    menuLog->addAction(actPrintLog);
    menuLog->addSeparator();
    menuLog->addAction(actClearLog);
    menuMySQL = menuBar()->addMenu(tr("&MySQL"));
    menuMySQL->addAction(actConnectMySQL);
    menuMySQL->addAction(actDisconnectMySQL);
    menuTCP = menuBar()->addMenu(tr("TCP"));
    menuTCP->addAction(actRunTCP);
    menuTCP->addAction(actStopTCP);
    menuUsers = menuBar()->addMenu(tr("Users"));
    menuUsers->addAction(actAddUser);
    menuUsers->addAction(actEditUser);
    menuUsers->addSeparator();
    menuUsers->addAction(actDeleteUser);
    menuAbout = menuBar()->addMenu(tr("About"));
    menuAbout->addAction(actShowLicense);
    menuAbout->addAction(actShowAbout);
    menuMySQL->actions().at(1)->setEnabled(false);
    menuTCP->actions().at(1)->setEnabled(false);
}

void TimePlannerServer::saveLog()
{
    QString logFileName = QFileDialog::getSaveFileName(this,
                                                      tr("Save log as"),
                                                      "",
                                                      tr("Log files (*.log)"));
    if (!logFileName.isEmpty())
        _saveLog(logFileName);
}

void TimePlannerServer::printLog()
{
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog printDialog(&printer, this);
    printDialog.setWindowTitle(tr("Print log"));
    if (txtLog->textCursor().hasSelection())
        printDialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);
    if (printDialog.exec() == QDialog::Accepted)
        txtLog->document()->print(&printer);
}

void TimePlannerServer::clearLog()
{
    QMessageBox::StandardButton confirmClearDialog;
    confirmClearDialog = QMessageBox::question(this,
                                               tr("Clear the log"),
                                               tr("Are you sure you want to clear the log?"),
                                               QMessageBox::Yes|QMessageBox::No);
    if (confirmClearDialog == QMessageBox::Yes)
        txtLog->clear();
}

void TimePlannerServer::connectMySQL()
{
    QString databaseName;
    QString log_string;
    int MySQL_port = 3306;
    bool getLoginOk = false;
    QString mysql_login = QInputDialog::getText(this,
                                                   tr("Connect to MySQL"),
                                                   tr("MySQL login"),
                                                   QLineEdit::Normal,
                                                   "root",
                                                   &getLoginOk);
    bool getPasswordOk = false;
    QString mysql_password;
    if (getLoginOk)
        mysql_password = QInputDialog::getText(this,
                                               tr("Connect to MySQL"),
                                               tr("MySQL password"),
                                               QLineEdit::Normal,
                                               "",
                                               &getPasswordOk);
    if (getLoginOk && getPasswordOk)
    {
        bool getDatabaseNameOk = false;
        databaseName = QInputDialog::getText(this,
                                               tr("Connect to MySQL"),
                                               tr("MySQL database"),
                                               QLineEdit::Normal,
                                               "timeplanner_db",
                                               &getDatabaseNameOk);
        if (getDatabaseNameOk)
        {
            if(!sqlConnector->connect_db(mysql_login,
                                         mysql_password,
                                         databaseName,
                                         MySQL_port,
                                         log_string))
            {
                QMessageBox::critical(0,
                                      tr("Connection to MySQL"),
                                      tr("Unable to establish a database connection."),
                                      QMessageBox::Cancel
                                      );
                logNow(log_string);
                return;
            }
        }
        else
        {
            return;
        }
    }
    else
    {
        return;
    }
    logNow(log_string);
    lblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT).append("%1").arg(MySQL_port));
    lblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER).append(mysql_login));
    lblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE).append(databaseName));
    menuMySQL->actions().at(0)->setEnabled(false);
    menuMySQL->actions().at(1)->setEnabled(true);
    log(tr("Successful connection to MySQL Server"));
}

void TimePlannerServer::disconnectMySQL()
{
    QMessageBox::StandardButton confirmDisconnectMySQLDialog;
    confirmDisconnectMySQLDialog = QMessageBox::question(this,
                                                         tr("Disconnect from MySQL Server"),
                                                         tr("Are you sure you want to disconnect from MySQL Server?"),
                                                         QMessageBox::Yes|QMessageBox::No);
    if (confirmDisconnectMySQLDialog == QMessageBox::Yes)
    {
        sqlConnector->disconnect_db();
        lblMySQLPort->setText(QString(STATUS_LBL_MYSQL_PORT));
        lblMySQLUser->setText(QString(STATUS_LBL_MYSQL_USER));
        lblMySQLDataBase->setText(QString(STATUS_LBL_MYSQL_DATABASE));
        menuMySQL->actions().at(0)->setEnabled(true);
        menuMySQL->actions().at(1)->setEnabled(false);
        logNow("Disconnected from MySQL Server");
    }
}

void TimePlannerServer::runTCP()
{
    bool tcp_port_selected = false;
    quint16 tcp_port = QInputDialog::getInt(this,
                                            tr("Select TCP port"),
                                            tr("TCP port:"),
                                            INITIAL_PORT,
                                            0,
                                            65535,
                                            1,
                                            &tcp_port_selected);
    if (tcp_port_selected)
    {
        logNow(tr("Starting TCP Server at %1 port.").arg(tcp_port));
        connect(tcpServer, &QTcpServer::newConnection,
                this, &TimePlannerServer::newConncetion);
        if (!tcpServer->listen(QHostAddress::Any, tcp_port))
        {
            QMessageBox::critical(0,
                                  tr("Cannot start TCP server"),
                                  tr("Unable to start a TCP server."),
                                  QMessageBox::Cancel);
            tcpServer->close();
            return;
        }
    }
    else
    {
        return;
    }
    lblTCPPort->setText(QString(STATUS_LBL_TCP_PORT).append("%1").arg(tcp_port));
    menuTCP->actions().at(0)->setEnabled(false);
    menuTCP->actions().at(1)->setEnabled(true);
    log(tr("TCP server has been run successfully"));
}

void TimePlannerServer::stopTCP()
{
    QMessageBox::StandardButton confirmStopTCPDialog;
    confirmStopTCPDialog = QMessageBox::question(this,
                                                 tr("Stop TCP Server"),
                                                 tr("Are you sure you want to stop TCP Server?"),
                                                 QMessageBox::Yes|QMessageBox::No);
    if (confirmStopTCPDialog == QMessageBox::Yes)
    {
        tcpServer->close();
        lblTCPPort->setText(QString(STATUS_LBL_TCP_PORT));
        menuTCP->actions().at(0)->setEnabled(true);
        menuTCP->actions().at(1)->setEnabled(false);
        logNow(tr("TCP Server stoped"));
    }
}

void TimePlannerServer::addUser()
{
    AddUserDialog addUserDialog;
    addUserDialog.setModal(true);
    if (addUserDialog.exec())
    {
        QString log_string;
        sqlConnector->admin_insert_user(addUserDialog.getLogin(),
                                        addUserDialog.getPassword(),
                                        addUserDialog.getFirstName(),
                                        addUserDialog.getSecondame(),
                                        addUserDialog.getLastName(),
                                        addUserDialog.getPosition(),
                                        addUserDialog.getCreateTaskRight(),
                                        log_string);
        logNow(log_string);
    }
}

void TimePlannerServer::editUser()
{
    QString log_string;
    bool getLoginOk = false;
    QString login = QInputDialog::getText(this, tr("Edit user"), tr("Login"), QLineEdit::Normal, "", &getLoginOk);
    if (getLoginOk)
    {
        QVector<QVariant> user = sqlConnector->admin_get_user_by_login(login, log_string);
        logNow(log_string);
        if (user.empty())
        {
            QMessageBox::warning(0,
                                 tr("Edit user"),
                                 tr("User %1 not found.").arg(login),
                                 QMessageBox::Cancel);
            return;
        }
        else
        {
            EditUserDialog editUserDialog(user);
            editUserDialog.setModal(true);
            if (editUserDialog.exec())
            {
                log_string.clear();
                QByteArray forceSignOutSession = sqlConnector->get_session_by_login(login, log_string);
                if (!forceSignOutSession.isEmpty())
                {
                    QTcpSocket *socket = _sockets[forceSignOutSession];
                    if (socket != NULL)
                    {
                        _sendToClient(socket, MSG_FORCE_SIGN_OUT, QVector<QVariant>());
                        socket->waitForBytesWritten();
                        socket->close();
                        _sockets.remove(forceSignOutSession);
                    }
                }
                sqlConnector->force_sign_out(login,
                                             log_string);
                sqlConnector->admin_edit_user(login,
                                              editUserDialog.getLogin(),
                                              editUserDialog.getResetedPassword(),
                                              editUserDialog.getFirstName(),
                                              editUserDialog.getSecondame(),
                                              editUserDialog.getLastName(),
                                              editUserDialog.getPosition(),
                                              editUserDialog.getCreateTaskRight(),
                                              log_string);
                logNow(log_string);
            }
        }
    }
}

void TimePlannerServer::deleteUser()
{
    bool getLoginOk = false;
    QString login = QInputDialog::getText(this,
                                          tr("Delete user"),
                                          tr("Login"),
                                          QLineEdit::Normal,
                                          "",
                                          &getLoginOk);

    if (getLoginOk)
    {
        QString log_string;
        QVector<QVariant> user = sqlConnector->admin_get_user_by_login(login, log_string);
        logNow(log_string);
        if (user.empty())
        {
            QMessageBox::warning(0,
                                 tr("Delete user"),
                                 tr("User %1 not found.").arg(login),
                                 QMessageBox::Cancel);
            return;
        }
        else
        {
            QMessageBox::StandardButton confirmDeleteUser;
            confirmDeleteUser = QMessageBox::question(this,
                                                      tr("Delete user"),
                                                      tr("Are you sure you want to delete %1 (%2: %3 %4 %5)?")
                                                      .arg(user[0].toString())
                                                      .arg(user[4].toString())
                                                      .arg(user[3].toString())
                                                      .arg(user[1].toString())
                                                      .arg(user[2].toString()),
                                                      QMessageBox::Yes|QMessageBox::No);
            if (confirmDeleteUser == QMessageBox::Yes)
            {
                log_string.clear();
                QByteArray forceSignOutSession = sqlConnector->get_session_by_login(login, log_string);
                if (!forceSignOutSession.isEmpty())
                {
                    QTcpSocket *socket = _sockets[forceSignOutSession];
                    if (socket != NULL)
                    {
                        logNow(QString("Force sign out %1 user with %2 session").arg(login).arg(QString(forceSignOutSession)));
                        _sendToClient(socket, MSG_FORCE_SIGN_OUT, QVector<QVariant>());
                        socket->close();
                        _sockets.remove(forceSignOutSession);
                    }
                }
                sqlConnector->force_sign_out(login, log_string);
                sqlConnector->admin_delete_user(login, log_string);
                log(log_string);
            }
        }
    }
}

void TimePlannerServer::showLicense()
{
//    QFile licenseFile(tr("License_ENG.txt"));
//    if (!licenseFile.open(QFile::ReadOnly | QFile::Text))
//    {
//        QMessageBox::information(this, tr("License"), tr("License file not found"),QMessageBox::Ok);
//        return;
//    }
//    QTextStream in(&licenseFile);
    QMessageBox::information(this, tr("License"), tr("GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007"), QMessageBox::Ok);
}

void TimePlannerServer::showAbout()
{
    QMessageBox::information(this, tr("About TimePlanner Server"), tr("This is TimePlanner Server application"),QMessageBox::Ok);
}

TimePlannerServer::~TimePlannerServer()
{

}

void TimePlannerServer::newConncetion()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();\
    connect(clientSocket, &QTcpSocket::disconnected,
            clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead,
            this, &TimePlannerServer::receiveClientQuery);
    logNow(tr("New connection from %7").arg(clientSocket->peerAddress().toString()));
    QVector<QVariant> data;
    data.push_back(QVariant("Server response: Connected."));
    _sendToClient(clientSocket, MSG_MESSAGE, data);
}

void TimePlannerServer::receiveClientQuery()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QDataStream inputDataStream(clientSocket);
    inputDataStream.setVersion(QDataStream::Qt_5_5);
    qint64 blocksize = 0;
    qint8 clientQueryType;
    logNow(tr("New query from %7").arg(clientSocket->peerAddress().toString()));
    if (clientSocket->bytesAvailable() < sizeof(qint64) + sizeof(qint8))
    {
        return;
    }
    QString login;
    QString password, temp_password;
    quint32 clientIP = clientSocket->peerAddress().toIPv4Address();
    QByteArray session;
    QString caption;
    QString content;
    QDateTime deadline;
    QString assignee;
    QVector<QVariant> return_to_client;
    QString log_string;
    QVector<Task> tasks;
    Role role;
    QVariant task;
    qint32 task_id;
    QString status;
    inputDataStream >> blocksize >> clientQueryType;
    switch(clientQueryType)
    {
    case MSG_SIGN_IN:
        inputDataStream >> login >> password;
        if (sqlConnector->check_temp_password(login, password, clientIP, log_string))
        {
            logNow(log_string);
            _sendToClient(clientSocket, MSG_TEMP_PASSWORD, return_to_client);
            qDebug() << "Need to enter new password";
            return;
        }
        _clientSignIn(clientSocket, login, password, NORMAL, clientIP, log_string);
        break;
    case MSG_TEMP_PASSWORD:
        inputDataStream >> login >> temp_password >> password;
        qDebug() << "Temp password: " << temp_password << " New password: " << password;
        if (sqlConnector->check_temp_password(login, temp_password, clientIP, log_string))
        {
            _clientSignIn(clientSocket, login, password, NEW, clientIP, log_string);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_SIGN_OUT:
        inputDataStream >> session;
        sqlConnector->sign_out(session, log_string);
        _sockets.remove(session);
        logNow(tr("Sign out %1").arg(QString(session.toHex())));
        break;
    case MSG_USERS:
        inputDataStream >> session;
        if (sqlConnector->get_users(session, return_to_client, log_string))
        {
            _sendToClient(clientSocket, MSG_USERS, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_CREATE:
        inputDataStream >> session >> assignee >> caption >> content >> deadline;
        if (sqlConnector->create_task(session, caption, content, assignee, deadline, log_string))
        {
            return_to_client.push_back(QVariant("Task successfully created!"));
            _sendToClient(clientSocket, MSG_MESSAGE, return_to_client);

            QByteArray notifySession = sqlConnector->get_session_by_login(assignee, log_string);
            logNow(log_string);
            if (!notifySession.isEmpty())
            {
                QVector<QVariant> task;
                task.push_back(caption);
                task.push_back(content);
                task.push_back(deadline);
                QMap<QByteArray, QTcpSocket*>::Iterator it = _sockets.find(notifySession);
                if ( it != _sockets.end())
                {
                    _sendToClient(it.value(), MSG_NOTIFY_NEW_TASK, task);
                }
            }
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_EDIT:
        inputDataStream >> session >> task_id >> caption >> content >> deadline >> assignee;
        if (sqlConnector->edit_task(session, task_id, caption, content, assignee, deadline, log_string))
        {
            logNow(log_string);
            return_to_client.push_back(tr("Task edited successfully!"));
            _sendToClient(clientSocket, MSG_MESSAGE, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_EDIT_STATUS:
        inputDataStream >> session >> task_id >> status;
        if (sqlConnector->edit_task_status(session, task_id, status, log_string))
        {
            logNow(log_string);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        inputDataStream >> session;
        if (clientQueryType == MSG_TASK_USER_CREATOR)
            role = CREATOR_ROLE;
        else if (clientQueryType == MSG_TASK_USER_ASSIGNEE)
            role = ASSIGNEE_ROLE;
        if (sqlConnector->get_tasks(session, tasks, role, log_string))
        {
            return_to_client.push_back(tasks.size());
            for (int i = 0; i < tasks.size(); ++i)
            {
                task.setValue(tasks[i]);
                return_to_client.push_back(task);
            }
            _sendToClient(clientSocket, clientQueryType, return_to_client);
        }
        else
        {
            return_to_client.push_back(QVariant(ERROR_SESSION_FAILURE));
            _sendToClient(clientSocket, MSG_ERROR, return_to_client);
        }
        break;
    // TODO: other client query types
    default:
        inputDataStream.skipRawData(clientSocket->bytesAvailable());
    }
}

void TimePlannerServer::_sendToClient(QTcpSocket *socket,
                  quint8 serverAnswerType,
                  QVector<QVariant> data)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
        return;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);
    out << quint64(0) << serverAnswerType;
    qint32 tasks_num;
    switch (serverAnswerType)
    {
    case MSG_ERROR:
        logNow(tr("Sending %1 error to %2\n")
            .arg(data[0].toInt())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toInt()          // Error code
                ;
        break;
    case MSG_SIGN_IN:
        logNow(tr("Sending successful sign in to %1\n")
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // First name
            << data[1].toString()       // Second name
            << data[2].toString()       // Last name
            << data[3].toString()       // Position
            << data[4].toBool()         // Create tasks right
            << data[5].toByteArray()    // Session ID
                ;
        break;
    case MSG_TEMP_PASSWORD:
        break;
    case MSG_SIGN_OUT:
        break;
    case MSG_FORCE_SIGN_OUT:
        break;
    case MSG_MESSAGE:
        logNow(tr("Sending \"%1\" to %2\n")
            .arg(data[0].toString())
            .arg(socket->peerAddress().toString())
                );
        out << data[0].toString()       // Message
                ;
        break;
    case MSG_USERS:
        logNow(tr("Sending users to %1\n")
                .arg(socket->peerAddress().toString())
                );
        out << data.size();
        for(int i = 0; i < data.size(); ++i)
            out << data[i].toString();
        break;
    case MSG_TASK_USER_CREATOR:
    case MSG_TASK_USER_ASSIGNEE:
        tasks_num = data[0].toInt();
        logNow(tr("Sending %1 tasks to %2\n")
            .arg(tasks_num)
            .arg(socket->peerAddress().toString()));
        out << tasks_num;
        for (int i = 1; i <= tasks_num; ++i)
            out << data[i].value<Task>().task_id
                << data[i].value<Task>().caption
                << data[i].value<Task>().content
                << data[i].value<Task>().modified
                << data[i].value<Task>().deadline
                << data[i].value<Task>().another_user_login
                << data[i].value<Task>().status;
        break;
    case MSG_NOTIFY_NEW_TASK:
        logNow(tr("Sending new task notification to %1").arg(socket->peerAddress().toString()));
        out << data[0].toString()
            << data[1].toString()
            << data[2].toString();
        break;
        // TODO: other server answer types
    }
    out.device()->seek(0);
    out << quint64(block.size() - sizeof(quint64));
    if (socket->isWritable())
        socket->write(block);
    socket->waitForBytesWritten();
}

void TimePlannerServer::log(QString msg)
{
    txtLog->append(msg);
}

void TimePlannerServer::logNow(QString msg)
{
    QDateTime now = QDateTime::currentDateTimeUtc();
    log(now.toString("dd.MM.yyyy hh:mm:ss.zzz %1").arg(msg));
}

void TimePlannerServer::_saveLog(QString logFilename)
{
    QFile logFile(logFilename);
    if (logFile.open(QIODevice::WriteOnly))
    {
        QTextStream logStream(&logFile);
        logStream << txtLog->toPlainText();
        logFile.close();
    }
    else
        QMessageBox::warning(0,
                             tr("Save log"),
                             tr("Unable to save %1").arg(logFilename),
                             QMessageBox::Cancel);
}

void TimePlannerServer::_clientSignIn(QTcpSocket *clientSocket,
                                      QString login,
                                      QString password,
                                      SignInPasswordType passwordType,
                                      quint32 clientIP,
                                      QString &log_string)
{
    QVector<QVariant> return_to_client;
    switch (passwordType)
    {
    case NORMAL:
        return_to_client = sqlConnector->sign_in(login, password, clientIP, log_string);
        break;
    case NEW:
        return_to_client = sqlConnector->sign_in_with_new_password(login, password, clientIP, log_string);
        break;
    }
    logNow(log_string);
    log_string.clear();
    if (return_to_client.isEmpty())
    {
        // User (with such login and password) not found
        return_to_client.push_back(QVariant(ERROR_SIGN_IN_FAILURE));
        _sendToClient(clientSocket, MSG_ERROR, return_to_client);
    }
    else
    {
        _sockets.insert(return_to_client[5].toByteArray(), clientSocket);
        _sendToClient(clientSocket, MSG_SIGN_IN, return_to_client);
    }
}
