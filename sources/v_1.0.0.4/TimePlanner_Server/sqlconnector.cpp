#include "sqlconnector.h"

SqlConnector::SqlConnector(QWidget *parent)
    : QWidget(parent)
{

}
SqlConnector::~SqlConnector()
{
    disconnect_db();
}

bool SqlConnector::connect_db(QString mysql_login, QString mysql_password, QString database_name, int mysql_port, QString &log_string)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setPort(mysql_port);
    db.setUserName(mysql_login);
    db.setPassword(mysql_password);
    db.open();
    log_string.append(QString("Establishing database connection:\nDriver name: %1\nMySQL Host: %2\nMySQL Port: %3\nMySQL User: %4\nMySQL Database: %5.\n")
                     .arg(db.driverName())
                     .arg(db.hostName())
                     .arg(db.port())
                     .arg(db.userName())
                     .arg(database_name)
                     );
    QSqlQuery check_connection_query;
    check_connection_query.exec(SQL_USE_UNEXISTING_DATABASE);
    log_string.append(QString("Trying to find running MySQL Server\n(1049 error — server found,\n 2006 error — server not found):\n %1 %2 %3.")
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_NO_CONNECTION)
    {
        return false;
    }
    check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
    log_string.append(QString("\nTrying to find %1 database:\n %2 %3. %4.")
                     .arg(database_name)
                     .arg(check_connection_query.lastError().nativeErrorCode())
                     .arg(check_connection_query.lastError().driverText())
                     .arg(check_connection_query.lastError().databaseText())
                     );
    if (check_connection_query.lastError().nativeErrorCode() == SQL_ERROR_UNKNOWN_DATABASE)
    {
        check_connection_query.exec(QString(SQL_CREATE_DATABASE).arg(database_name));
        log_string.append(QString("\nCreating new %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_USE_DATABASE).arg(database_name));
        log_string.append(QString("Use %1 database:\n %2 %3. %4.\n")
                         .arg(database_name)
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_USERS);
        log_string.append(QString("Creating `users` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_SESSIONS);
        log_string.append(QString("Creating `sessions` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASKS);
        log_string.append(QString("Creating `tasks` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TASK_STATUS);
        log_string.append(QString("Creating `task_status` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("Done"));
        log_string.append(QString("Fill `task_status` table with \'Done\' value:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("In progress"));
        log_string.append(QString("Fill `task_status` table with \'In progress\' value:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
        check_connection_query.exec(SQL_CREATE_TABLE_TEMP_PASSWORDS);
        log_string.append(QString("Creating `temp_passwords` table:\n %1 %2. %3.\n")
                         .arg(check_connection_query.lastError().nativeErrorCode())
                         .arg(check_connection_query.lastError().driverText())
                         .arg(check_connection_query.lastError().databaseText())
                         );
    }
    return true;
}

void SqlConnector::disconnect_db()
{
    db.close();
}

QVector<QVariant> SqlConnector::sign_in(QString login,
                                        QString password,
                                        quint32 clientIP,
                                        QString &log_string
                                        )
{
    // Get hash of the password
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    // Perform query
    QSqlQuery query_sign_in;

    QString query_string =
            QString(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)
            .arg(login)
            .arg(QString(hash_password.toHex()));
    query_sign_in.exec(query_string);
    log_string.append(QString("Sign in %1 query:\n %2 %3. %4.")
                     .arg(login)
                     .arg(query_sign_in.lastError().nativeErrorCode())
                     .arg(query_sign_in.lastError().driverText())
                     .arg(query_sign_in.lastError().databaseText())
                     );
    // Read query answer
    QVector<QVariant> data;
    quint32 user_id;
    if (query_sign_in.next())
    {
        // Generate new session and insert it into sessions database table
        QByteArray session = _generate_session_id(clientIP);
        user_id = query_sign_in.value(0).toInt();
        data.push_back(query_sign_in.value(3)); // First name
        data.push_back(query_sign_in.value(4)); // Second name
        data.push_back(query_sign_in.value(5)); // Last name
        data.push_back(query_sign_in.value(6)); // Position
        data.push_back(query_sign_in.value(7)); // Create tasks right
        data.push_back(session); // Session Id
        query_sign_in.exec(QString(SQL_DELETE_SESSIONS).arg(user_id));
        query_string =
                QString(SQL_INSERT_NEW_SESSION)
                .arg(QString(session.toHex()))
                .arg(user_id)
                .arg(clientIP)
                ;
        query_sign_in.exec(query_string);
        log_string.append(QString("\nCreate session %1 query:\n %2 %3. %4.")
                         .arg(QString(session.toHex()))
                         .arg(query_sign_in.lastError().nativeErrorCode())
                         .arg(query_sign_in.lastError().driverText())
                         .arg(query_sign_in.lastError().databaseText())
                         );
    }
    // If user not found the data vector will be empty.
    return data;
}

QVector<QVariant> SqlConnector::sign_in_with_new_password(QString login, QString password, quint32 clientIP, QString &log_string)
{
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    QSqlQuery query_sign_in_with_new_password;
    QString query_string = QString(SQL_DELETE_TEMP_PASSWORD)
                            .arg(login);
    query_sign_in_with_new_password.exec(query_string);
    query_string = QString(SQL_EDIT_USER_PASSWORD)
                    .arg(QString(hash_password.toHex()))
                    .arg(login);
    query_sign_in_with_new_password.exec(query_string);
    log_string.append(QString("Delete temporary password at %1 user  query:\n %2 %3. %4.")
                     .arg(login)
                     .arg(query_sign_in_with_new_password.lastError().nativeErrorCode())
                     .arg(query_sign_in_with_new_password.lastError().driverText())
                     .arg(query_sign_in_with_new_password.lastError().databaseText())
                     );
    return sign_in(login, password, clientIP, log_string);
}

bool SqlConnector::check_temp_password(QString login, QString password, quint32 clientIP, QString &log_string)
{
    QSqlQuery query_check_temp_password;
    QByteArray b_password(password.toStdString().c_str(), password.length());
    QByteArray hash_password = QCryptographicHash::hash(b_password, QCryptographicHash::Sha3_256);
    QString query_string = QString(SQL_CHECK_TEMP_PASSWORD)
                            .arg(login)
                            .arg(QString(hash_password.toHex()));
    query_check_temp_password.exec(query_string);
    log_string.append(QString("Check temp password %1 query:\n %2 %3. %4.")
                     .arg(login)
                     .arg(query_check_temp_password.lastError().nativeErrorCode())
                     .arg(query_check_temp_password.lastError().driverText())
                     .arg(query_check_temp_password.lastError().databaseText())
                     );
    bool result = false;
    while(query_check_temp_password.next())
    {
        result = true;
    }
    return result;
}

void SqlConnector::sign_out(QByteArray session, QString &log_string)
{
    QSqlQuery query_sign_out;
    QString query_string = QString(SQL_DELETE_SESSION)
                            .arg(QString(session.toHex()));
    query_sign_out.exec(query_string);
    log_string.append(QString("Sign out %1 query:\n %2 %3. %4.")
                      .arg(QString(session.toHex()))
                      .arg(query_sign_out.lastError().nativeErrorCode())
                      .arg(query_sign_out.lastError().driverText())
                      .arg(query_sign_out.lastError().databaseText())
                      );
}
void SqlConnector::force_sign_out(QString login, QString &log_string)
{
    QSqlQuery query_force_sign_out;
    QString query_string = QString(SQL_GET_UID_BY_LOGIN)
                            .arg(login);
    query_force_sign_out.exec(query_string);
    log_string.append(QString("Admin get user id by %1 login query:\n %3 %4. %5.")
                      .arg(login)
                      .arg(query_force_sign_out.lastError().nativeErrorCode())
                      .arg(query_force_sign_out.lastError().driverText())
                      .arg(query_force_sign_out.lastError().databaseText())
                      );
    qint32 user_id;
    if(query_force_sign_out.next())
    {
        user_id = query_force_sign_out.value(0).toInt();
        query_string = QString(SQL_FORCE_DELETE_SESSION)
                                .arg(user_id)
                                ;
        query_force_sign_out.exec(query_string);
        log_string.append(QString("Force sign out %1 query:\n %2 %3. %4.")
                          .arg(login)
                          .arg(query_force_sign_out.lastError().nativeErrorCode())
                          .arg(query_force_sign_out.lastError().driverText())
                          .arg(query_force_sign_out.lastError().databaseText())
                          );
    }
}
bool SqlConnector::get_users(QByteArray session, QVector<QVariant> &data, QString &log_string)
{
    QSqlQuery query_get_users;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_GET_USERS);
        query_get_users.exec(query_string);
        log_string.append(QString("Get users query by %1:\n %2 %3. %4.")
                          .arg(QString(session.toHex()))
                          .arg(query_get_users.lastError().nativeErrorCode())
                          .arg(query_get_users.lastError().driverText())
                          .arg(query_get_users.lastError().databaseText())
                          );
        while(query_get_users.next())
        {
            data.push_back( query_get_users.value(0).toString());
        }
    }
    return correct_session;
}

QByteArray SqlConnector::get_session_by_login(QString login, QString &log_string)
{
    QByteArray session;
    QSqlQuery query_get_session_by_login;
    QString query_string = QString(SQL_GET_UID_BY_LOGIN)
                            .arg(login);
    query_get_session_by_login.exec(query_string);
    log_string.append(QString("Get user id by %1 login query:\n %3 %4. %5.")
                      .arg(login)
                      .arg(query_get_session_by_login.lastError().nativeErrorCode())
                      .arg(query_get_session_by_login.lastError().driverText())
                      .arg(query_get_session_by_login.lastError().databaseText())
                      );
    qint32 user_id;
    if(query_get_session_by_login.next())
    {
        user_id = query_get_session_by_login.value(0).toInt();
        query_string = QString(SQL_GET_SESSION_BY_UID)
                                .arg(user_id)
                                ;
        query_get_session_by_login.exec(query_string);
        log_string.append(QString("Get session by %1 user id query:\n %2 %3. %4.")
                          .arg(user_id)
                          .arg(query_get_session_by_login.lastError().nativeErrorCode())
                          .arg(query_get_session_by_login.lastError().driverText())
                          .arg(query_get_session_by_login.lastError().databaseText())
                          );
        if (query_get_session_by_login.next())
            session = query_get_session_by_login.value(0).toByteArray();
    }
    return session;
}

void SqlConnector::admin_insert_user(QString login, QString password, QString firstName, QString secondName, QString lastName, QString position, bool createTaskRight, QString &log_string)
{
    QByteArray hash = QCryptographicHash::hash(QByteArray(password.toStdString().c_str(), password.length()), QCryptographicHash::Sha3_256).toHex();
    QSqlQuery query_insert_user;
    QString query_string = QString(SQL_INSERT_USER)
            .arg(login)
            .arg(QString(hash))
            .arg(firstName)
            .arg(secondName)
            .arg(lastName)
            .arg(position)
            .arg(createTaskRight)
            ;
    query_insert_user.exec(query_string);
    log_string.append(QString("Admin insert new %1 user query:\n %2 %3. %4.")
                      .arg(login)
                      .arg(query_insert_user.lastError().nativeErrorCode())
                      .arg(query_insert_user.lastError().driverText())
                      .arg(query_insert_user.lastError().databaseText())
                      );
    query_string = QString(SQL_INSERT_TEMP_PASSWORD)
                   .arg(QString(hash))
                   .arg(login);
    query_insert_user.exec(query_string);
    log_string.append(QString("Admin insert temp password for %1 user query:\n %2 %3. %4.")
                      .arg(login)
                      .arg(query_insert_user.lastError().nativeErrorCode())
                      .arg(query_insert_user.lastError().driverText())
                      .arg(query_insert_user.lastError().databaseText())
                      );
}

void SqlConnector::admin_edit_user(QString oldLogin, QString newLogin, QString newPassword, QString firstName, QString secondName, QString lastName, QString position, bool createTaskRight, QString &log_string)
{
    QSqlQuery query_edit_user;
    QString query_string = QString(SQL_EDIT_USER)
            .arg(newLogin)
            .arg(firstName)
            .arg(secondName)
            .arg(lastName)
            .arg(position)
            .arg(createTaskRight)
            .arg(oldLogin)
            ;
    bool successfullEditUser = query_edit_user.exec(query_string);
    log_string.append(QString("Admin edit %1->%2 user query:\n %3 %4. %5.")
                      .arg(oldLogin)
                      .arg(newLogin)
                      .arg(query_edit_user.lastError().nativeErrorCode())
                      .arg(query_edit_user.lastError().driverText())
                      .arg(query_edit_user.lastError().databaseText())
                      );
    if (successfullEditUser && !newPassword.isEmpty())
    {
        QByteArray hash = QCryptographicHash::hash(QByteArray(newPassword.toStdString().c_str(), newPassword.length()), QCryptographicHash::Sha3_256);
        query_string = QString(SQL_EDIT_USER_PASSWORD)
                    .arg(QString(hash.toHex()))
                    .arg(newLogin)
                    ;
        query_edit_user.exec(query_string);
        log_string.append(QString("Admin edit %1 user password query:\n %3 %4. %5.")
                          .arg(newLogin)
                          .arg(query_edit_user.lastError().nativeErrorCode())
                          .arg(query_edit_user.lastError().driverText())
                          .arg(query_edit_user.lastError().databaseText())
                          );
        query_string = QString(SQL_DELETE_TEMP_PASSWORD)
                       .arg(oldLogin);
        query_edit_user.exec(query_string);
        query_string = QString(SQL_DELETE_TEMP_PASSWORD)
                       .arg(newLogin);
        query_edit_user.exec(query_string);
        query_string = QString(SQL_INSERT_TEMP_PASSWORD)
                       .arg(QString(hash.toHex()))
                       .arg(newLogin);
        query_edit_user.exec(query_string);
        log_string.append(QString("Admin insert temp password for %1 user query:\n %2 %3. %4.")
                          .arg(newLogin)
                          .arg(query_edit_user.lastError().nativeErrorCode())
                          .arg(query_edit_user.lastError().driverText())
                          .arg(query_edit_user.lastError().databaseText())
                          );
    }
}

void SqlConnector::admin_delete_user(QString login, QString &log_string)
{
    QSqlQuery query_deleteUser;
    QString query_string = QString(SQL_DELETE_USER).arg(login);
    query_deleteUser.exec(query_string);
    log_string.append(QString("Admin delete %1 user query:\n %2 %3. %4.")
                      .arg(login)
                      .arg(query_deleteUser.lastError().nativeErrorCode())
                      .arg(query_deleteUser.lastError().driverText())
                      .arg(query_deleteUser.lastError().databaseText())
                      );
}

bool SqlConnector::create_task(QByteArray session, QString caption, QString content, QString assignee, QDateTime deadline, QString &log_string)
{
    QSqlQuery query_create_task;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_GET_UID_BY_LOGIN)
                                .arg(assignee);
        query_create_task.exec(query_string);
        log_string.append(QString("Get user id by %1 login query by %2:\n %3 %4. %5.")
                          .arg(assignee)
                          .arg(QString(session.toHex()))
                          .arg(query_create_task.lastError().nativeErrorCode())
                          .arg(query_create_task.lastError().driverText())
                          .arg(query_create_task.lastError().databaseText())
                          );
        qint32 assignee_id;
        if(query_create_task.next())
        {
            assignee_id = query_create_task.value(0).toInt();
            query_string = QString(SQL_INSERT_NEW_TASK)
                                    .arg(user_id)
                                    .arg(assignee_id)
                                    .arg(caption)
                                    .arg(content)
                                    .arg(2)
                                    .arg(deadline.toString("yyyy-MM-dd HH:mm:ss"))
                                    ;
            query_create_task.exec(query_string);
            log_string.append(QString("\nCreate task query by %1:\n %2 %3. %4.")
                              .arg(QString(session.toHex()))
                              .arg(query_create_task.lastError().nativeErrorCode())
                              .arg(query_create_task.lastError().driverText())
                              .arg(query_create_task.lastError().databaseText())
                              );
        }
    }
    return correct_session;
}

bool SqlConnector::edit_task(QByteArray session, quint32 task_id, QString caption, QString content, QString assignee, QDateTime deadline, QString &log_string)
{
    QSqlQuery query_edit_task;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_EDIT_TASK)
                                .arg(assignee)
                                .arg(caption)
                                .arg(content)
                                .arg(deadline.toString("yyyy-MM-dd HH:mm:ss"))
                                .arg(task_id)
                                ;
        query_edit_task.exec(query_string);
        log_string.append(QString("\nEdit task by %1:\n %2 %3. %4.")
                          .arg(QString(session.toHex()))
                          .arg(query_edit_task.lastError().nativeErrorCode())
                          .arg(query_edit_task.lastError().driverText())
                          .arg(query_edit_task.lastError().databaseText())
                          );
    }
    return correct_session;
}

bool SqlConnector::edit_task_status(QByteArray session, quint32 task_id, QString status, QString &log_string)
{
    QSqlQuery query_edit_task_staus;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string = QString(SQL_EDIT_TASK_STATUS)
                                .arg(task_id)
                                .arg(status);
        query_edit_task_staus.exec(query_string);
        log_string.append(QString("\nEdit task status by %1:\n %2 %3. %4.")
                          .arg(QString(session.toHex()))
                          .arg(query_edit_task_staus.lastError().nativeErrorCode())
                          .arg(query_edit_task_staus.lastError().driverText())
                          .arg(query_edit_task_staus.lastError().databaseText())
                          );
    }
    return correct_session;
}

bool SqlConnector::get_tasks(QByteArray session, QVector<Task> &data, Role role, QString &log_string)
{
    QSqlQuery query_task_creator;
    qint32 user_id = check_session(session, log_string);
    bool correct_session = (user_id != -1);
    if (correct_session)
    {
        QString query_string;
        switch (role)
        {
        case CREATOR_ROLE:
            query_string = QString(SQL_TASKS_CREATOR).arg(user_id);
            break;
        case ASSIGNEE_ROLE:
            query_string = QString(SQL_TASKS_ASSIGNEE).arg(user_id);
            break;
        }


        query_task_creator.exec(query_string);
        log_string.append(QString("Get tasks query by %1:\n %2 %3. %4.")
                          .arg(QString(session.toHex()))
                          .arg(query_task_creator.lastError().nativeErrorCode())
                          .arg(query_task_creator.lastError().driverText())
                          .arg(query_task_creator.lastError().databaseText())
                          );
        while(query_task_creator.next())
        {
            Task task;
            task.task_id = query_task_creator.value(0).toInt();
            task.caption = query_task_creator.value(1).toString();
            task.content = query_task_creator.value(2).toString();
            task.modified = query_task_creator.value(3).toDateTime();
            task.deadline = query_task_creator.value(4).toDateTime();
            task.another_user_login = query_task_creator.value(5).toString();
            task.status = query_task_creator.value(6).toString();
            data.push_back(task);
        }
    }
    return correct_session;
}

QVector<QVariant> SqlConnector::admin_get_user_by_login(QString login, QString &log_string)
{
    QSqlQuery query_get_user_by_login;
    QString query_string = QString(SQL_GET_USER_BY_LOGIN).arg(login);
    query_get_user_by_login.exec(query_string);
    log_string.append(QString("Admin get user %1 query:\n %2 %3. %4.")
                      .arg(login)
                      .arg(query_get_user_by_login.lastError().nativeErrorCode())
                      .arg(query_get_user_by_login.lastError().driverText())
                      .arg(query_get_user_by_login.lastError().databaseText())
                      );
    QVector<QVariant> user;
    while(query_get_user_by_login.next())
    {
        user.push_back(query_get_user_by_login.value(0));
        user.push_back(query_get_user_by_login.value(1));
        user.push_back(query_get_user_by_login.value(2));
        user.push_back(query_get_user_by_login.value(3));
        user.push_back(query_get_user_by_login.value(4));
        user.push_back(query_get_user_by_login.value(5));
    }
    return user;
}

QByteArray SqlConnector::_generate_session_id(quint32 clientIP)
{
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream << clientIP << QDateTime::currentMSecsSinceEpoch() << qrand();
    return QCryptographicHash::hash(data, QCryptographicHash::Sha3_256);
}

int SqlConnector::check_session(QByteArray session, QString &log_string)
{
    QSqlQuery query_check_session;
    QString query_string = QString(SQL_CHECK_SESSION)
                            .arg(QString(session.toHex()));
    query_check_session.exec(query_string);
    log_string.append(QString("Check session %1 query:\n %2 %3. %4.")
                      .arg(QString(session.toHex()))
                      .arg(query_check_session.lastError().nativeErrorCode())
                      .arg(query_check_session.lastError().driverText())
                      .arg(query_check_session.lastError().databaseText())
                      );
    // Returns -1 if wrong session.
    if (query_check_session.lastError().isValid())
        return -1;
    if (query_check_session.next())
        return query_check_session.value(0).toInt();
    else return -1;
}
