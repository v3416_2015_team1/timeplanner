#ifndef EDITUSERDIALOG_H
#define EDITUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>
#include <QCheckBox>
#include <QVariant>
#include <QAction>
#include <QClipboard>
#include <QApplication>

class EditUserDialog : public QDialog
{
public:
    EditUserDialog(QVector<QVariant> &initData);
    ~EditUserDialog();
    inline QString getLogin(){return txtLogin->text();}
    inline QString getResetedPassword(){return lblResetedPassword->text();}
    inline QString getFirstName(){return txtFirstName->text();}
    inline QString getSecondame(){return txtSecondName->text();}
    inline QString getLastName(){return txtLastName->text();}
    inline QString getPosition(){return txtPosition->text();}
    inline bool getCreateTaskRight(){return chkbxCreateTaskRight->isChecked();}
signals:

public slots:
private slots:
    void onBtnSaveClicked();
    void onBtnCancelClicked();
    void onBtnResetPasswordClicked();
    void copyResetedPassword();
private:
    QLabel *lblLogin;
    QLabel *lblResetedPasswordLbl;
    QLabel *lblResetedPassword;
    QLabel *lblFirstName;
    QLabel *lblSecondName;
    QLabel *lblLastName;
    QLabel *lblPosition;
    QLineEdit *txtLogin;
    QPushButton *btnResetPassword;
    QLineEdit *txtFirstName;
    QLineEdit *txtSecondName;
    QLineEdit *txtLastName;
    QLineEdit *txtPosition;
    QCheckBox *chkbxCreateTaskRight;
    QPushButton *btnSave;
    QPushButton *btnCancel;
    QGridLayout *layout;
    QAction *actCopyResetedPassword;
};

#endif // EDITUSERDIALOG_H
