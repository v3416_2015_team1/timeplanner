#include "mainwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    taskType = ASSIGNEE;

    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle("Time Planner");
    actionShowHide = new QAction("Show/Hide Time Planner", this);
    connect(actionShowHide, &QAction::triggered,
            this, &Widget::showHide);
    actionQuit = new QAction("Quit", this);
    connect(actionQuit,&QAction::triggered,
            this, &QApplication::quit);

    menu = new QMenu(this);
    menu->addAction(actionShowHide);
    menu->addAction(actionQuit);
    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(menu);
    systemTrayIcon->setToolTip("Time Planner");
    systemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &Widget::onTrayIconActivated);

    resize(640, 320);
    //setMaximumSize(640,320);
    adjustSize();



    vl = new QVBoxLayout(this);
    QGroupBox * edit = new QGroupBox(this);
    editLayout = new QHBoxLayout;
    userName = new QLabel;
    editLayout->addWidget(userName);   // Получить из базы данных при входе
    //editLayout->insertSpacing(1,200);
    editItem = new QSpacerItem(1,0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    editLayout->addSpacerItem(editItem);


    edit->setLayout(editLayout);
    vl->addWidget(edit);

    QGroupBox * task = new QGroupBox;
    taskNamesL = new QHBoxLayout;


    curTask = new QLabel("Assigned tasks");
    taskNamesL->addWidget(curTask);

    task->setLayout(taskNamesL);
    taskWidget = new QWidget;
    taskGrid = new QGridLayout;
    taskWidget->setLayout(taskGrid);

    vl->addWidget(task);



    QScrollArea * scrollArea = new QScrollArea;
    vl->addWidget(scrollArea);
    scrollArea->setWidget(taskWidget);
    scrollArea->setWidgetResizable(true);
  //  scrollArea->widget()->resize(640,200);
    //scrollArea->widget()->adjustSize();
    scrollArea->setGeometry(0,320,640,200);
    scrollArea->adjustSize();
    scrollArea->show();


    /*---в ответе сервера!!------*/
#if 0
   addTaskLine(0,"Task 1", "descr1", QDateTime::currentDateTime(),QDateTime::currentDateTime(),"Login","In process");
   addTaskLine(1,"Task ", "descr1", QDateTime::currentDateTime(),QDateTime::currentDateTime(),"Login","In process");
#endif

  /*-----------------*/

    taskGB = new QGroupBox(this);
    taskBL = new QHBoxLayout;
    myTaskB = new QRadioButton("Created tasks");
    assigneeTaskB = new QRadioButton("Assigneed tasks");
    showTaskB = new QPushButton("Show");
    connect(showTaskB, &QPushButton::clicked, this, &Widget::onshowTaskBPressed);
    QSpacerItem * taskBItem = new QSpacerItem(350,0, QSizePolicy::Fixed, QSizePolicy::Minimum);
    taskBL->addSpacerItem(taskBItem);
    taskBL->addWidget(showTaskB);
    taskBL->addWidget(myTaskB);
    taskBL->addWidget(assigneeTaskB);

    taskGB->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Minimum);
    taskGB->setLayout(taskBL);

    vl->addWidget(taskGB);
    this->setLayout(vl);
    //newTask->resize(50, 10);

}

void Widget::setScrollBar( )
{
    int factor = tdHeight;
    vScrollBar->setValue(int(factor * vScrollBar->value()
                              + ((factor - 1) * vScrollBar->pageStep()/2)));

}
void Widget::logOut()
{
    taskType = ASSIGNEE;
    tasks.clear();
    emit logOutSignal();
}

void Widget::setUserName(QString username)
{
    userName->setText(username);
}

void Widget::onshowTaskBPressed()
{
    if (myTaskB->isChecked())
    {
        if (taskType != CREATOR)
        {
            taskType = CREATOR;
            curTask->setText("Created tasks");
            tasks.clear();//

            emit askCreatedTasks();
        }

    }
    else if (assigneeTaskB->isChecked())
    {
        if (taskType != ASSIGNEE)
        {
            taskType = ASSIGNEE;
            curTask->setText("Assigned tasks");
            tasks.clear();///
            emit askAssigneeTasks();
        }

    }
}

void Widget::addTaskLine(int taskId, QString caption, QString content, QDateTime modified, QDateTime deadline, QString assigneeLogin, QString status)
{

     TaskLine * line = new TaskLine;
     line->num = taskId;

     line->taskLayout = new QGridLayout;


     line->task = new QLineEdit;
     line->task->setReadOnly(true);
     line->task->setText(caption);
     line->taskLayout->addWidget(line->task, 0,0);
     int y_id = 0;
     if (taskType == CREATOR)
     {
         line->assignee = new QLineEdit;

         line->assignee->setReadOnly(true);
         line->assignee->setText(assigneeLogin);
         line->taskLayout->addWidget(line->assignee, 0,1);
         y_id = 1;
     }

     line->watchTask = new QPushButton();
     line->watchTask->setIcon(QIcon(":/Icons/read.ico"));
     line->watchTask->setToolTip("Read Task");
     line->taskLayout->addWidget(line->watchTask, 0,y_id + 1);

     connect(line->watchTask, &QPushButton::pressed, line, &TaskLine::showHideDescription);

     line->dateEdit = new QDateEdit(deadline.date());
     line->dateEdit->setReadOnly(true);
     line->taskLayout->addWidget(line->dateEdit, 0, y_id + 2);

     line->editTask = new QPushButton();
     line->editTask->setIcon(QIcon(":/Icons/edit.ico"));
     line->editTask->setToolTip("Edit Task");
     line->editTask->setDisabled(true);
     line->taskLayout->addWidget(line->editTask, 0, y_id + 3);

     line->taskDescription = new QTextEdit(content);
     line->taskDescription->setReadOnly(true);
     line->taskLayout->addWidget(line->taskDescription, 1, 0);
     line->taskDescription->setVisible(false);


     if (taskType == CREATOR)
     {
          line->editTask->setDisabled(false);
          line->taskDescription->append(modified.toString());
          connect(line->editTask, &QPushButton::clicked, line, &TaskLine::askEditTask);
          connect(line->taskDescription, SIGNAL(textChanged()), line, SLOT(onTextChanged()));
     }


     line->changeStatus = new QPushButton("Change status");
     line->changeStatus->setToolTip("Change task status");
     line->changeStatus ->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
     line->statusBox = new QComboBox;

     line->statusBox->addItem(status);
     line->statusBox->addItem(tr("Done"));
     line->statusBox->addItem(tr("In process"));

     line->statusBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

     connect(line->changeStatus, &QPushButton::clicked,line, &TaskLine::onChangeStatusPress);


     line->statusLayout = new QHBoxLayout;
     line->statusLayout->addWidget(line->changeStatus);
     line->statusLayout->addWidget(line->statusBox);

     QSpacerItem *statusItem = new QSpacerItem(150,1, QSizePolicy::Fixed, QSizePolicy::Minimum);
     line->statusLayout->addSpacerItem(statusItem);

     line->taskLayout->addLayout(line->statusLayout,3,0);
     line->changeStatus->setVisible(false);
     line->statusBox->setVisible(false);


     tasks.push_back(line);
     taskGrid->addLayout(line->taskLayout,taskId,0);
}

void Widget::getNewNotification(QString caption, QString content)
{
   systemTrayIcon->showMessage(caption,content);
}

void Widget::closeEvent(QCloseEvent *event)
{
    if (systemTrayIcon->isVisible())
    {
        hide();
    }
    event->ignore();
}

TaskLine::TaskLine(QWidget *parent)
    : QWidget(parent)
{
}
TaskLine::~TaskLine()
{
}

void TaskLine::showHideDescription()
{
   if(!taskDescription->isVisible())
   {
       taskDescription->setVisible(true);
       changeStatus->setVisible(true);
       statusBox->setVisible(true);
       watchTask->setIcon(QIcon(":/Icons/up.ico"));
   }
   else
   {
       taskDescription->setVisible(false);
       changeStatus->setVisible(false);
       statusBox->setVisible(false);
       watchTask->setIcon(QIcon(":/Icons/read.ico"));
       if ( !taskDescription->isReadOnly())
       {
           taskDescription->setReadOnly(true);
       }
       if ( !task->isReadOnly())
       {
           task->setReadOnly(true);
       }

   }

}

void TaskLine::onChangeStatusPress()
{
    taskStatus = this->statusBox->currentText();
    emit changeStatusSignal(this->num);
    //TODO сигнал в базу
}

void TaskLine::onTextChanged()
{
    emit taskDescrChanged(this->num);
}


void TaskLine::askEditTask()
{
   showHideDescription();
   if ( taskDescription->isReadOnly())
   {
        taskDescription->setReadOnly(false);
    }
    if ( task->isReadOnly())
    {
        task->setReadOnly(false);
    }

}

void Widget::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}

void Widget::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
    {
        showHide();
    }
}

void Widget::onCheckTask()
{
   // todo заполнить комбо бокс
}


Widget::~Widget()
{

}
