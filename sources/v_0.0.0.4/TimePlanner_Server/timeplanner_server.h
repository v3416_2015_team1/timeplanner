#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QInputDialog>
#include "sqlconnector.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QDateTime>
#include <QTextEdit>
#include <QVBoxLayout>


#define INITIAL_PORT            3333
//#define SERVER_SEND_PART_NOTIFY 1

#define MSG_ERROR       0
#define MSG_MESSAGE     10

#define MSG_SIGN_IN     1
#define MSG_SIGN_OUT    11

#define MSG_TASK_CREATE             2
#define MSG_TASK_USER_CREATOR       12
#define MSG_TASK_USER_ASSIGNEE      22
#define MSG_TASK_EDIT               32 //
#define MSG_TASK_DELETE             42 //

#define MSG_REGISTRATION    3//
#define MSG_USERS           13//

#define ERROR_SIGN_IN_FAILURE 4001
#define ERROR_SESSION_FAILURE 4002

class TimePlannerServer : public QMainWindow
{
    Q_OBJECT
    SqlConnector *sqlConnector;
    QTcpServer *tcpServer;
    QTextEdit *txtLog;
    void _sendToClient(QTcpSocket *socket,
                      quint8 reason,
                      QVector<QVariant> data);
public:
    TimePlannerServer(QWidget *parent = 0);
    ~TimePlannerServer();

public slots:
    void newConncetion();
    void receiveClientQuery();
    void log(QString msg);
    bool connectToMySQL(QString databaseName, int MySQL_port, QString &log_string);
    bool startTCPServer();
};

#endif // MAINWINDOW_H
