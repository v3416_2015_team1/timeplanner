#include "adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Add new user"));
    _pLblLogin = new QLabel(tr("Login"));
    _pLblTempPasswordLbl = new QLabel(tr("Temporary password"));
    _pLblTempPassword = new QLabel();
    _generateTempPassword();
    QFont tempPasswordFont = _pLblTempPassword->font();
    tempPasswordFont.setBold(true);
    tempPasswordFont.setPointSize(14);
    _pLblTempPassword->setFont(tempPasswordFont);
    _pBtnCopy = new QPushButton(tr("Copy"));
    _pLblFirstName = new QLabel(tr("First name"));
    _pLblSecondName = new QLabel(tr("Second name"));
    _pLblLastName = new QLabel(tr("Last name"));
    _pLblPosition = new QLabel(tr("Position"));
    _pLtxtLogin = new QLineEdit;
    _pLtxtLogin->setPlaceholderText(tr("Login"));
    _pLtxtFirstName = new QLineEdit;
    _pLtxtFirstName->setPlaceholderText(tr("First name"));
    _pLtxtSecondName = new QLineEdit;
    _pLtxtSecondName->setPlaceholderText(tr("Second name"));
    _pLtxtLastName = new QLineEdit;
    _pLtxtLastName->setPlaceholderText(tr("Last name"));
    _pLtxtPosition = new QLineEdit;
    _pLtxtPosition->setPlaceholderText(tr("Position"));
    _pChkbxCreateTaskRight = new QCheckBox("This user is allowed to create task");
    _pBtnSave = new QPushButton(tr("Save user"));
    _pBtnCancel = new QPushButton(tr("Cancel"));
    connect(_pBtnCopy, &QPushButton::clicked,
            this, &AddUserDialog::_copyTempPassword);
    connect(_pBtnSave, &QPushButton::clicked,
            this, &AddUserDialog::_onBtnSaveClicked);
    connect(_pBtnCancel, &QPushButton::clicked,
            this, &AddUserDialog::_onBtnCancelClicked);
    quint8 layoutRow = 0;
    _pGltMain = new QGridLayout(this);
    _pGltMain->addWidget(_pLblLogin,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtLogin,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblTempPasswordLbl,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLblTempPassword,
                         layoutRow,
                         1);
    _pGltMain->addWidget(_pBtnCopy,
                         layoutRow++,
                         2);
    _pGltMain->addWidget(_pLblFirstName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtFirstName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblSecondName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtSecondName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblLastName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtLastName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblPosition,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtPosition,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pChkbxCreateTaskRight,
                         layoutRow++,
                         0, 1, 3);
    _pGltMain->addWidget(_pBtnSave,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pBtnCancel,
                         layoutRow++,
                         1, 1, 2);
    _pBtnSave->setDefault(true);
}

void AddUserDialog::_onBtnSaveClicked()
{
    if (_pLtxtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                             tr("Login"),
                             tr("No login entered."),
                             QMessageBox::Ok
                            );
        return;
    }
    this->accept();
}

void AddUserDialog::_onBtnCancelClicked()
{
    this->reject();
}

void AddUserDialog::_generateTempPassword()
{
    QString newPassword("");
    _pLblTempPassword->setText(newPassword);
    for (quint8 i = 0; i < 8; ++i)
    {
        quint8 symbolCategory = qrand() % 3;
        switch (symbolCategory)
        {
        case 0:
            newPassword.push_back(QChar('0' + qrand() % 10));
            break;
        case 1:
            newPassword.push_back(QChar('A' + qrand() % 26));
            break;
        case 2:
            newPassword.push_back(QChar('a' + qrand() % 26));
            break;
        }
    }
    _pLblTempPassword->setText(newPassword);
}

void AddUserDialog::_copyTempPassword()
{
    QApplication::clipboard()->setText(_pLblTempPassword->text());
}

AddUserDialog::~AddUserDialog()
{
}
