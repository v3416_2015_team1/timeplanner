#include "sqlconnector.h"

SqlConnector::SqlConnector(QWidget *parent)
    : QWidget(parent)
{
}

bool SqlConnector::connectDB(QString strMysqlLogin,
                             QString strMysqlPassword,
                             QString strDatabaseName,
                             int mysqlPort,
                             QString &strLog)
{
    _sqlDB = QSqlDatabase::addDatabase("QMYSQL");
    _sqlDB.setHostName("localhost");
    _sqlDB.setPort(mysqlPort);
    _sqlDB.setUserName(strMysqlLogin);
    _sqlDB.setPassword(strMysqlPassword);
    _sqlDB.open();
    strLog.append(QString("Establishing database connection:\nDriver name: %1\nMySQL Host: %2\nMySQL Port: %3\nMySQL User: %4\nMySQL Database: %5.\n")
                     .arg(_sqlDB.driverName())
                     .arg(_sqlDB.hostName())
                     .arg(_sqlDB.port())
                     .arg(_sqlDB.userName())
                     .arg(strDatabaseName)
                     );
    QSqlQuery sqlQueryCheckConnection;
    sqlQueryCheckConnection.exec(SQL_USE_UNEXISTING_DATABASE);
    strLog.append(QString("Trying to find running MySQL Server\n(1049 error — server found,\n 2006 error — server not found):\n %1 %2 %3.")
                     .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                     .arg(sqlQueryCheckConnection.lastError().driverText())
                     .arg(sqlQueryCheckConnection.lastError().databaseText())
                     );
    if (sqlQueryCheckConnection.lastError().nativeErrorCode() == SQL_ERROR_NO_CONNECTION)
    {
        return false;
    }
    sqlQueryCheckConnection.exec(QString(SQL_USE_DATABASE).arg(strDatabaseName));
    strLog.append(QString("\nTrying to find %1 database:\n %2 %3. %4.")
                     .arg(strDatabaseName)
                     .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                     .arg(sqlQueryCheckConnection.lastError().driverText())
                     .arg(sqlQueryCheckConnection.lastError().databaseText())
                     );
    if (sqlQueryCheckConnection.lastError().nativeErrorCode() == SQL_ERROR_UNKNOWN_DATABASE)
    {
        sqlQueryCheckConnection.exec(QString(SQL_CREATE_DATABASE).arg(strDatabaseName));
        strLog.append(QString("\nCreating new %1 database:\n %2 %3. %4.\n")
                         .arg(strDatabaseName)
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(QString(SQL_USE_DATABASE).arg(strDatabaseName));
        strLog.append(QString("Use %1 database:\n %2 %3. %4.\n")
                         .arg(strDatabaseName)
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(SQL_CREATE_TABLE_USERS);
        strLog.append(QString("Creating `users` table:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(SQL_CREATE_TABLE_SESSIONS);
        strLog.append(QString("Creating `sessions` table:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(SQL_CREATE_TABLE_TASKS);
        strLog.append(QString("Creating `tasks` table:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(SQL_CREATE_TABLE_TASK_STATUS);
        strLog.append(QString("Creating `task_status` table:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("Done"));
        strLog.append(QString("Fill `task_status` table with \'Done\' value:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(QString(SQL_FILL_TABLE_TASK_STATUS)
                                    .arg("In progress"));
        strLog.append(QString("Fill `task_status` table with \'In progress\' value:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
        sqlQueryCheckConnection.exec(SQL_CREATE_TABLE_TEMP_PASSWORDS);
        strLog.append(QString("Creating `temp_passwords` table:\n %1 %2. %3.\n")
                         .arg(sqlQueryCheckConnection.lastError().nativeErrorCode())
                         .arg(sqlQueryCheckConnection.lastError().driverText())
                         .arg(sqlQueryCheckConnection.lastError().databaseText())
                         );
    }
    return true;
}

void SqlConnector::disconnectDB()
{
    _sqlDB.close();
}

QVector<QVariant> SqlConnector::signIn(QString strLogin,
                                       QString strPassword,
                                       quint32 clientIP,
                                       QString &strLog
                                       )
{
    // Get hash of the password
    QByteArray btarrPassword(strPassword.toStdString().c_str(), strPassword.length());
    QByteArray btarrHashPassword = QCryptographicHash::hash(btarrPassword, QCryptographicHash::Sha3_256);
    // Perform query
    QSqlQuery sqlQuerySignIn;

    QString strQuery =
            QString(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)
            .arg(strLogin)
            .arg(QString(btarrHashPassword.toHex()));
    sqlQuerySignIn.exec(strQuery);
    strLog.append(QString("Sign in %1 query:\n %2 %3. %4.")
                     .arg(strLogin)
                     .arg(sqlQuerySignIn.lastError().nativeErrorCode())
                     .arg(sqlQuerySignIn.lastError().driverText())
                     .arg(sqlQuerySignIn.lastError().databaseText())
                     );
    // Read query answer
    QVector<QVariant> data;
    quint32 userId;
    if (sqlQuerySignIn.next())
    {
        // Generate new session and insert it into sessions database table
        QByteArray session = _generateSessionId(clientIP);
        userId = sqlQuerySignIn.value(0).toInt();
        data.push_back(sqlQuerySignIn.value(3)); // First name
        data.push_back(sqlQuerySignIn.value(4)); // Second name
        data.push_back(sqlQuerySignIn.value(5)); // Last name
        data.push_back(sqlQuerySignIn.value(6)); // Position
        data.push_back(sqlQuerySignIn.value(7)); // Create tasks right
        data.push_back(session); // Session Id
        sqlQuerySignIn.exec(QString(SQL_DELETE_SESSIONS).arg(userId));
        strQuery = QString(SQL_INSERT_NEW_SESSION)
                .arg(QString(session.toHex()))
                .arg(userId)
                .arg(clientIP);
        sqlQuerySignIn.exec(strQuery);
        strLog.append(QString("\nCreate session %1 query:\n %2 %3. %4.")
                         .arg(QString(session.toHex()))
                         .arg(sqlQuerySignIn.lastError().nativeErrorCode())
                         .arg(sqlQuerySignIn.lastError().driverText())
                         .arg(sqlQuerySignIn.lastError().databaseText())
                         );
    }
    // If user not found the data vector will be empty.
    return data;
}

QVector<QVariant> SqlConnector::signInWithNewPassword(QString strLogin,
                                                      QString strPassword,
                                                      quint32 clientIP,
                                                      QString &strLog)
{
    QByteArray btarrPassword(strPassword.toStdString().c_str(),
                             strPassword.length());
    QByteArray btarrHashPassword = QCryptographicHash::hash(btarrPassword,
                                                            QCryptographicHash::Sha3_256);
    QSqlQuery sqlQuerySignInWithNewPassword;
    QString strQuery = QString(SQL_DELETE_TEMP_PASSWORD)
                            .arg(strLogin);
    sqlQuerySignInWithNewPassword.exec(strQuery);
    strQuery = QString(SQL_EDIT_USER_PASSWORD)
                    .arg(QString(btarrHashPassword.toHex()))
                    .arg(strLogin);
    sqlQuerySignInWithNewPassword.exec(strQuery);
    strLog.append(QString("Delete temporary password at %1 user  query:\n %2 %3. %4.")
                     .arg(strLogin)
                     .arg(sqlQuerySignInWithNewPassword.lastError().nativeErrorCode())
                     .arg(sqlQuerySignInWithNewPassword.lastError().driverText())
                     .arg(sqlQuerySignInWithNewPassword.lastError().databaseText())
                     );
    return signIn(strLogin,
                  strPassword,
                  clientIP,
                  strLog);
}

bool SqlConnector::checkTempPassword(QString strLogin,
                                     QString strPassword,
                                     quint32 clientIP,
                                     QString &strLog)
{
    QSqlQuery sqlQueryCheckTempPassword;
    QByteArray btarrPassword(strPassword.toStdString().c_str(),
                             strPassword.length());
    QByteArray btarrHashPassword = QCryptographicHash::hash(btarrPassword,
                                                            QCryptographicHash::Sha3_256);
    QString strQuery = QString(SQL_CHECK_TEMP_PASSWORD)
                            .arg(strLogin)
                            .arg(QString(btarrHashPassword.toHex()));
    sqlQueryCheckTempPassword.exec(strQuery);
    strLog.append(QString("Check temp password %1 query:\n %2 %3. %4.")
                     .arg(strLogin)
                     .arg(sqlQueryCheckTempPassword.lastError().nativeErrorCode())
                     .arg(sqlQueryCheckTempPassword.lastError().driverText())
                     .arg(sqlQueryCheckTempPassword.lastError().databaseText())
                     );
    bool result = false;
    while(sqlQueryCheckTempPassword.next())
    {
        result = true;
    }
    return result;
}

void SqlConnector::signOut(QByteArray btarrSession,
                           QString &strLog)
{
    QSqlQuery sqlQuerySignOut;
    QString strQuery = QString(SQL_DELETE_SESSION)
                        .arg(QString(btarrSession.toHex()));
    sqlQuerySignOut.exec(strQuery);
    strLog.append(QString("Sign out %1 query:\n %2 %3. %4.")
                      .arg(QString(btarrSession.toHex()))
                      .arg(sqlQuerySignOut.lastError().nativeErrorCode())
                      .arg(sqlQuerySignOut.lastError().driverText())
                      .arg(sqlQuerySignOut.lastError().databaseText())
                      );
}

void SqlConnector::forceSignOut(QString strLogin,
                                QString &strLog)
{
    QSqlQuery sqlQueryForceSignOut;
    QString strQuery = QString(SQL_GET_UID_BY_LOGIN)
                            .arg(strLogin);
    sqlQueryForceSignOut.exec(strQuery);
    strLog.append(QString("Admin get user id by %1 login query:\n %3 %4. %5.")
                      .arg(strLogin)
                      .arg(sqlQueryForceSignOut.lastError().nativeErrorCode())
                      .arg(sqlQueryForceSignOut.lastError().driverText())
                      .arg(sqlQueryForceSignOut.lastError().databaseText())
                      );
    qint32 userId;
    if(sqlQueryForceSignOut.next())
    {
        userId = sqlQueryForceSignOut.value(0).toInt();
        strQuery = QString(SQL_FORCE_DELETE_SESSION)
                                .arg(userId)
                                ;
        sqlQueryForceSignOut.exec(strQuery);
        strLog.append(QString("Force sign out %1 query:\n %2 %3. %4.")
                          .arg(strLogin)
                          .arg(sqlQueryForceSignOut.lastError().nativeErrorCode())
                          .arg(sqlQueryForceSignOut.lastError().driverText())
                          .arg(sqlQueryForceSignOut.lastError().databaseText())
                          );
    }
}

bool SqlConnector::getUsers(QByteArray btarrSession,
                            QVector<QVariant> &vecData,
                            QString &strLogString)
{
    QSqlQuery sqlQueryGetUsers;
    qint32 userId = checkSession(btarrSession,
                                 strLogString);
    bool correctSession = (userId != -1);
    if (correctSession)
    {
        QString strQuery = QString(SQL_GET_USERS);
        sqlQueryGetUsers.exec(strQuery);
        strLogString.append(QString("Get users query by %1:\n %2 %3. %4.")
                          .arg(QString(btarrSession.toHex()))
                          .arg(sqlQueryGetUsers.lastError().nativeErrorCode())
                          .arg(sqlQueryGetUsers.lastError().driverText())
                          .arg(sqlQueryGetUsers.lastError().databaseText())
                          );
        while(sqlQueryGetUsers.next())
        {
            vecData.push_back( sqlQueryGetUsers.value(0).toString());
        }
    }
    return correctSession;
}

QByteArray SqlConnector::getSessionByLogin(QString strLogin,
                                           QString &strLog)
{
    QByteArray btarrSession;
    QSqlQuery sqlQueryGetSessionByLogin;
    QString strQuery = QString(SQL_GET_UID_BY_LOGIN)
                            .arg(strLogin);
    sqlQueryGetSessionByLogin.exec(strQuery);
    strLog.append(QString("Get user id by %1 login query:\n %3 %4. %5.")
                      .arg(strLogin)
                      .arg(sqlQueryGetSessionByLogin.lastError().nativeErrorCode())
                      .arg(sqlQueryGetSessionByLogin.lastError().driverText())
                      .arg(sqlQueryGetSessionByLogin.lastError().databaseText())
                      );
    qint32 userId;
    if(sqlQueryGetSessionByLogin.next())
    {
        userId = sqlQueryGetSessionByLogin.value(0).toInt();
        strQuery = QString(SQL_GET_SESSION_BY_UID)
                                .arg(userId)
                                ;
        sqlQueryGetSessionByLogin.exec(strQuery);
        strLog.append(QString("Get session by %1 user id query:\n %2 %3. %4.")
                          .arg(userId)
                          .arg(sqlQueryGetSessionByLogin.lastError().nativeErrorCode())
                          .arg(sqlQueryGetSessionByLogin.lastError().driverText())
                          .arg(sqlQueryGetSessionByLogin.lastError().databaseText())
                          );
        if (sqlQueryGetSessionByLogin.next())
            btarrSession = sqlQueryGetSessionByLogin.value(0).toByteArray();
    }
    return btarrSession;
}

void SqlConnector::adminInsertUser(QString strLogin,
                                   QString strPassword,
                                   QString strFirstName,
                                   QString strSecondName,
                                   QString strLastName,
                                   QString strPosition,
                                   bool createTaskRight,
                                   QString &strLog)
{
    QByteArray btarrHash = QCryptographicHash::hash(QByteArray(strPassword.toStdString().c_str(), strPassword.length()), QCryptographicHash::Sha3_256).toHex();
    QSqlQuery sqlQueryInsertUser;
    QString strQuery = QString(SQL_INSERT_USER)
            .arg(strLogin)
            .arg(QString(btarrHash))
            .arg(strFirstName)
            .arg(strSecondName)
            .arg(strLastName)
            .arg(strPosition)
            .arg(createTaskRight)
            ;
    sqlQueryInsertUser.exec(strQuery);
    strLog.append(QString("Admin insert new %1 user query:\n %2 %3. %4.")
                      .arg(strLogin)
                      .arg(sqlQueryInsertUser.lastError().nativeErrorCode())
                      .arg(sqlQueryInsertUser.lastError().driverText())
                      .arg(sqlQueryInsertUser.lastError().databaseText())
                      );
    strQuery = QString(SQL_INSERT_TEMP_PASSWORD)
                   .arg(QString(btarrHash))
                   .arg(strLogin);
    sqlQueryInsertUser.exec(strQuery);
    strLog.append(QString("Admin insert temp password for %1 user query:\n %2 %3. %4.")
                      .arg(strLogin)
                      .arg(sqlQueryInsertUser.lastError().nativeErrorCode())
                      .arg(sqlQueryInsertUser.lastError().driverText())
                      .arg(sqlQueryInsertUser.lastError().databaseText())
                      );
}

void SqlConnector::adminEditUser(QString strOldLogin,
                                 QString strNewLogin,
                                 QString strNewPassword,
                                 QString strFirstName,
                                 QString strSecondName,
                                 QString strLastName,
                                 QString strPosition,
                                 bool createTaskRight,
                                 QString &strLog)
{
    QSqlQuery sqlQueryEditUser;
    QString strQuery = QString(SQL_EDIT_USER)
            .arg(strNewLogin)
            .arg(strFirstName)
            .arg(strSecondName)
            .arg(strLastName)
            .arg(strPosition)
            .arg(createTaskRight)
            .arg(strOldLogin)
            ;
    bool successfullEditUser = sqlQueryEditUser.exec(strQuery);
    strLog.append(QString("Admin edit %1->%2 user query:\n %3 %4. %5.")
                      .arg(strOldLogin)
                      .arg(strNewLogin)
                      .arg(sqlQueryEditUser.lastError().nativeErrorCode())
                      .arg(sqlQueryEditUser.lastError().driverText())
                      .arg(sqlQueryEditUser.lastError().databaseText())
                      );
    if (successfullEditUser && !strNewPassword.isEmpty())
    {
        QByteArray btarrHash = QCryptographicHash::hash(QByteArray(strNewPassword.toStdString().c_str(),
                                                                   strNewPassword.length()),
                                                        QCryptographicHash::Sha3_256);
        strQuery = QString(SQL_EDIT_USER_PASSWORD)
                    .arg(QString(btarrHash.toHex()))
                    .arg(strNewLogin)
                    ;
        sqlQueryEditUser.exec(strQuery);
        strLog.append(QString("Admin edit %1 user password query:\n %3 %4. %5.")
                          .arg(strNewLogin)
                          .arg(sqlQueryEditUser.lastError().nativeErrorCode())
                          .arg(sqlQueryEditUser.lastError().driverText())
                          .arg(sqlQueryEditUser.lastError().databaseText())
                          );
        strQuery = QString(SQL_DELETE_TEMP_PASSWORD)
                       .arg(strOldLogin);
        sqlQueryEditUser.exec(strQuery);
        strQuery = QString(SQL_DELETE_TEMP_PASSWORD)
                       .arg(strNewLogin);
        sqlQueryEditUser.exec(strQuery);
        strQuery = QString(SQL_INSERT_TEMP_PASSWORD)
                       .arg(QString(btarrHash.toHex()))
                       .arg(strNewLogin);
        sqlQueryEditUser.exec(strQuery);
        strLog.append(QString("Admin insert temp password for %1 user query:\n %2 %3. %4.")
                          .arg(strNewLogin)
                          .arg(sqlQueryEditUser.lastError().nativeErrorCode())
                          .arg(sqlQueryEditUser.lastError().driverText())
                          .arg(sqlQueryEditUser.lastError().databaseText())
                          );
    }
}

void SqlConnector::adminDeleteUser(QString strLogin,
                                   QString &strLog)
{
    QSqlQuery sqlQueryDeleteUser;
    QString strQuery = QString(SQL_DELETE_USER).arg(strLogin);
    sqlQueryDeleteUser.exec(strQuery);
    strLog.append(QString("Admin delete %1 user query:\n %2 %3. %4.")
                      .arg(strLogin)
                      .arg(sqlQueryDeleteUser.lastError().nativeErrorCode())
                      .arg(sqlQueryDeleteUser.lastError().driverText())
                      .arg(sqlQueryDeleteUser.lastError().databaseText())
                      );
}

bool SqlConnector::createTask(QByteArray btarrSession,
                              QString strCaption,
                              QString strContent,
                              QString strAssignee,
                              QDateTime dateDeadline,
                              QString &strLog)
{
    QSqlQuery sqlQueryCreateTask;
    qint32 userId = checkSession(btarrSession,
                                 strLog);
    bool correctSession = (userId != -1);
    if (correctSession)
    {
        QString strQuery = QString(SQL_GET_UID_BY_LOGIN)
                                .arg(strAssignee);
        sqlQueryCreateTask.exec(strQuery);
        strLog.append(QString("Get user id by %1 login query by %2:\n %3 %4. %5.")
                          .arg(strAssignee)
                          .arg(QString(btarrSession.toHex()))
                          .arg(sqlQueryCreateTask.lastError().nativeErrorCode())
                          .arg(sqlQueryCreateTask.lastError().driverText())
                          .arg(sqlQueryCreateTask.lastError().databaseText())
                          );
        qint32 assigneeId;
        if(sqlQueryCreateTask.next())
        {
            assigneeId = sqlQueryCreateTask.value(0).toInt();
            strQuery = QString(SQL_INSERT_NEW_TASK)
                                    .arg(userId)
                                    .arg(assigneeId)
                                    .arg(strCaption)
                                    .arg(strContent)
                                    .arg(2)
                                    .arg(dateDeadline.toString("yyyy-MM-dd HH:mm:ss"))
                                    ;
            sqlQueryCreateTask.exec(strQuery);
            strLog.append(QString("\nCreate task query by %1:\n %2 %3. %4.")
                              .arg(QString(btarrSession.toHex()))
                              .arg(sqlQueryCreateTask.lastError().nativeErrorCode())
                              .arg(sqlQueryCreateTask.lastError().driverText())
                              .arg(sqlQueryCreateTask.lastError().databaseText())
                              );
        }
    }
    return correctSession;
}

bool SqlConnector::editTask(QByteArray btarrSession,
                            quint32 taskId,
                            QString strCaption,
                            QString strContent,
                            QString strAssignee,
                            QDateTime dateDeadline,
                            QString &strLog)
{
    QSqlQuery sqlQueryEditTask;
    qint32 userId = checkSession(btarrSession,
                                 strLog);
    bool correctSession = (userId != -1);
    if (correctSession)
    {
        QString strQuery = QString(SQL_EDIT_TASK)
                                .arg(strAssignee)
                                .arg(strCaption)
                                .arg(strContent)
                                .arg(dateDeadline.toString("yyyy-MM-dd HH:mm:ss"))
                                .arg(taskId)
                                ;
        sqlQueryEditTask.exec(strQuery);
        strLog.append(QString("\nEdit task by %1:\n %2 %3. %4.")
                          .arg(QString(btarrSession.toHex()))
                          .arg(sqlQueryEditTask.lastError().nativeErrorCode())
                          .arg(sqlQueryEditTask.lastError().driverText())
                          .arg(sqlQueryEditTask.lastError().databaseText())
                          );
    }
    return correctSession;
}

bool SqlConnector::editTaskStatus(QByteArray btarrSession,
                                  quint32 taskId,
                                  QString strStatus,
                                  QString &strLog)
{
    QSqlQuery sqlQueryEditTaskStaus;
    qint32 userId = checkSession(btarrSession,
                                 strLog);
    bool correctSession = (userId != -1);
    if (correctSession)
    {
        QString queryString = QString(SQL_EDIT_TASK_STATUS)
                                .arg(taskId)
                                .arg(strStatus);
        sqlQueryEditTaskStaus.exec(queryString);
        strLog.append(QString("\nEdit task status by %1:\n %2 %3. %4.")
                          .arg(QString(btarrSession.toHex()))
                          .arg(sqlQueryEditTaskStaus.lastError().nativeErrorCode())
                          .arg(sqlQueryEditTaskStaus.lastError().driverText())
                          .arg(sqlQueryEditTaskStaus.lastError().databaseText())
                          );
    }
    return correctSession;
}

bool SqlConnector::getTasks(QByteArray btarrSession,
                            QVector<Task> &vecData,
                            Role role,
                            QString &strLog)
{
    QSqlQuery sqlQueryTaskCreator;
    qint32 userId = checkSession(btarrSession,
                                 strLog);
    bool correctSession = (userId != -1);
    if (correctSession)
    {
        QString strQuery;
        switch (role)
        {
        case CREATOR_ROLE:
            strQuery = QString(SQL_TASKS_CREATOR).arg(userId);
            break;
        case ASSIGNEE_ROLE:
            strQuery = QString(SQL_TASKS_ASSIGNEE).arg(userId);
            break;
        }
        sqlQueryTaskCreator.exec(strQuery);
        strLog.append(QString("Get tasks query by %1:\n %2 %3. %4.")
                          .arg(QString(btarrSession.toHex()))
                          .arg(sqlQueryTaskCreator.lastError().nativeErrorCode())
                          .arg(sqlQueryTaskCreator.lastError().driverText())
                          .arg(sqlQueryTaskCreator.lastError().databaseText())
                          );
        while(sqlQueryTaskCreator.next())
        {
            Task task;
            task.taskId = sqlQueryTaskCreator.value(0).toInt();
            task.caption = sqlQueryTaskCreator.value(1).toString();
            task.content = sqlQueryTaskCreator.value(2).toString();
            task.modified = sqlQueryTaskCreator.value(3).toDateTime();
            task.deadline = sqlQueryTaskCreator.value(4).toDateTime();
            task.anotherUserLogin = sqlQueryTaskCreator.value(5).toString();
            task.status = sqlQueryTaskCreator.value(6).toString();
            vecData.push_back(task);
        }
    }
    return correctSession;
}

QVector<QVariant> SqlConnector::adminGetUserByLogin(QString strLogin,
                                                    QString &strLog)
{
    QSqlQuery sqlQueryGetUserByLogin;
    QString strQuery = QString(SQL_GET_USER_BY_LOGIN).arg(strLogin);
    sqlQueryGetUserByLogin.exec(strQuery);
    strLog.append(QString("Admin get user %1 query:\n %2 %3. %4.")
                      .arg(strLogin)
                      .arg(sqlQueryGetUserByLogin.lastError().nativeErrorCode())
                      .arg(sqlQueryGetUserByLogin.lastError().driverText())
                      .arg(sqlQueryGetUserByLogin.lastError().databaseText())
                      );
    QVector<QVariant> vecUser;
    while(sqlQueryGetUserByLogin.next())
    {
        vecUser.push_back(sqlQueryGetUserByLogin.value(0));
        vecUser.push_back(sqlQueryGetUserByLogin.value(1));
        vecUser.push_back(sqlQueryGetUserByLogin.value(2));
        vecUser.push_back(sqlQueryGetUserByLogin.value(3));
        vecUser.push_back(sqlQueryGetUserByLogin.value(4));
        vecUser.push_back(sqlQueryGetUserByLogin.value(5));
    }
    return vecUser;
}

QByteArray SqlConnector::_generateSessionId(quint32 clientIP)
{
    QByteArray btarrData;
    QDataStream dataStream(&btarrData, QIODevice::WriteOnly);
    dataStream << clientIP << QDateTime::currentMSecsSinceEpoch() << qrand();
    return QCryptographicHash::hash(btarrData,
                                    QCryptographicHash::Sha3_256);
}

int SqlConnector::checkSession(QByteArray btarrSession,
                               QString &strLog)
{
    QSqlQuery sqlQueryCheckSession;
    QString strQuery = QString(SQL_CHECK_SESSION)
                            .arg(QString(btarrSession.toHex()));
    sqlQueryCheckSession.exec(strQuery);
    strLog.append(QString("Check session %1 query:\n %2 %3. %4.")
                      .arg(QString(btarrSession.toHex()))
                      .arg(sqlQueryCheckSession.lastError().nativeErrorCode())
                      .arg(sqlQueryCheckSession.lastError().driverText())
                      .arg(sqlQueryCheckSession.lastError().databaseText())
                      );
    // Returns -1 if wrong session.
    if (sqlQueryCheckSession.lastError().isValid())
        return -1;
    if (sqlQueryCheckSession.next())
        return sqlQueryCheckSession.value(0).toInt();
    else return -1;
}

SqlConnector::~SqlConnector()
{
    disconnectDB();
}
