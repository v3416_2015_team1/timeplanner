#include "edituserdialog.h"

EditUserDialog::EditUserDialog(QVector<QVariant> &initData)
{
    setWindowTitle(tr("Edit user"));
    quint8 layoutRow = 0;
    _pLblLogin = new QLabel(tr("Login"));
    _pLblResetedPasswordLbl = new QLabel(tr("New password"));
    _pLblResetedPasswordLbl->setVisible(false);
    _pLblResetedPassword = new QLabel;
    _pLblResetedPassword->setVisible(false);
    QFont resetPasswordFont = _pLblResetedPassword->font();
    resetPasswordFont.setBold(true);
    resetPasswordFont.setPointSize(14);
    _pLblResetedPassword->setFont(resetPasswordFont);
    _pBtnCopy = new QPushButton(tr("Copy"));
    _pBtnCopy->setVisible(false);
    _pLblFirstName = new QLabel(tr("First name"));
    _pLblSecondName = new QLabel(tr("Second name"));
    _pLblLastName = new QLabel(tr("Last name"));
    _pLblPosition = new QLabel(tr("Position"));
    _pLtxtLogin = new QLineEdit;
    _pLtxtLogin->setPlaceholderText(tr("Login"));
    _pLtxtLogin->setText(initData[layoutRow++].toString());
    _pBtnResetPassword = new QPushButton(tr("Reset password"));
    connect(_pBtnResetPassword, QPushButton::clicked,
            this, &EditUserDialog::_onBtnResetPasswordClicked);
    _pLtxtFirstName = new QLineEdit;
    _pLtxtFirstName->setPlaceholderText(tr("First name"));
    _pLtxtFirstName->setText(initData[layoutRow++].toString());
    _pLtxtSecondName = new QLineEdit;
    _pLtxtSecondName->setPlaceholderText(tr("Second name"));
    _pLtxtSecondName->setText(initData[layoutRow++].toString());
    _pLtxtLastName = new QLineEdit;
    _pLtxtLastName->setPlaceholderText(tr("Last name"));
    _pLtxtLastName->setText(initData[layoutRow++].toString());
    _pLtxtPosition = new QLineEdit;
    _pLtxtPosition->setPlaceholderText(tr("Position"));
    _pLtxtPosition->setText(initData[layoutRow++].toString());
    _pChkbxCreateTaskRight = new QCheckBox("This user is allowed to create tasks");
    _pChkbxCreateTaskRight->setChecked(initData[layoutRow++].toBool());
    _pBtnSave = new QPushButton(tr("Save user"));
    _pBtnCancel = new QPushButton(tr("Cancel"));
    connect(_pBtnCopy, &QPushButton::clicked,
            this, &EditUserDialog::_copyResetedPassword);
    connect(_pBtnSave, &QPushButton::clicked,
            this, &EditUserDialog::_onBtnSaveClicked);
    connect(_pBtnCancel, &QPushButton::clicked,
            this, &EditUserDialog::_onBtnCancelClicked);
    layoutRow = 0;
    _pGltMain = new QGridLayout(this);
    _pGltMain->addWidget(_pLblLogin,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtLogin,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pBtnResetPassword,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblResetedPasswordLbl,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLblResetedPassword,
                         layoutRow,
                         1);
    _pGltMain->addWidget(_pBtnCopy,
                         layoutRow++,
                         2);
    _pGltMain->addWidget(_pLblFirstName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtFirstName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblSecondName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtSecondName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblLastName,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtLastName,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pLblPosition,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pLtxtPosition,
                         layoutRow++,
                         1, 1, 2);
    _pGltMain->addWidget(_pChkbxCreateTaskRight,
                         layoutRow++,
                         0, 1, 3);
    _pGltMain->addWidget(_pBtnSave,
                         layoutRow,
                         0);
    _pGltMain->addWidget(_pBtnCancel,
                         layoutRow++,
                         1, 1, 2);
    _pBtnSave->setDefault(true);
}

void EditUserDialog::_onBtnSaveClicked()
{
    if (_pLtxtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                             tr("Login"),
                             tr("No login entered."),
                             QMessageBox::Ok
                            );
        return;
    }
    this->accept();
}

void EditUserDialog::_onBtnCancelClicked()
{
    this->reject();
}

void EditUserDialog::_onBtnResetPasswordClicked()
{
    QMessageBox::StandardButton confirmResetPassword;
    confirmResetPassword = QMessageBox::question(this,
                                              tr("Reset password"),
                                              tr("Are you sure you want to reset password?"),
                                              QMessageBox::Yes|QMessageBox::No);
    if (confirmResetPassword == QMessageBox::Yes)
    {
        QString strNewPassword("");
        _pLblResetedPassword->setText(strNewPassword);
        for (quint8 i = 0; i < 8; ++i)
        {
            quint8 symbolCategory = qrand() % 3;
            switch (symbolCategory)
            {
            case 0:
                strNewPassword.push_back(QChar('0' + qrand() % 10));
                break;
            case 1:
                strNewPassword.push_back(QChar('A' + qrand() % 26));
                break;
            case 2:
                strNewPassword.push_back(QChar('a' + qrand() % 26));
                break;
            }
        }
        _pLblResetedPassword->setText(strNewPassword);
        _pLblResetedPasswordLbl->setVisible(true);
        _pLblResetedPassword->setVisible(true);
        _pBtnCopy->setVisible(true);
    }
}

void EditUserDialog::_copyResetedPassword()
{
    QApplication::clipboard()->setText(_pLblResetedPassword->text());
}

EditUserDialog::~EditUserDialog()
{
}
