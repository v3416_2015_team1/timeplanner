#include "client.h"

Client::Client(QWidget *parent)
    :QWidget(parent)
{
    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle("Time Planner");
    resize(320, 240);
    adjustSize();
    vl = new QVBoxLayout();
    nameLayout = new QHBoxLayout;
    nameLabel = new QLabel("      Name");
    login = new QLineEdit(this);
    login->setPlaceholderText("Enter your login");

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(login);
    vl->addLayout(nameLayout);

    passwordLayout = new QHBoxLayout;
    passwordLabel = new QLabel("Password");
    password = new QLineEdit(this);
    password->setEchoMode(QLineEdit::Password);
    password->setPlaceholderText("Enter your password");
    passwordLayout->addWidget(passwordLabel);
    passwordLayout->addWidget(password);
    vl->addLayout(passwordLayout);

    buttons = new QHBoxLayout;

    signIn = new QPushButton("Sign In");

    signIn->setDefault(true);
    signIn->setFocusPolicy(Qt::StrongFocus);

    cancel = new QPushButton("Cancel");
    buttons->insertSpacing(0,75);
    buttons->addWidget(signIn);
    buttons->addWidget(cancel);
    buttonItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
    buttons->addSpacerItem(buttonItem);


    connect(signIn, &QPushButton::clicked, this, &Client::onSignInPress);
    connect(cancel, &QPushButton::clicked, this, &QApplication::quit);

    vl->addLayout(buttons);
    this->setLayout(vl);

    //timer = new QTimer(this);
    //connect(timer,SIGNAL(timeout()), this, SLOT(askNotification()));
    //timer->start(60 * 1000 * 3);
}

void Client::keyPressEvent(QKeyEvent * e)
{
    if (e->key() == Qt::Key_Return)
    {
        signIn->click();
    }
}

void Client::startApplication()
{
     this->show();
}

void Client::onChangeStatusPress(uint task_id)
{


    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    // Отправляем серверу запрос на получение Задач пользователя
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_ALL_USER);
    QString stat;
    int c_i;
    for (int i = 0; i <  mainWidget->tasks.size(); i++)
    {
        if  (mainWidget->tasks[i]->num == task_id)
        {
            stat = mainWidget->tasks[i]->statusBox->currentText();
            c_i = i;
            break;
        }
    }

    if (stat == "Done")
        delete mainWidget->tasks[c_i];

    out << idSession << login << task_id << stat;

    socket->write(block);
}

void Client::onSignInPress()
{
    this->connectToServer();
}

void Client::performLogOut()
{
    name.clear();
    familyName.clear();
    stepName.clear();//имя, фамилия, отчество
    post.clear();
    password->clear();
    login->clear();

    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(SIGN_OUT);
    out << idSession;
    idSession = NULL;

    out.device()->seek(0);
    socket->write(block);

    socket->waitForBytesWritten();
    socket->disconnectFromHost();
    while (socket->state() != QAbstractSocket::UnconnectedState)
    {
       socket->waitForDisconnected();
    }
    socket->close();
    delete socket;
    socket = NULL;
}

void Client::onLogOutSignal()
{
    performLogOut();
    mainWidget->close();
    delete mainWidget;
    mainWidget = NULL;
    this->show();
}

void Client::onAppQuit()
{
    QMessageBox::StandardButton confirmQuit;
    confirmQuit = QMessageBox::question(this,
                                        tr("Quit TimePlanner"),
                                        tr("Are you sure you want to quit TimePlanner?"),
                                        QMessageBox::Yes|QMessageBox::No);
    if (confirmQuit == QMessageBox::Yes)
    {
        performLogOut();
    }

}
void Client::askAllUser()
{

    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_ALL_USER);
    out << idSession;

    socket->write(block);

}


void Client::askCreateTask()
{


    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);



    QString user =  mainWidget->getTaskUser();
    QDate dataDeadLine = mainWidget->getDeadlineDate();
    QString tn = mainWidget->getTaskNameText();
    QString description = mainWidget->getTaskDescriptionText();

    out << quint64(0) << qint8(CLIENT_SEND_ASK_CREATE_TASK);
    out << idSession << user << tn << description << dataDeadLine;

    socket->write(block);

}

void Client::askEditTask(uint task_id)
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_EDIT_TASK);

    out << idSession << login << task_id;

    socket->write(block);

}

void Client::askForUser()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_USER);

    out << this->login->text() << this->password->text();
    out.device()->seek(0);
    socket->write(block);

}

void Client::askForTasksCreator()
{

    // Проверяем наличие соединения
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }


    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_CREATOR);
    out << idSession;

    socket->write(block);

}


void Client::askForTasksAssignee()
{

    // Проверяем наличие соединения
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }


    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_ASSIGNEE);
    out << idSession;

    socket->write(block);

}


void Client::connectToServer()
{
        socket = new QTcpSocket();
        connect(socket, &QTcpSocket::connected, this, &Client::connected);
        connect(socket, &QTcpSocket::disconnected,socket, &QTcpSocket::deleteLater);

        connect(socket, &QTcpSocket::readyRead, this, &Client::readServerAnswer);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(error(QAbstractSocket::SocketError)));

        socket->connectToHost("localhost",3333);
        socket->waitForConnected();
}
void Client::askNotification()
{
    if (socket->state() == QAbstractSocket::UnconnectedState)
    {
       QErrorMessage * err = new QErrorMessage;
       err->showMessage("No connection to the server.");
       return;
    }

    // Отправляем серверу запрос на получение Задач пользователя
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_NOTIFICATION);
    out << idSession;
    socket->write(block);
}
void Client::connected()
{
    askForUser();

}
void Client::connectCancel()
{
    socket->disconnectFromHost();
    if (socket->state() != QAbstractSocket::UnconnectedState)
        socket->waitForDisconnected();
}


void Client::readServerAnswer()
{

    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_5);
    qint8 answerCode = 0;
    qint64 sizeData;
    QString info;
    QString taskName, taskDescription, taskAssigneeLogin, taskStatus;
    QString caption, content, deadline;
    QDateTime taskModified, taskDeadLine;
    qint32 taskId;
    QErrorMessage * err;

    int numUsers = 0;
    int row = 0, count = 0;
    int numTasks = 0;
    int errorCode = 0;
    bool unknownAnswerCode = false;

    while (!in.atEnd() && !unknownAnswerCode)
    {
        in >> sizeData >> answerCode;
        switch(answerCode)
        {
        case CLIENT_SEND_ASK_USER:

            in >> name >> stepName >> familyName;
            in >> post;
            in >> idSession; ///
            askForTasksAssignee();
            this->hide();

            info = familyName + " " + name + " " + stepName + " (" +post + ")";
            mainWidget = new Widget();
            connect(mainWidget, &Widget::appQuit, this, &Client::onAppQuit);

            connect(mainWidget, &Widget::askCreateTaskSignal, this, &Client::askCreateTask);
            connect(mainWidget, &Widget::askAssigneeTasks, this, Client::askForTasksAssignee);
            connect(mainWidget, &Widget::askCreatedTasks, this, Client::askForTasksCreator);
            connect(mainWidget, &Widget::logOutSignal, this, Client::onLogOutSignal);
            connect(mainWidget, &Widget::allUsersSignal, this, Client::askAllUser);

            connect(this, SIGNAL(sendNewNotification(QString,QString)),mainWidget,SLOT(getNewNotification(QString,QString)));
            mainWidget->setUserName(info);
            mainWidget->show();
            break;

        case CLIENT_SEND_ASK_TASK_CREATOR:

            in >> numTasks;
            mainWidget->tasks.resize(numTasks);
            while (true)
            {
                if (row == numTasks)   break;

                in >> taskId >> taskName >> taskDescription >> taskModified >> taskDeadLine >> taskAssigneeLogin >> taskStatus;
                mainWidget->addTaskLine(row,taskId, taskName, taskDescription, taskModified, taskDeadLine, taskAssigneeLogin, taskStatus);
                connect(mainWidget->tasks[row], SIGNAL(changeStatusSignal(uint)), this, SLOT(onChangeStatusPress(uint)));
                connect(mainWidget->tasks[row], SIGNAL(taskDescrChanged(uint)), this, SLOT(askEditTask(uint)));
                row++;
            }

            break;

        case CLIENT_SEND_ASK_TASK_ASSIGNEE:

            in >> numTasks;

            mainWidget->tasks.resize(numTasks);
            while (true)
            {
                if (row == numTasks)   break;

                in >> taskId >> taskName >> taskDescription >>
                        taskModified >> taskDeadLine >> taskAssigneeLogin >> taskStatus;
                mainWidget->addTaskLine(row, taskId, taskName,
                                        taskDescription, taskModified, taskDeadLine, taskAssigneeLogin, taskStatus);

                connect(mainWidget->tasks[row], SIGNAL(changeStatusSignal(uint)), this, SLOT(onChangeStatusPress(uint)));
                row++;
            }

            break;

        case CLIENT_SEND_ASK_ALL_USER:
            in >> numUsers;
            while (true)
            {
                if (count  == numUsers) break;
                in >> info;
                mainWidget->addUserToList(info);
                count++;
            }

            break;
        case SERVER_SEND_NEW_TASK_NOTIFICATION:
            in >> caption >> content >> deadline;
            emit sendNewNotification(tr("New task"), QString("%1 \n %2 \n Deadline: %3").arg(caption).arg(content).arg(deadline));
            emit mainWidget->emitRefreshClicked();
            break;
        case SERVER_SEND_USER_DELETE:
           //TODO
           break;
        case SERVER_SEND_USER_EDITED:
           //TODO
           break;
        case CLIENT_ERROR:
            in >> errorCode;
            if (errorCode == 4001)
            {
                err = new QErrorMessage;
                err->showMessage("Указан неверный логин/пароль");
                //??
            }
            break;
        case 10: // QString message from server
            in >> info;
            qDebug() << info;
            break;
        default:
            unknownAnswerCode = true;
            break;
        }

    }

}

void Client::error(QAbstractSocket::SocketError error)
{
    // Обрабатываем найденные ошибки, связанные с подключением
    QString strError = "Error: ";
    switch(error)
    {
    case QAbstractSocket::ConnectionRefusedError:
        strError.append("The connection was refused.");
        break;
    case QAbstractSocket::RemoteHostClosedError:
        strError.append("The remote host is closed.");
        break;
    case QAbstractSocket::HostNotFoundError:
        strError.append("The host was not found.");
        break;
    default:
        strError.append(QString(socket->errorString()));
    }

    QErrorMessage * err = new QErrorMessage;
    err->showMessage(strError);


}

Client::~Client()
{


}


