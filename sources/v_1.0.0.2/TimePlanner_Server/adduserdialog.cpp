#include "adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Add new user"));
    lblLogin = new QLabel(tr("Login"));
    lblPassword = new QLabel(tr("Password"));
    lblPasswordConfirm = new QLabel(tr("Confirm password"));
    lblFirstName = new QLabel(tr("First name"));
    lblSecondName = new QLabel(tr("Second name"));
    lblLastName = new QLabel(tr("Last name"));
    lblPosition = new QLabel(tr("Position"));
    txtLogin = new QLineEdit;
    txtLogin->setPlaceholderText(tr("Login"));
    txtPassword = new QLineEdit;
    txtPassword->setPlaceholderText(tr("Password"));
    txtPassword->setEchoMode(QLineEdit::Password);
    txtPasswordConfirm = new QLineEdit;
    txtPasswordConfirm->setPlaceholderText(tr("Confirm password"));
    txtPasswordConfirm->setEchoMode(QLineEdit::Password);
    txtFirstName = new QLineEdit;
    txtFirstName->setPlaceholderText(tr("First name"));
    txtSecondName = new QLineEdit;
    txtSecondName->setPlaceholderText(tr("Second name"));
    txtLastName = new QLineEdit;
    txtLastName->setPlaceholderText(tr("Last name"));
    txtPosition = new QLineEdit;
    txtPosition->setPlaceholderText(tr("Position"));
    btnSave = new QPushButton(tr("Save user"));
    btnCancel = new QPushButton(tr("Cancel"));
    connect(btnSave, &QPushButton::clicked,
            this, &AddUserDialog::onBtnSaveClicked);
    connect(btnCancel, &QPushButton::clicked,
            this, &AddUserDialog::onBtnCancelClicked);
    quint8 layoutRow = 0;
    layout = new QGridLayout(this);
    layout->addWidget(lblLogin, layoutRow, 0);
    layout->addWidget(txtLogin, layoutRow++, 1);
    layout->addWidget(lblPassword, layoutRow, 0);
    layout->addWidget(txtPassword, layoutRow++, 1);
    layout->addWidget(lblPasswordConfirm, layoutRow, 0);
    layout->addWidget(txtPasswordConfirm, layoutRow++, 1);
    layout->addWidget(lblFirstName, layoutRow, 0);
    layout->addWidget(txtFirstName, layoutRow++, 1);
    layout->addWidget(lblSecondName, layoutRow, 0);
    layout->addWidget(txtSecondName, layoutRow++, 1);
    layout->addWidget(lblLastName, layoutRow, 0);
    layout->addWidget(txtLastName, layoutRow++, 1);
    layout->addWidget(lblPosition, layoutRow, 0);
    layout->addWidget(txtPosition, layoutRow++, 1);
    layout->addWidget(btnSave, layoutRow, 0);
    layout->addWidget(btnCancel, layoutRow++, 1);
}

AddUserDialog::~AddUserDialog()
{

}

void AddUserDialog::onBtnSaveClicked()
{
    if (txtLogin->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Login"),
                            tr("No login entered."),
                            QMessageBox::Ok
                            );
        return;
    }
    if (txtPassword->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("No password entered."),
                            QMessageBox::Ok
                            );
        txtPassword->clear();
        txtPasswordConfirm->clear();
        return;
    }
    if (txtPassword->text() != txtPasswordConfirm->text())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("Password mismatch."),
                            QMessageBox::Ok
                            );
        txtPassword->clear();
        txtPasswordConfirm->clear();
        return;
    }
    this->accept();
}

void AddUserDialog::onBtnCancelClicked()
{
    this->reject();
}
