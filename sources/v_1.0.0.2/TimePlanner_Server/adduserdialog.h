#ifndef ADDUSERDIALOG_H
#define ADDUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>

class AddUserDialog : public QDialog
{
    Q_OBJECT
public:
    AddUserDialog(QWidget *parent = 0);
    ~AddUserDialog();
    QString getLogin(){return txtLogin->text();}
    QString getPassword(){return txtPassword->text();}
    QString getFirstName(){return txtFirstName->text();}
    QString getSecondame(){return txtSecondName->text();}
    QString getLastName(){return txtLastName->text();}
    QString getPosition(){return txtPosition->text();}
signals:
public slots:
private slots:
    void onBtnSaveClicked();
    void onBtnCancelClicked();
private:
    QLabel *lblLogin;
    QLabel *lblPassword;
    QLabel *lblPasswordConfirm;
    QLabel *lblFirstName;
    QLabel *lblSecondName;
    QLabel *lblLastName;
    QLabel *lblPosition;
    QLineEdit *txtLogin;
    QLineEdit *txtPassword;
    QLineEdit *txtPasswordConfirm;
    QLineEdit *txtFirstName;
    QLineEdit *txtSecondName;
    QLineEdit *txtLastName;
    QLineEdit *txtPosition;
    QPushButton *btnSave;
    QPushButton *btnCancel;
    QGridLayout *layout;
};

#endif // ADDUSERDIALOG_H
