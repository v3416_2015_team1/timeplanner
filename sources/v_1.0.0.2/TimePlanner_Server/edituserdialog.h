#ifndef EDITUSERDIALOG_H
#define EDITUSERDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QMessageBox>

class EditUserDialog : public QDialog
{
public:
    EditUserDialog(QVector<QString> &initData);
    ~EditUserDialog();
    QString getLogin(){return txtLogin->text();}
    QString getFirstName(){return txtFirstName->text();}
    QString getSecondame(){return txtSecondName->text();}
    QString getLastName(){return txtLastName->text();}
    QString getPosition(){return txtPosition->text();}
signals:

public slots:
private slots:
    void onBtnSaveClicked();
    void onBtnCancelClicked();
private:
    QLabel *lblLogin;
    QLabel *lblFirstName;
    QLabel *lblSecondName;
    QLabel *lblLastName;
    QLabel *lblPosition;
    QLineEdit *txtLogin;
    QLineEdit *txtFirstName;
    QLineEdit *txtSecondName;
    QLineEdit *txtLastName;
    QLineEdit *txtPosition;
    QPushButton *btnSave;
    QPushButton *btnCancel;
    QGridLayout *layout;
};

#endif // EDITUSERDIALOG_H
