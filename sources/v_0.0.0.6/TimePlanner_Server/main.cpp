#include "timeplanner_server.h"
#include <QApplication>
#include <QtSql/QSqlDatabase>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath("plugins");
    QApplication a(argc, argv);
    TimePlannerServer w;
    w.show();
    QString log_string;
    w.connectToMySQL("timeplanner_db", 3306, log_string);
    w.log(log_string);
    w.startTCPServer();

    return a.exec();
}
