#include "newpassworddialog.h"

NewPasswordDialog::NewPasswordDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Create new password"));
    _pLblNewPassword = new QLabel(tr("New password"));
    _pLblConfirmNewPassword = new QLabel(tr("Confirm new password"));
    _pLtxtNewPassword = new QLineEdit();
    _pLtxtNewPassword->setEchoMode(QLineEdit::Password);
    _pLtxtNewPassword->setPlaceholderText(tr("New password"));
    _pLtxtConfirmNewPassword = new QLineEdit();
    _pLtxtConfirmNewPassword->setEchoMode(QLineEdit::Password);
    _pLtxtConfirmNewPassword->setPlaceholderText(tr("Confirm new password"));
    _pBtnOk = new QPushButton(tr("Ok"));
    _pBtnCancel = new QPushButton(tr("Cancel"));
    connect(_pBtnOk, &QPushButton::clicked,
            this, &NewPasswordDialog::onBtnOkClicked);
    connect(_pBtnCancel, &QPushButton::clicked,
            this, &NewPasswordDialog::onBtnCancelClicked);
    _pGlt = new QGridLayout(this);
    int rowNum = 0;
    _pGlt->addWidget(_pLblNewPassword,
                     rowNum,
                     0);
    _pGlt->addWidget(_pLtxtNewPassword,
                     rowNum++,
                     1);
    _pGlt->addWidget(_pLblConfirmNewPassword,
                     rowNum,
                     0);
    _pGlt->addWidget(_pLtxtConfirmNewPassword,
                     rowNum++,
                     1);
    _pGlt->addWidget(_pBtnOk,
                     rowNum,
                     0);
    _pGlt->addWidget(_pBtnCancel,
                     rowNum++,
                     1);
}

NewPasswordDialog::~NewPasswordDialog()
{
}

void NewPasswordDialog::onBtnOkClicked()
{
    if (_pLtxtNewPassword->text().isEmpty())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("No password entered."),
                            QMessageBox::Ok
                            );
        _pLtxtNewPassword->clear();
        _pLtxtConfirmNewPassword->clear();
        return;
    }
    if (_pLtxtNewPassword->text() != _pLtxtConfirmNewPassword->text())
    {
        QMessageBox::warning(0,
                            tr("Password"),
                            tr("Password mismatch."),
                            QMessageBox::Ok
                            );
        _pLtxtNewPassword->clear();
        _pLtxtConfirmNewPassword->clear();
        return;
    }
    this->accept();
}

void NewPasswordDialog::onBtnCancelClicked()
{
    this->reject();
}
