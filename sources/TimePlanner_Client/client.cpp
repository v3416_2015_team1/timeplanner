#include "client.h"

Client::Client(QWidget *parent)
    :QWidget(parent)
{
    readSettings(SETTINGS_FILE);
    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle(tr("Time Planner"));
    resize(320,
           240);
    adjustSize();
    _pVltBase = new QVBoxLayout();
    _pHltLogin = new QHBoxLayout;
    _pLblLogin = new QLabel(tr("      Name"));
    _pLtxtLogin = new QLineEdit(this);
    _pLtxtLogin->setPlaceholderText(tr("Enter your login"));
    _pHltLogin->addWidget(_pLblLogin);
    _pHltLogin->addWidget(_pLtxtLogin);
    _pVltBase->addLayout(_pHltLogin);
    _pHltPassword = new QHBoxLayout;
    _pLblPassword = new QLabel(tr("Password"));
    _pLtxtPassword = new QLineEdit(this);
    _pLtxtPassword->setEchoMode(QLineEdit::Password);
    _pLtxtPassword->setPlaceholderText(tr("Enter your password"));
    _pHltPassword->addWidget(_pLblPassword);
    _pHltPassword->addWidget(_pLtxtPassword);
    _pVltBase->addLayout(_pHltPassword);
    _pHltControlBtns = new QHBoxLayout;
    _pBtnSignIn = new QPushButton(tr("Sign In"));
    _pBtnSignIn->setDefault(true);
    _pBtnSignIn->setFocusPolicy(Qt::StrongFocus);
    _pBtnCancel = new QPushButton(tr("Cancel"));
    _pHltControlBtns->insertSpacing(0,75);
    _pHltControlBtns->addWidget(_pBtnSignIn);
    _pHltControlBtns->addWidget(_pBtnCancel);
    _pSpaserButtons = new QSpacerItem(1,
                                      1,
                                      QSizePolicy::Expanding,
                                      QSizePolicy::Fixed);
    _pHltControlBtns->addSpacerItem(_pSpaserButtons);
    connect(_pBtnSignIn, &QPushButton::clicked,
            this, &Client::onSignInPress);
    connect(_pBtnCancel, &QPushButton::clicked,
            this, &QApplication::quit);

    _pVltBase->addLayout(_pHltControlBtns);
    this->setLayout(_pVltBase);
}

void Client::keyPressEvent(QKeyEvent * e)
{
    if (e->key() == Qt::Key_Return)
    {
        _pBtnSignIn->click();
    }
}

void Client::startApplication()
{
    this->show();
}

void Client::onChangeStatusPress(uint taskId)
{
    QString strStat;
    uint size = _pPlannerWidget->getTasksSize();
    for (int i = 0; i < size; i++)
    {
        if  (_pPlannerWidget->getTaskId(i) == taskId)
        {
            strStat = _pPlannerWidget->getStatusText(i);
            break;
        }
    }
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_CHANGE_STATUS);
    out <<  _btarrIdSession << taskId << strStat;
    _pSocket->write(block);
    _pSocket->waitForBytesWritten();
    emit _pPlannerWidget->onRefreshPress();
}

void Client::onSignInPress()
{
    this->connectToServer();
}

void Client::performLogOut()
{
    _strName.clear();
    _strFamilyName.clear();
    _strStepName.clear();
    _strPost.clear();
    _pLtxtPassword->clear();
    _pLtxtLogin->clear();
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(SIGN_OUT);
    out <<  _btarrIdSession;
     _btarrIdSession = NULL;
    out.device()->seek(0);
    _pSocket->write(block);
    _pSocket->waitForBytesWritten();
    _pSocket->disconnectFromHost();
    _pSocket->close();
    delete _pSocket;
    _pSocket = NULL;
}

void Client::onLogOutSignal()
{
    performLogOut();
    _pPlannerWidget->close();
    delete _pPlannerWidget;
    _pPlannerWidget = NULL;
    this->show();
}
void Client::forceSignOut()
{
    _strName.clear();
    _strFamilyName.clear();
    _strStepName.clear();
    _strPost.clear();
    _pLtxtPassword->clear();
    _pLtxtLogin->clear();
    _pPlannerWidget->close();
    delete _pPlannerWidget;
    _pPlannerWidget = NULL;
    this->setVisible(true);
}

void Client::onAppQuit()
{
    QMessageBox::StandardButton confirmQuit;
    confirmQuit = QMessageBox::question(this,
                                        tr("Quit TimePlanner"),
                                        tr("Are you sure you want to quit TimePlanner?"),
                                        QMessageBox::Yes|QMessageBox::No);
    if (confirmQuit == QMessageBox::Yes)
    {
        performLogOut();
    }
}

void Client::askAllUser()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_ALL_USER);
    out <<  _btarrIdSession;
    _pSocket->write(block);
}

void Client::askCreateTask()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    QString strUser =  _pPlannerWidget->getTaskUser();
    QDate dateDeadLine = _pPlannerWidget->getDeadlineDate();
    QString strTaskName = _pPlannerWidget->getTaskNameText();
    QString strDescription = _pPlannerWidget->getTaskDescriptionText();
    out << quint64(0) << qint8(CLIENT_SEND_ASK_CREATE_TASK);
    out <<  _btarrIdSession << strUser << strTaskName << strDescription << dateDeadLine;
    _pSocket->write(block);
}

void Client::askEditTask(uint taskId, uint rowId)
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_EDIT_TASK);
    out <<  _btarrIdSession << taskId << _pPlannerWidget->getTaskName(rowId);
    out << _pPlannerWidget->getDescription(rowId);
    out << _pPlannerWidget->getDeadline(rowId) << _pPlannerWidget->getAssignee(rowId);
    _pSocket->write(block);

}

void Client::askForUser()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_USER);
    out << this->_pLtxtLogin->text() << this->_pLtxtPassword->text();
    out.device()->seek(0);
    _pSocket->write(block);
}

void Client::askForUserWithNewPassword(QString newPassword)
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(TEMP_PASSWORD);
    out << this->_pLtxtLogin->text() << this->_pLtxtPassword->text() << newPassword;
    out.device()->seek(0);
    _pSocket->write(block);
}

void Client::askForTasksCreator()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_CREATOR);
    out <<  _btarrIdSession;
    _pSocket->write(block);
}

void Client::askForTasksAssignee()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_TASK_ASSIGNEE);
    out <<  _btarrIdSession;
    _pSocket->write(block);
}

void Client::connectToServer()
{
    _pSocket = new QTcpSocket();
    connect(_pSocket, &QTcpSocket::connected,
            this, &Client::connected);
    connect(_pSocket, &QTcpSocket::disconnected,
            _pSocket, &QTcpSocket::deleteLater);
    connect(_pSocket, &QTcpSocket::readyRead,
            this, &Client::readServerAnswer);
    connect(_pSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(error(QAbstractSocket::SocketError)));

    if ( _mapSettings.contains(SETTING_SERVER_IP) &&  _mapSettings.contains(SETTING_TCP_PORT))
    {
        _pSocket->connectToHost( _mapSettings[SETTING_SERVER_IP], (quint16) _mapSettings[SETTING_TCP_PORT].toInt());
        _pSocket->waitForConnected();
    }
    else
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("Undefined %1 or %2 at %3").arg(SETTING_SERVER_IP, SETTING_TCP_PORT, SETTINGS_FILE));
    }
}

void Client::askNotification()
{
    if (_pSocket->state() == QAbstractSocket::UnconnectedState)
    {
        QErrorMessage *err = new QErrorMessage;
        err->showMessage(tr("No connection to the server."));
        return;
    }
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << qint8(CLIENT_SEND_ASK_NOTIFICATION);
    out <<  _btarrIdSession;
    _pSocket->write(block);
}

void Client::connected()
{
    askForUser();
}

void Client::connectCancel()
{
    _pSocket->disconnectFromHost();
    if (_pSocket->state() != QAbstractSocket::UnconnectedState)
        _pSocket->waitForDisconnected();
}


void Client::readServerAnswer()
{
    QDataStream in(_pSocket);
    in.setVersion(QDataStream::Qt_5_5);
    qint8 answerCode = 0;
    qint64 sizeData;
    QString strInfo;
    QString strTaskName,
            strTaskDescription,
            strTaskAssigneeLogin,
            strTaskStatus;
    QString strCaption,
            strContent,
            strDeadline;
    QDateTime dateTaskModified,
            dateTaskDeadLine;
    qint32 taskId;
    QErrorMessage * error;
    int numUsers = 0;
    int row = 0, count = 0;
    int numTasks = 0;
    int errorCode = 0;
    bool unknownAnswerCode = false;
    bool createTasksRight = false;
    NewPasswordDialog *newPasswordDialog;
    while (!in.atEnd() && !unknownAnswerCode)
    {
        in >> sizeData >> answerCode;
        switch(answerCode)
        {
        case TEMP_PASSWORD:
            newPasswordDialog = new NewPasswordDialog();
            newPasswordDialog->setModal(true);
            if (newPasswordDialog->exec())
            {
                askForUserWithNewPassword(newPasswordDialog->getNewPassword());
            }
            delete newPasswordDialog;
            break;
        case CLIENT_SEND_ASK_USER:
            in >> _strName >> _strStepName >> _strFamilyName;
            in >> _strPost;
            in >> createTasksRight;
            in >>  _btarrIdSession;
            askForTasksAssignee();
            this->hide();
            strInfo = _strFamilyName + " " + _strName + " " + _strStepName + " (" +_strPost + ")";
            _pPlannerWidget = new PlannerWidget();
            connect(_pPlannerWidget, &PlannerWidget::appQuit,
                    this, &Client::onAppQuit);
            connect(_pPlannerWidget, &PlannerWidget::askCreateTaskSignal,
                    this, &Client::askCreateTask);
            connect(_pPlannerWidget, &PlannerWidget::askAssigneeTasks,
                    this, &Client::askForTasksAssignee);
            connect(_pPlannerWidget, &PlannerWidget::askCreatedTasks,
                    this, &Client::askForTasksCreator);
            connect(_pPlannerWidget, &PlannerWidget::logOutSignal,
                    this, &Client::onLogOutSignal);
            connect(_pPlannerWidget, &PlannerWidget::allUsersSignal,
                    this, &Client::askAllUser);
            connect(this, SIGNAL(sendNewNotification(QString,QString)),
                    _pPlannerWidget, SLOT(getNewNotification(QString,QString)));
            _pPlannerWidget->setUserName(strInfo);
            _pPlannerWidget->setTaskListTypeSwitchVisible(createTasksRight);
            _pPlannerWidget->setCreateTaskVisible(createTasksRight);
            _pPlannerWidget->show();
            break;
        case CLIENT_SEND_ASK_TASK_CREATOR:
            in >> numTasks;
            _pPlannerWidget->resizeVecTasks(numTasks);
            while (true)
            {
                if (row == numTasks)
                    break;
                in >> taskId >> strTaskName >> strTaskDescription >> dateTaskModified >> dateTaskDeadLine >> strTaskAssigneeLogin >> strTaskStatus;
                _pPlannerWidget->addTaskLine(row,
                                             taskId,
                                             strTaskName,
                                             strTaskDescription,
                                             dateTaskModified, dateTaskDeadLine,
                                             strTaskAssigneeLogin, strTaskStatus);
                connect(_pPlannerWidget->getTask(row), SIGNAL(changeStatusSignal(uint)),
                        this, SLOT(onChangeStatusPress(uint)));
                connect(_pPlannerWidget->getTask(row), SIGNAL(taskChanged(uint, uint)),
                        this, SLOT(askEditTask(uint, uint)));
                row++;
            }
            break;
        case CLIENT_SEND_ASK_TASK_ASSIGNEE:
            in >> numTasks;
              _pPlannerWidget->resizeVecTasks(numTasks);
            while (true)
            {
                if (row == numTasks)   break;
                in >> taskId >> strTaskName >> strTaskDescription >>
                        dateTaskModified >> dateTaskDeadLine >> strTaskAssigneeLogin >> strTaskStatus;
                _pPlannerWidget->addTaskLine(row,
                                             taskId,
                                             strTaskName,
                                             strTaskDescription,
                                             dateTaskModified,
                                             dateTaskDeadLine,
                                             strTaskAssigneeLogin,
                                             strTaskStatus);
                connect(_pPlannerWidget->getTask(row), SIGNAL(changeStatusSignal(uint)),
                        this, SLOT(onChangeStatusPress(uint)));
                row++;
            }
            break;
        case CLIENT_SEND_ASK_ALL_USER:
            in >> numUsers;
            while (true)
            {
                if (count  == numUsers) break;
                in >> strInfo;
                _pPlannerWidget->addUserToList(strInfo);
                count++;
            }
            break;
        case CLIENT_SEND_CHANGE_STATUS:
            //
            break;
        case SERVER_SEND_NEW_TASK_NOTIFICATION:
            in >> strCaption >> strContent >> strDeadline;
            emit sendNewNotification(tr("New task"),
                                     QString("%1 \n %2 \n Deadline:  %3").arg(strCaption).arg(strContent).arg(strDeadline));
            emit _pPlannerWidget->emitRefreshClicked();
            break;
        case SIGN_OUT:
            this->onLogOutSignal();
            break;
        case FORCE_SIGN_OUT:
            this->forceSignOut();
            break;
        case CLIENT_ERROR:
            in >> errorCode;
            if (errorCode == 4001)
            {
                error = new QErrorMessage;
                error->showMessage(tr("Incorrect login or password!"));
            }
            break;
        case MESSAGE:
            in >> strInfo;
            qDebug() << strInfo;
            break;
        default:
            unknownAnswerCode = true;
            in.skipRawData(_pSocket->bytesAvailable());
            break;
        }
    }
}

void Client::error(QAbstractSocket::SocketError error)
{
    QString strError = "Error: ";
    switch(error)
    {
    case QAbstractSocket::ConnectionRefusedError:
        strError.append(tr("The connection was refused."));
        break;
    case QAbstractSocket::RemoteHostClosedError:
        strError.append(tr("The remote host is closed."));
        break;
    case QAbstractSocket::HostNotFoundError:
        strError.append(tr("The host was not found."));
        break;
    default:
        strError.append(QString(_pSocket->errorString()));
    }
    QErrorMessage * err = new QErrorMessage;
    err->showMessage(strError);
}

void Client::readSettings(QString settingsFileName)
{
    QFile settings_file(settingsFileName);
    if (settings_file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream settings_stream(&settings_file);
        while (!settings_stream.atEnd())
        {
            QStringList setting = settings_stream.readLine().split(QRegExp("(\\=)"));
            if (setting.size() == 2)
                 _mapSettings.insert(setting.at(0), setting.at(1));
        }
        settings_file.close();
    }
    else
    {
        QErrorMessage * err = new QErrorMessage;
        err->showMessage(tr("Settings file %1 not found!").arg(settingsFileName));
    }
}

Client::~Client(){}
