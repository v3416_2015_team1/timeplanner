#include "plannerwidget.h"

PlannerWidget::PlannerWidget(QWidget *parent)
    : QWidget(parent)
{
    taskType = ASSIGNEE;
    setWindowIcon(QIcon(":/Icons/TimePlanner.ico"));
    setWindowTitle(tr("Time Planner"));
    actionShowHide = new QAction(tr("Show/Hide Time Planner"),
                                 this);
    connect(actionShowHide, &QAction::triggered,
            this, &PlannerWidget::showHide);
    actionQuit = new QAction(tr("Quit"), this);
    connect(actionQuit,&QAction::triggered,
            this, &PlannerWidget::onAppQuit);
    menu = new QMenu(this);
    menu->addAction(actionShowHide);
    menu->addAction(actionQuit);
    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setContextMenu(menu);
    systemTrayIcon->setToolTip(tr("Time Planner"));
    systemTrayIcon->setIcon(QIcon(":/Icons/TimePlanner.ico"));
    systemTrayIcon->show();
    connect(systemTrayIcon, &QSystemTrayIcon::activated,
            this, &PlannerWidget::onTrayIconActivated);
    resize(640,
           320);
    adjustSize();
    _pVltBase = new QVBoxLayout(this);
    _pGbxEdit = new QGroupBox(this);
    _pHltButtons = new QHBoxLayout;
    _pLblUserName = new QLabel;
    QFont f( "Calibri",
             10,
             QFont::Bold);
    _pLblUserName->setFont(f);
    _pHltButtons->addWidget(_pLblUserName);
    _pSpaserEdit = new QSpacerItem(1,
                                   0,
                                   QSizePolicy::Expanding,
                                   QSizePolicy::Minimum);
    _pHltButtons->addSpacerItem(_pSpaserEdit);
    _pBtnCreate = new QPushButton;
    _pBtnCreate->setIcon(QIcon(":/Icons/add.ico"));
    _pBtnCreate->setToolTip(tr("Create New Task"));
    connect(_pBtnCreate, &QPushButton::clicked,
            this, &PlannerWidget::onCreatePress);
    _pHltButtons->addWidget(_pBtnCreate);
    _pBtnRefresh = new QPushButton;
    _pBtnRefresh->setIcon(QIcon(":/Icons/refresh.ico"));
    _pBtnRefresh->setToolTip(tr("Refresh"));
    connect(_pBtnRefresh, &QPushButton::clicked,
            this, &PlannerWidget::onRefreshPress);
    _pHltButtons->addWidget(_pBtnRefresh);
    _pBtnLogOut = new QPushButton;
    _pBtnLogOut->setIcon(QIcon(":/Icons/exit.ico"));
    _pBtnLogOut->setToolTip(tr("Sign out"));
    connect(_pBtnLogOut, &QPushButton::clicked,
            this, &PlannerWidget::logOut);
    _pHltButtons->addWidget(_pBtnLogOut);
    _pGbxEdit->setLayout(_pHltButtons);
    _pVltBase->addWidget(_pGbxEdit);
    QGroupBox *pGbxTask = new QGroupBox(this);
    _pHltTaskType  = new QHBoxLayout;
    _pLblCurrentTaskType = new QLabel(tr("Assigned tasks"));
    _pLblCurrentTaskType->setFont(f);
    _pHltTaskType ->addWidget(_pLblCurrentTaskType);
    pGbxTask->setLayout(_pHltTaskType );
    _pVltBase->addWidget(pGbxTask);
    _pScrollArea = new QScrollArea(this);
    _pVltBase->addWidget(_pScrollArea);
    _pTaskWidget = new QWidget(this);
    _pVltTask = new QVBoxLayout(_pTaskWidget);
    _pVltTask->setSizeConstraint(QLayout::SetMinimumSize);
    _pVltTask->setAlignment(Qt::AlignTop);
    _pTaskWidget->setLayout(_pVltTask);
    _pScrollArea->setWidget(_pTaskWidget);
    _pScrollArea->setWidgetResizable(true);

    _pScrollArea->setGeometry(0,
                              320,
                              640,
                              200);
    _pScrollArea->adjustSize();
    _pScrollArea->show();
    _pGbxTypeTasks = new QGroupBox(this);
    _pHltControl = new QHBoxLayout;
    _pRbtnCreatedTasks = new QRadioButton(tr("Created tasks"));
    _pRbtnAssigneeTasks = new QRadioButton(tr("Assigneed tasks"));
    _pRbtnAssigneeTasks->setChecked(true);
    connect(_pRbtnCreatedTasks, &QRadioButton::toggled,
            this, &PlannerWidget::onMyTaskBPressed);
    connect(_pRbtnAssigneeTasks, &QRadioButton::toggled,
            this, &PlannerWidget::onAssigneeTaskBPressed);
    _pHltControl->addWidget(_pRbtnCreatedTasks);
    _pHltControl->addWidget(_pRbtnAssigneeTasks);
    _pGbxTypeTasks->setSizePolicy(QSizePolicy::Fixed,
                                  QSizePolicy::Minimum);
    _pGbxTypeTasks->setLayout(_pHltControl);
    _pVltBase->addWidget(_pGbxTypeTasks);
    this->setLayout(_pVltBase);
}

TaskLine * PlannerWidget::getTask(uint row)
{
    return _vecTasks[row];
}

uint PlannerWidget::getTaskId(uint i)
{
    return _vecTasks[i]->getTaskId();
}

QString PlannerWidget::getStatusText(uint i)
{
    return _vecTasks[i]->getStatusText();
}

uint PlannerWidget::getTasksSize()
{
    return _vecTasks.size();
}

QString PlannerWidget::getTaskName(uint rowId)
{
    return _vecTasks[rowId]->getTaskName();
}

QString PlannerWidget::getDescription(uint rowId)
{
    return _vecTasks[rowId]->getDescription();
}
QDateTime PlannerWidget::getDeadline(uint rowId)
{
     return _vecTasks[rowId]->getDeadline();
}

QString PlannerWidget::getAssignee(uint rowId)
{
     return _vecTasks[rowId]->getAssignee();
}

void PlannerWidget::resizeVecTasks(int numTasks)
{
    _vecTasks.resize(numTasks);
}

void PlannerWidget::setTaskListTypeSwitchVisible(bool visible)
{
    this->_pGbxTypeTasks->setVisible(visible);
}

void PlannerWidget::setCreateTaskVisible(bool visible)
{
    _pBtnCreate->setVisible(visible);
}

void PlannerWidget::addUserToList(QString &strInfo)
{
    if (_pCbxEmployees != NULL)
        _pCbxEmployees->addItem(strInfo);
}

void PlannerWidget::onCreatePress()
{
    _pCreateWidget = new QWidget;
    _pCreateWidget->setWindowTitle(tr("Create new task"));
    _pGltCreate = new QGridLayout(_pCreateWidget);
    _pLblNameEmployee = new QLabel(tr("Employee"));
    _pCbxEmployees = new QComboBox;
    emit allUsersSignal();
    _pGltCreate->addWidget(_pLblNameEmployee,
                           0,
                           0);
    _pGltCreate->addWidget(_pCbxEmployees,
                           0,
                           1);
    _pLblNewTaskName = new QLabel(tr("Task"));
    _pLtxtTaskName = new QLineEdit;
    _pLtxtTaskName->setPlaceholderText(tr("Enter caption of your task"));
    _pGltCreate->addWidget(_pLblNewTaskName,
                           1,
                           0);
    _pGltCreate->addWidget(_pLtxtTaskName,
                           1,
                           1);
    _pLblDescription = new QLabel(tr("Description"));
    _pTxtDescription = new QTextEdit;
    _pTxtDescription->setPlaceholderText("Enter content of your task");
    _pGltCreate->addWidget(_pLblDescription,
                           2,
                           0);
    _pGltCreate->addWidget(_pTxtDescription,
                           2,
                           1);
    _pLblDeadline = new QLabel(tr("Deadline"));
    _pDateDeadline = new QDateEdit(QDate::currentDate());
    _pDateDeadline->setMinimumDateTime(QDateTime::currentDateTime());
    _pGltCreate->addWidget(_pLblDeadline,
                           3,
                           0);
    _pGltCreate->addWidget(_pDateDeadline,
                           3,
                           1);
    _pBtnCreateCurrentTask = new QPushButton(tr("Create task"));
    _pGltCreate->addWidget(_pBtnCreateCurrentTask,
                           4,
                           1);
    _pCreateWidget->setLayout(_pGltCreate);
    connect(_pBtnCreateCurrentTask, &QPushButton::clicked,
            this, &PlannerWidget::askCreateTask);
    connect(_pBtnCreateCurrentTask, &QPushButton::clicked,
            _pCreateWidget, &QWidget::close);
    _pCreateWidget->show();
}

QDate PlannerWidget::getDeadlineDate()
{
    return _pDateDeadline->date();
}
QString PlannerWidget::getTaskUser()
{
    return _pCbxEmployees->currentText();
}
QString PlannerWidget::getTaskNameText()
{
    return _pLtxtTaskName->text();
}

QString PlannerWidget::getTaskDescriptionText()
{
    return _pTxtDescription->toPlainText();
}

void PlannerWidget::askCreateTask()
{
    emit this->askCreateTaskSignal();
}

void PlannerWidget::logOut()
{
    taskType = ASSIGNEE;
    for (int i = 0; i < _vecTasks.size(); i++)
    {
        delete _vecTasks[i];
    }
     _vecTasks.clear();
    _pScrollArea->update();
    emit logOutSignal();
}

void PlannerWidget::setUserName(QString strUsername)
{
    _pLblUserName->setText(strUsername);
}

void clearLayout(QLayout *_layout)
{
    QLayoutItem *_child;
    while ((_child = _layout->takeAt(0)) != 0)
    {
        if(_child->layout() != 0)
            clearLayout( _child->layout() );
        else if(_child->widget() != 0)
            delete _child->widget();
        delete _child;
    }
}

void PlannerWidget::cleanLayout(QVBoxLayout *_vltLayout)
{
    QLayoutItem *_child;
    while ((_child = _vltLayout->takeAt(0)) != 0)
    {
        if(_child->layout() != 0)
            clearLayout( _child->layout() );
        else if(_child->widget() != 0)
            delete _child->widget();
        delete _child;
    }
    for (int i = 0; i <  _vecTasks.size(); i++)
    {
        delete  _vecTasks[i];
    }
     _vecTasks.clear();
}

void PlannerWidget::onRefreshPress()
{
    if (taskType == CREATOR)
    {
        cleanLayout(_pVltTask);
        delete _pTaskWidget->layout();
        _pVltTask = NULL;
        _pVltTask = new QVBoxLayout(_pTaskWidget);
        _pVltTask->setSizeConstraint(QLayout::SetMinimumSize);
        _pVltTask->setAlignment(Qt::AlignTop);
        _pTaskWidget->setLayout(_pVltTask);
        emit askCreatedTasks();
    }
    else if (taskType == ASSIGNEE)
    {
        cleanLayout(_pVltTask);
        delete _pTaskWidget->layout();
        _pVltTask = NULL;
        _pVltTask = new QVBoxLayout(_pTaskWidget);
        _pVltTask->setSizeConstraint(QLayout::SetMinimumSize);
        _pVltTask->setAlignment(Qt::AlignTop);
        _pTaskWidget->setLayout(_pVltTask);
        emit askAssigneeTasks();
    }
}
void PlannerWidget::onMyTaskBPressed()
{
    if (_pRbtnCreatedTasks->isChecked())
    {
        if (taskType != CREATOR)
        {
            taskType = CREATOR;
            _pLblCurrentTaskType->setText(tr("Created tasks"));
            cleanLayout(_pVltTask);
            delete _pTaskWidget->layout();
            _pVltTask = NULL;
            _pVltTask = new QVBoxLayout(_pTaskWidget);
            _pVltTask->setSizeConstraint(QLayout::SetMinimumSize);
            _pVltTask->setAlignment(Qt::AlignTop);
            _pTaskWidget->setLayout(_pVltTask);
            emit askCreatedTasks();
        }
    }
}

void PlannerWidget::onAssigneeTaskBPressed()
{
    if (_pRbtnAssigneeTasks->isChecked())
    {
        if (taskType != ASSIGNEE)
        {
            taskType = ASSIGNEE;
            _pLblCurrentTaskType->setText(tr("Assigned tasks"));
            cleanLayout(_pVltTask);
            delete _pTaskWidget->layout();
            _pVltTask = NULL;
            _pVltTask = new QVBoxLayout(_pTaskWidget);
            _pVltTask->setSizeConstraint(QLayout::SetMinimumSize);
            _pVltTask->setAlignment(Qt::AlignTop);
            _pTaskWidget->setLayout(_pVltTask);
            emit askAssigneeTasks();
        }
    }
}

void PlannerWidget::addTaskLine(int row,
                         int taskId,
                         QString strCaption,
                         QString strContent,
                         QDateTime dateModified,
                         QDateTime dateDeadline,
                         QString strAssigneeLogin,
                         QString strStatus)
{
     _vecTasks[row] = new TaskLine(row,
                              taskId,
                              taskType,
                              strCaption,
                              strContent,
                              dateModified,
                              dateDeadline,
                              strAssigneeLogin,
                              strStatus,
                              _pTaskWidget);/////
    _pVltTask->addLayout( _vecTasks[row]->getTaskLayout());
}

void PlannerWidget::emitSignalOut()
{
    emit _pBtnLogOut->clicked();
}

void PlannerWidget::emitRefreshClicked()
{
    emit _pBtnRefresh->clicked();
}

void PlannerWidget::getNewNotification(QString strCaption, QString strContent)
{
    systemTrayIcon->showMessage(strCaption,
                                strContent);
}

void PlannerWidget::closeEvent(QCloseEvent *pEvent)
{
    if (systemTrayIcon->isVisible())
    {
        hide();
    }
    pEvent->ignore();
}

void PlannerWidget::keyPressEvent(QKeyEvent *pEvent)
{
    QWidget::keyPressEvent(pEvent);
    if ( pEvent->key() == Qt::Key_Escape)//Esc
    {
        if (systemTrayIcon->isVisible())
        {
            hide();
        }
        pEvent->ignore();
    }
}

void PlannerWidget::showHide()
{
    if (!isVisible())
    {
        QDesktopWidget *desktopWidget = QApplication::desktop();
        if (this->frameGeometry().width() >= desktopWidget->width() ||
            this->frameGeometry().height() >= desktopWidget->height())
        {
            resize(640, 320);
        }
    }
    setVisible(!isVisible());
}

void PlannerWidget::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
    if (activationReason == QSystemTrayIcon::DoubleClick)
    {
        showHide();
    }
}

void PlannerWidget::onAppQuit()
{
    taskType = ASSIGNEE;
    cleanLayout(_pVltTask);
    delete _pTaskWidget->layout();
    _pVltTask = NULL;
    emit appQuit();
    qApp->quit();
}

PlannerWidget::~PlannerWidget(){}
