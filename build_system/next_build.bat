@echo off
:SETTINGS
set "current_dir=D:\TimePlannerRepo\timeplanner\build_system"
set "git_dir=D:\TimePlannerRepo\timeplanner\.git"

set "source_client_dir=D:\TimePlanner\TimePlanner_Client"
set "source_server_dir=D:\TimePlanner\TimePlanner_Server"

set "build_client_dir=D:\TimePlannerBuild\build-TimePlanner_Client-Desktop_Qt_5_5_0_MinGW_32bit-Release"
set "build_server_dir=D:\TimePlannerBuild\build-TimePlanner_Server-Desktop_Qt_5_5_0_MinGW_32bit-Release"

set "build_setup_client_dir=D:\TimePlanner\ReleaseBuilds\TimePlanner"
set "build_setup_server_dir=D:\TimePlanner\ReleaseBuilds\TimePlanner_Server"

set "inno_setup_dir=C:\Program Files (x86)\Inno Setup 5"
set "system_dir=C:\WINDOWS\system32"

set "qmake_dir=D:\Qt\5.5\mingw492_32\bin"
set "make_dir=D:\Qt\Tools\mingw492_32\bin"
set "git_exe_dir=D:\Program Files\Git\cmd"

set "qmake_exe=qmake.exe"
set "make_exe=mingw32-make.exe"
set "makefile=Makefile.Release"

set "source_client_pro_file=TimePlanner_Client.pro"
set "source_server_pro_file=TimePlanner_Server.pro"

set "client_exe=TimePlanner_Client.exe"
set "server_exe=TimePlanner_Server.exe"

set "setup_client_exe=Setup_TimePlanner.exe"
set "setup_server_exe=Setup_TimePlanner_Server.exe"

set "setup_client_script=client_installation_script.iss"
set "setup_server_script=server_installation_script.iss"

set repo_builds_dir=..\builds
set repo_sources_dir=..\sources
set TEMP_PATH=%PATH%
set "PATH=%qmake_dir%;%make_dir%;%git_dir%;%system_dir%"
:END_OF_SETTINGS

:GET_NEW_VERSION
setlocal enableextensions disabledelayedexpansion
set /p old_version=<build_number.txt
set /p major_numbers=<"major_numbers.txt"
set /p third_number=<"third_number.txt"
SET temp_old_version=%old_version%
for /f "tokens=1,2,3,4 delims=. " %%a in ("%old_version%") do set old_num1=%%a&set old_num2=%%b&set old_num3=%%c&set old_num4=%%d
set /a new_num4=%old_num4%+1
set new_version=%major_numbers%.%third_number%.%new_num4%
set old_version_comma=%old_version:.=,%
set new_version_comma=%new_version:.=,%
set version_suffix=_v_%new_version%_x86_win32
:END_OF_GET_NEW_VERSION

:UPDATE_VERSION.H_FILES
set old_filever=#define VER_FILEVERSION             %old_version_comma%
set new_filever=#define VER_FILEVERSION             %new_version_comma%
set old_filever_str=#define VER_FILEVERSION_STR         "%old_version%\0"
set new_filever_str=#define VER_FILEVERSION_STR         "%new_version%\0"
set old_productver=#define VER_PRODUCTVERSION          %old_version_comma%
set new_productver=#define VER_PRODUCTVERSION          %new_version_comma%
set old_productver_str=#define VER_PRODUCTVERSION_STR      "%old_version%\0"
set new_productver_str=#define VER_PRODUCTVERSION_STR      "%new_version%\0"

setlocal enableextensions disabledelayedexpansion

set "search1=%old_filever%"
set "replace1=%new_filever%"
set "search2=%old_filever_str%"
set "replace2=%new_filever_str%"
set "search3=%old_productver%"
set "replace3=%new_productver%"
set "search4=%old_productver_str%"
set "replace4=%new_productver_str%"

set "version_file_client=%source_client_dir%\version.h"
set "version_file_server=%source_server_dir%\version.h"

for /f "delims=" %%i in ('type "%version_file_client%" ^& break ^> "%version_file_client%" ') do (
	set "line=%%i"
	setlocal enabledelayedexpansion
	set "line=!line:%search1%=%replace1%!"
	set "line=!line:%search2%=%replace2%!"
	set "line=!line:%search3%=%replace3%!"
	set "line=!line:%search4%=%replace4%!"
	>>"%version_file_client%" echo(!line!
	endlocal
)
for /f "delims=" %%i in ('type "%version_file_server%" ^& break ^> "%version_file_server%" ') do (
	set "line=%%i"
	setlocal enabledelayedexpansion
	set "line=!line:%search1%=%replace1%!"
	set "line=!line:%search2%=%replace2%!"
	set "line=!line:%search3%=%replace3%!"
	set "line=!line:%search4%=%replace4%!"
	>>"%version_file_server%" echo(!line!
	endlocal
)
:END_OF_UPDATE_VERSION.H_FILES

:BUILD_PROJECTS
if not exist "%build_client_dir%" mkdir "%build_client_dir%"
cd "%build_client_dir%"
"%make_dir%\%make_exe%" -f "%build_client_dir%\%makefile%" clean
"%qmake_dir%\%qmake_exe%" -spec win32-g++ "%source_client_dir%\%source_client_pro_file%"  
"%make_dir%\%make_exe%" -f "%build_client_dir%\%makefile%"
if %ERRORLEVEL% NEQ 0 goto EXIT
cd "%current_dir%"
if not exist "%build_server_dir%" mkdir "%build_server_dir%"
cd "%build_server_dir%"
"%make_dir%\%make_exe%" -f "%build_server_dir%\%makefile%" clean
"%qmake_dir%\%qmake_exe%" -spec win32-g++ "%source_server_dir%\%source_server_pro_file%"
"%make_dir%\%make_exe%" -f "%build_server_dir%\%makefile%"
if %ERRORLEVEL% NEQ 0 goto EXIT
cd "%current_dir%"
:END_OF_BUILD_PROJECTS

:BUILD_INSTALLERS
if exist "%build_setup_client_dir%\%client_exe%" del "%build_setup_client_dir%\%client_exe%"
if exist "%build_setup_server_dir%\%server_exe%" del "%build_setup_server_dir%\%server_exe%"
copy "%build_client_dir%\release\%client_exe%" "%build_setup_client_dir%\%client_exe%"
copy "%build_server_dir%\release\%server_exe%" "%build_setup_server_dir%\%server_exe%"	
set "installation_client=%build_setup_client_dir%\%setup_client_script%"
set "installation_server=%build_setup_server_dir%\%setup_server_script%"
set search_installation_version=#define   Version    "%old_version%"
set replace_installation_version=#define   Version    "%new_version%"
for /f "delims=" %%i in ('type "%installation_client%" ^& break ^> "%installation_client%" ') do (
	set "line=%%i"
	setlocal enabledelayedexpansion
	set "line=!line:%search_installation_version%=%replace_installation_version%!"
	>>"%installation_client%" echo(!line!
	endlocal
)
for /f "delims=" %%i in ('type "%installation_server%" ^& break ^> "%installation_server%" ') do (
	set "line=%%i"
	setlocal enabledelayedexpansion
	set "line=!line:%search_installation_version%=%replace_installation_version%!"
	>>"%installation_server%" echo(!line!
	endlocal
)

if exist "%build_setup_client_dir%\%setup_client_exe%" del "%build_setup_client_dir%\%setup_client_exe%"
if exist "%build_setup_server_dir%\%setup_server_exe%" del "%build_setup_server_dir%\%setup_server_exe%"

"%inno_setup_dir%\ISCC.exe" "%build_setup_client_dir%\client_installation_script.iss"
if %ERRORLEVEL% NEQ 0 goto EXIT
"%inno_setup_dir%\ISCC.exe" "%build_setup_server_dir%\server_installation_script.iss"
if %ERRORLEVEL% NEQ 0 goto EXIT
:END_OF_BUILD_INSTALLERS

:ORGANIZE_FILES_AT_REPO
copy "%build_setup_client_dir%\Setup_TimePlanner.exe" "%repo_builds_dir%\Setup_TimePlanner%version_suffix%.exe"
copy "%build_setup_server_dir%\Setup_TimePlanner_Server.exe" "%repo_builds_dir%\Setup_TimePlanner_Server%version_suffix%.exe"

if not exist "%repo_sources_dir%\v_%old_version%" mkdir "%repo_sources_dir%\v_%old_version%"
xcopy "%repo_sources_dir%\TimePlanner_Client" "%repo_sources_dir%\v_%old_version%\TimePlanner_Client" /s /e /i
xcopy "%repo_sources_dir%\TimePlanner_Server" "%repo_sources_dir%\v_%old_version%\TimePlanner_Server" /s /e /i
rmdir "%repo_sources_dir%\TimePlanner_Client" /s /q
rmdir "%repo_sources_dir%\TimePlanner_Server" /s /q
xcopy "%source_client_dir%" "%repo_sources_dir%\TimePlanner_Client" /s /e /i
xcopy "%source_server_dir%" "%repo_sources_dir%\TimePlanner_Server" /s /e /i
:END_OF_ORGANIZE_FILES_AT_REPO

echo %new_version%>build_number.txt
echo Previous version: %old_version%
echo Current version: %new_version%

:PUSH_COMMIT
cd "%git_dir%"\..
"%git_exe_dir%\git" fetch origin
if %ERRORLEVEL% NEQ 0 goto EXIT

REM git add -u    добавление в список готовых к коммиту УЖЕ отслеживаемых файлов
REM git add .     добавление в список готовых к коммиту УЖЕ отслеживаемых и новых файлов
REM git commit        просто коммит подготовленных файлов
REM git commit -a     добавление в список готовых к коммиту УЖЕ отслеживаемых файлов, затем коммит

"%git_exe_dir%\git" add .
"%git_exe_dir%\git" commit -m"Release Build v. %new_version%"
if %ERRORLEVEL% NEQ 0 goto EXIT

"%git_exe_dir%\git" push origin
if %ERRORLEVEL% NEQ 0 goto EXIT

REM git tag %version%.%build%	-  легковесная метка
REM git tag %version%.%build% -a	- создание аннотированной метки

"%git_exe_dir%\git" tag %new_version%

"%git_exe_dir%\git" push origin --tags
: END_OF_PUSH_COMMIT
:EXIT
set PATH=%TEMP_PATH%
cd %current_dir%
endlocal
